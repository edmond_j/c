﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework
{
    public class Pair<T, U>
    {
        public T First { get; set; }
        public U Second { get; set; }

        public Pair()
        {
            First = default(T);
            Second = default(U);
        }

        public Pair(T t, U u)
        {
            First = t;
            Second = u;
        }
    }
}
