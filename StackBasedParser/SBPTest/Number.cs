﻿using StackBasedParser.Contracts;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBPTest
{
    [Export(typeof(IToken))]
    [ExportMetadata("Pattern", @"\d+")]
    internal class Number: BasePushable, IToken
    {
    }
}
