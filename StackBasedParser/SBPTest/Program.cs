﻿using StackBasedParser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBPTest
{
    class Program
    {
        static void Main(string[] args)
        {
            ParserImpl parser = new ParserImpl();
            
            while (true)
            {
                parser.Parse(Console.ReadLine());
            }
        }
    }
}
