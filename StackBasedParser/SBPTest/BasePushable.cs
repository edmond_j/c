﻿using StackBasedParser.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBPTest
{
    internal abstract class BasePushable: IPushable
    {
        public void Pushed(string value)
        {
            Console.WriteLine(value);
        }
    }
}
