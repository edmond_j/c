﻿using StackBasedParser;
using StackBasedParser.Contracts;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBPTest
{
    [Export(typeof(IOperand))]
    [ExportMetadata("Pattern", @"\+|-", IsMultiple=true)]
    [ExportMetadata("Precedence", 2, IsMultiple = true)]
    [ExportMetadata("Associativity", OperandAssociativity.Left, IsMultiple = true)]
    internal class AddTypeOperand : BasePushable, IOperand
    {
    }
}
