﻿using StackBasedParser;
using StackBasedParser.Contracts;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBPTest
{
    [Export(typeof(IOperand))]
    [ExportMetadata("Pattern", @"\\|\*")]
    [ExportMetadata("Precedence", 3)]
    [ExportMetadata("Associativity", OperandAssociativity.Left)]
    internal class MultTypeOperand: BasePushable, IOperand
    {
    }
}
