﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;
using StackBasedParser.Contracts;
using System.ComponentModel.Composition.Primitives;
using System.ComponentModel.Composition.Hosting;
using System.Reflection;
using StackBasedParser.ImplInternal;
using StackBasedParser.ImplInternal.Contracts;

namespace StackBasedParser
{
    public class ParserImpl : IParser
    {
        public ComposablePartCatalog Catalog { get; set; }

        public ParserImpl(ComposablePartCatalog catalog)
        {
            this.Catalog = catalog;
        }

        public ParserImpl()
        {
            this.Catalog = new AssemblyCatalog(Assembly.GetEntryAssembly());
        }

        private bool _ignoreBlanks = true;
        public bool IgnoreBlanks 
        {
            get
            {
                return (_ignoreBlanks);
            }
            set
            {
                _ignoreBlanks = value;
            }
        }

        public void Parse(string input)
        {
            IParserMotor motor = new ParserMotor(this.Catalog);

            motor.IgnoreBlanks = this.IgnoreBlanks;
            motor.ItemPushed += motor_ItemPushed;
            motor.OperandPushed += motor_OperandPushed;
            motor.TokenPushed += motor_TokenPushed;
            motor.Parse(input);
        }

        void motor_TokenPushed(object sender, ItemPushedEventArgs e)
        {
            if (this.TokenPushed != null)
                this.TokenPushed(this, e);
        }

        void motor_OperandPushed(object sender, ItemPushedEventArgs e)
        {
            if (this.OperandPushed != null)
                this.OperandPushed(this, e);
        }

        void motor_ItemPushed(object sender, ItemPushedEventArgs e)
        {
            if (this.ItemPushed != null)
                this.ItemPushed(this, e);
        }

        public event EventHandler<ItemPushedEventArgs> ItemPushed;

        public event EventHandler<ItemPushedEventArgs> OperandPushed;

        public event EventHandler<ItemPushedEventArgs> TokenPushed;
    }
}
