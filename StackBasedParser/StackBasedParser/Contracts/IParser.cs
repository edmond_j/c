﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StackBasedParser.Contracts
{
    public interface IParser
    {
        bool IgnoreBlanks { get; set; }

        void Parse(String input);

        event EventHandler<ItemPushedEventArgs> ItemPushed;
        event EventHandler<ItemPushedEventArgs> OperandPushed;
        event EventHandler<ItemPushedEventArgs> TokenPushed;
    }
}
