﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StackBasedParser.Contracts
{
    public interface ITokenMetadata
    {
        String Pattern { get; }
    }
}
