﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StackBasedParser
{
    public class ItemPushedEventArgs : EventArgs
    {
        public String Item { get; private set; }

        public ItemPushedEventArgs(String item)
        {
            this.Item = item;
        }
    }
}
