﻿using StackBasedParser.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace StackBasedParser.ImplInternal
{
    internal class Token: ITokenMetadata
    {
        public IToken Tok { get; set; }
        public string Pattern { get; set; }
        public Regex Exp { get; set; }
    }
}
