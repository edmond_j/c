﻿using Framework;
using StackBasedParser.Contracts;
using StackBasedParser.ImplInternal.Contracts;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Primitives;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace StackBasedParser.ImplInternal
{
    internal class ParserMotor: IParserMotor
    {
        [ImportMany(typeof(IOperand))]
        private IEnumerable<Lazy<IOperand, IOperandMetadata>> _metaOperands;

        [ImportMany(typeof(IToken))]
        private IEnumerable<Lazy<IToken, ITokenMetadata>> _metaTokens;

        private ICollection<Operand> _operands;
        private ICollection<Token> _tokens;

        private Stack<Pair<String, Operand>> _stack = new Stack<Pair<string,Operand>>();

        public ParserMotor(ComposablePartCatalog catalog)
        {
            CompositionContainer container = new CompositionContainer(catalog);

            container.ComposeParts(this);

            _operands = new List<Operand>();
            _tokens = new List<Token>();
            foreach (Lazy<IOperand, IOperandMetadata> op in _metaOperands)
            {
                _operands.Add(new Operand() { Op = op.Value, Associativity = op.Metadata.Associativity, Pattern = FormatPattern(op.Metadata.Pattern), Precedence = op.Metadata.Precedence });
                _operands.Last().Exp = new Regex(_operands.Last().Pattern);
            }
            foreach (Lazy<IToken, ITokenMetadata> tok in _metaTokens)
            {
                _tokens.Add(new Token() { Tok = tok.Value, Pattern = FormatPattern(tok.Metadata.Pattern) });
                _tokens.Last().Exp = new Regex(_tokens.Last().Pattern);
            }
            Console.WriteLine("{0} tokens && {1} operands", _tokens.Count, _operands.Count);
        }

        static private readonly char[] EscapeChars = new char[] { '^', '(', ')', '$' };

        static private String FormatPattern(String pattern)
        {
            StringBuilder builder = new StringBuilder();

            foreach (char c in pattern)
            {
                if (EscapeChars.Contains(c))
                    builder.Append('\\');
                builder.Append(c);
            }
            return ("^(" + builder.ToString() + ")");
        }

        public event EventHandler<ItemPushedEventArgs> ItemPushed;

        public event EventHandler<ItemPushedEventArgs> OperandPushed;

        public event EventHandler<ItemPushedEventArgs> TokenPushed;

        public bool IgnoreBlanks
        {
            get;
            set;
        }

        public void Parse(string input)
        {
            bool stop = false;
            Regex ignBlnk = new Regex(@"^\s*(.*)$");
            Match match = null;
            Token tokFound = null;
            Operand opFound = null;
            String item;

            while (!stop)
            {
                if (this.IgnoreBlanks && (match = ignBlnk.Match(input)).Success)
                    input = ignBlnk.Match(input).Captures[0].Value;
                if (input.Length == 0)
                    break;
                if (input[0] == '(')
                {
                    _stack.Push(null);
                }
                else if (input[0] == ')')
                {
                    popToNull();
                }
                else
                {
                    foreach (Token tok in _tokens)
                    {
                        match = tok.Exp.Match(input);

                        if (match.Success)
                        {
                            tokFound = tok;
                            break;
                        }
                    }
                    if (!match.Success)
                        foreach (Operand op in _operands)
                        {
                            match = op.Exp.Match(input);

                            if (match.Success)
                            {
                                opFound = op;
                                break;
                            }
                        }
                    if (match.Success)
                    {
                        item = match.Groups[0].Value;
                        input = input.Substring(item.Length);
                        Console.WriteLine("Found {0}, left {1}", item, input);
                        if (tokFound != null)
                            this.pushToken(item, tokFound);
                        if (opFound != null)
                            this.pushOperand(item, opFound);
                    }
                    stop = !match.Success;
                    match = null;
                    tokFound = null;
                    opFound = null;
                }
            }
            popToEnd();
        }

        private void popToEnd()
        {
            while (_stack.Count != 0)
                popOperand();
        }

        private void popToNull()
        {
        }

        private void pushOperand(string item, Operand opFound)
        {
            while (!(_stack.Count == 0 || _stack.Peek() == null || _stack.Peek().Second.Precedence < opFound.Precedence || (_stack.Peek().Second.Precedence == opFound.Precedence && opFound.Associativity == OperandAssociativity.Right)))
            {
                popOperand();
            }
            Console.WriteLine("Item {0}", item);
            _stack.Push(new Pair<string, Operand>((String)item.Clone(), opFound));
        }

        private void popOperand()
        {
            Pair<String, Operand> op = _stack.Pop();

            op.Second.Op.Pushed(op.First);
            if (this.ItemPushed != null)
                this.ItemPushed(this, new ItemPushedEventArgs(op.First));
            if (this.OperandPushed != null)
                this.OperandPushed(this, new ItemPushedEventArgs(op.First));
        }

        private void pushToken(string item, Token tokFound)
        {
            tokFound.Tok.Pushed(item);
            if (this.ItemPushed != null)
                this.ItemPushed(this, new ItemPushedEventArgs(item));
            if (this.TokenPushed != null)
                this.TokenPushed(this, new ItemPushedEventArgs(item));
        }
    }
}
