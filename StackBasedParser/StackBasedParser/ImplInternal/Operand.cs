﻿using StackBasedParser.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace StackBasedParser.ImplInternal
{
    internal class Operand : IOperandMetadata
    {
        public IOperand Op { get; set; }
        public string Pattern { get; set; }
        public Regex Exp { get; set; }
        public short Precedence { get; set; }
        public OperandAssociativity Associativity { get; set; }
    }
}
