﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tools
{
    class Events
    {
        public static void BroadcastEvent(object sender, EventHandler e, EventArgs args)
        {
            if (e != null)
                e(sender, args);
        }

        public static void BroadcastEvent<T>(object sender, EventHandler<T> e, T args)
        {
            if (e != null)
                e(sender, args);
        }
    }
}
