﻿using SoC.Contracts;

namespace SoC.Abstracts
{
    public abstract class AGameLinked
    {
        public IGame Game { set; get; }
    }
}
