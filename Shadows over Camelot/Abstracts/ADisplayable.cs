﻿using System;
using SoC.Contracts;

namespace SoC
{
    namespace Abstracts
    {
        public abstract class ADisplayable : IDisplayable
        {
            public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

            public void PropetyChanged(String prop)
            {
                if (this.PropertyChanged != null)
                    this.PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(prop));
            }
        }
    }
}
