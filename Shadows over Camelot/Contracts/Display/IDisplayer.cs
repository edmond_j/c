﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoC.Contracts
{
    interface IDisplayer
    {
        void Init(IGame Game);
    }
}
