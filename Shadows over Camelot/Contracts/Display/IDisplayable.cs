﻿using System.ComponentModel;

namespace SoC
{
    namespace Contracts
    {
        public interface IDisplayable : INotifyPropertyChanged
        {
        }
    }
}