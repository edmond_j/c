﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SoC
{
    namespace Contracts
    {
        public class CardDiscardedEventArgs: EventArgs
        {
            public ICharacter Who { set; get; }
        }
    }
}
