﻿namespace SoC
{
    namespace Contracts
    {
        public interface IColoredCard : ICard
        {
            bool IsSpecial { get; }
            IQuest RelatedQuest { get; }
        }
    }
}