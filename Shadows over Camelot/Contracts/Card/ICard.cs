﻿using System;

namespace SoC
{
    namespace Contracts
    {
        public interface ICard : IDisplayable
        {
            String Name { get; }
            String Description { get; }
            event EventHandler<CardDrawnEventArgs> CardDrawn;
            event EventHandler<CardPlayedEventArgs> CardPlayed;
            event EventHandler<CardDiscardedEventArgs> CardDiscarded;
            event EventHandler<CardRemovedEventArgs> CardRemoved;
        }
    }
}