using System;
using System.Collections.Generic;

namespace SoC.Contracts
{
    public interface IWhiteDeck : IDeck
    {
        new IWhiteCard Drawn();
        void AddCard(IWhiteCard card);
        void AddCards(ICollection<IWhiteCard> cards);
        void AddCards(IWhiteCard card, int quantity);
        new IWhiteCard GetCardOfType(Type t);
    }
}