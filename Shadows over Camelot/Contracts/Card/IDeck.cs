﻿using System;
using System.Collections;
using System.Collections.Generic;
using SoC;

namespace SoC
{
    namespace Contracts
    {
        public interface IDeck
        {
            event EventHandler<CardDrawnEventArgs> CardDrawn;
            event EventHandler<DeckShuffledEventArgs> DeckShuffled;

            ICard Drawn();
            void AddCard(ICard card);
            void AddCards(ICollection<ICard> cards);
            void AddCards(ICard card, int quantity);
            ICard GetCardOfType(Type t);
            void Shuffle();
        }
    }
}