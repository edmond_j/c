﻿using System;
using System.Collections.Generic;

namespace SoC.Contracts
{
    public interface IBlackDeck : IDeck
    {
        new IBlackCard Drawn();
        void AddCard(IBlackCard card);
        void AddCards(ICollection<IBlackCard> cards);
        void AddCards(IBlackCard card, int quantity);
        new IBlackCard GetCardOfType(Type t);
    }
}