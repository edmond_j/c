﻿namespace SoC.Contracts
{
    public interface IWhiteFightCard : IWhiteCard, IFightable
    {
    }
}