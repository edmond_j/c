﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoC.Contracts.Card
{
    public interface ICardMetadata
    {
        DeckType DeckType { get; set; }
        Int16 NumberOfCard { get; set; }
    }
}
