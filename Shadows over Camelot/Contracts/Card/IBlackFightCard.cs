﻿namespace SoC.Contracts
{
    public interface IBlackFightCard : IWhiteCard, IFightable
    {
    }
}