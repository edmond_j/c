﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SoC
{
    namespace Contracts
    {
        public class CardDrawnEventArgs : EventArgs
        {
            public ICharacter Who { private set; get; }
            public ICard WichCard { private set; get; }

            public CardDrawnEventArgs(ICharacter who, ICard card)
            {
                this.Who = who;
                this.WichCard = card;
            }
        }
    }
}