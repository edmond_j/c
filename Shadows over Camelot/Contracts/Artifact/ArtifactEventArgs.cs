﻿using SoC.Contracts.Artifact;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SoC
{
    namespace Contracts
    {
        public class ArtifactEventArgs: EventArgs
        {
            public IArtifact Artifact { private set; get; }

            public ArtifactEventArgs(IArtifact art)
            {
                this.Artifact = art;
            }
        }
    }
}