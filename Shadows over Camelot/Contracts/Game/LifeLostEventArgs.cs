﻿using System;

namespace SoC
{
    namespace Contracts
    {
        public class LifeLostEventArgs: EventArgs
        {
            public int LifeLost;
            public int CurrentLife;
        }
    }
}
