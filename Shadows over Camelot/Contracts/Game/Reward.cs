﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoC.Contracts
{
    public class Reward
    {
        public int WhiteSwords { set; get; }
        public int BlackSwords { set; get; }
        public int LifeLost { set; get; }
        public int WhiteCardDrawn { set; get; }
    }
}
