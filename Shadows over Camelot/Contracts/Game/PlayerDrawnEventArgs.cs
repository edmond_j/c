﻿using System;
using System.Collections.Generic;

namespace SoC
{
    namespace Contracts
    {
        public class PlayerDrawnEventArgs: EventArgs
        {
            public ICollection<IWhiteCard> CardsAdded;

            public PlayerDrawnEventArgs(ICollection<IWhiteCard> cards)
            {
                this.CardsAdded = cards;
            }

            public PlayerDrawnEventArgs(IWhiteCard card)
            {
                this.CardsAdded = new List<IWhiteCard> {card};
            }
        }
    }
}
