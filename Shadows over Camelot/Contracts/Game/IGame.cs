﻿using System;
using System.Collections.Generic;
using SoC;

namespace SoC
{
    namespace Contracts
    {
        public interface IGame
        {
            ICollection<IQuest> Quests { get; }
            ICamelot Camelot { get; }

            ICollection<ICard> Cards { get; }
            IWhiteDeck WhiteDeck { get; }
            IBlackDeck BlackDeck { get; }

            ICollection<ICharacter> Characters { get; }
            ICollection<ICharacter> InGameCharacters { get; }

            bool GameEnded { get; }

            event EventHandler<QuestCompletedEventArgs> QuestCompleted;
            event EventHandler<QuestCompletedEventArgs> QuestWon;
            event EventHandler<QuestCompletedEventArgs> QuestLost;
            event EventHandler<GameCompletedEventArgs> GameCompleted;
            event EventHandler<GameCompletedEventArgs> GameWon;
            event EventHandler<GameCompletedEventArgs> GameLost;

            event EventHandler<SwordPlacedEventArgs> SwordPlaced;
            event EventHandler<CatapultPlacedEventArgs> CatapultPlaced;
            IQuest GetInstanceOfQuest(Type questType);
            ICharacter GetInstanceOfCharacter(Type characterType);
            ICard GetInstanceOfCard(Type cardType);

            void PlayTurn();
        }
    }
}