﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SoC
{
    namespace Contracts
    {
        public class PlayerMovedEventArgs : EventArgs
        {
            public IQuest From { private set; get; }
            public IQuest To { set; get; }
            public bool Interrupted { get; set; }

            public PlayerMovedEventArgs(IQuest from, IQuest to)
            {
                this.From = from;
                this.To = to;
                this.Interrupted = false;
            }
        }
    }
}