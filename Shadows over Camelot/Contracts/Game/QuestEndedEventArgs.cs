﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SoC.Contracts
{
    public class QuestCompletedEventArgs : EventArgs
    {
        public IQuest Quest { private set; get; }
        public Reward Reward { get; set; }
        public Loyalties Winners { private set; get; }

        public QuestCompletedEventArgs(IQuest quest, Loyalties winners)
        {
            this.Quest = quest;
            this.Winners = winners;
        }
    }
}
