﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SoC.Contracts
{
    public class SwordPlacedEventArgs : EventArgs
    {
        public int HowMany { private set; get; }
        public Loyalties Loyaltie { private set; get; }

        public SwordPlacedEventArgs(int howMany, Loyalties loyaltie)
        {
            this.HowMany = howMany;
            this.Loyaltie = loyaltie;
        }
    }
}
