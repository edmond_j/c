﻿using System;
using SoC.Contracts;

namespace SoC
{
    namespace Contracts
    {
        public interface IChoice
        {
            String Description { get; }

            bool CanExecute(ICharacter charachter);
            bool Execute();
        }
    }
}