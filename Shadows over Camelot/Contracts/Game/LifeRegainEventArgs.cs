﻿using System;

namespace SoC
{
    namespace Contracts
    {
        public class LifeRegainEventArgs: EventArgs
        {
            public int LifeRegained;
            public int CurrentLife;
        }
    }
}
