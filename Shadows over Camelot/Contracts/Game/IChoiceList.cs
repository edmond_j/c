﻿using System.Collections.Generic;

namespace SoC
{
    namespace Contracts
    {
        public interface IChoiceList
        {
            ICollection<IChoice> FreeChoices { get; }
            ICollection<IChoice> StrongChoices { get; }

            void AddToFreeChoices(IChoice choice);
            void RemoveFromFreeChoices(IChoice choice);

            void AddToStrongChoices(IChoice choice);
            void RemoveFromStrongChoices(IChoice choice);
        }
    }
}
