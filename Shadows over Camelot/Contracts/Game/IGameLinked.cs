﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoC.Contracts
{
    public interface IGameLinked
    {
        IGame Game { set; get; }
    }
}
