﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoC.Contracts
{
    public interface ICombinedFightable: ICollection<IFightable>, IFightable
    {
    }
}
