﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SoC.Contracts
{
    public class FightEndedEventArgs : EventArgs
    {
        public IFightable Good { private set; get; }
        public int GoodValue { set; get; }
        public IFightable Bad { private set; get; }
        public int BadValue { set; get; }
        public IFightable Winner { private set; get; }

        public FightEndedEventArgs(IFightable good, int goodValue, IFightable bad, int badValue, IFightable winner = null)
        {
            this.Good = good;
            this.GoodValue = goodValue;
            this.Bad = bad;
            this.BadValue = badValue;
            this.Winner = winner;
        }
    }
}
