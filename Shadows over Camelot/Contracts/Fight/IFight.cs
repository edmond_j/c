﻿using System;
using SoC;

namespace SoC
{
    namespace Contracts
    {
        public interface IFight
        {
            Loyalties Fight(IFightable good, IFightable bad);

            event EventHandler<FightEndedEventArgs> FightResolution;
            event EventHandler<FightEndedEventArgs> FightEnded;
        }
    }
}
