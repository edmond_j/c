﻿using System;

namespace SoC
{
    namespace Contracts
    {
        public interface IFightable
        {
            int FightValue { get; }
            event EventHandler<FightResolutionEventArgs> FightResolution;
            event EventHandler<FightResolutionEventArgs> FightWon;
            event EventHandler<FightResolutionEventArgs> FightLost;
        }
    }
}
