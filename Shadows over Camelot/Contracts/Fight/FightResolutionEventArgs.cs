﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SoC.Contracts
{
    public class FightResolutionEventArgs : EventArgs
    {
        int YourValue { get; set; }
        int OpponentValue { get; set; }
    }
}
