﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SoC.Contracts
{
    public class CatapultPlacedEventArgs : EventArgs
    {
        public int HowMany { private set; get; }

        public CatapultPlacedEventArgs(int howMany)
        {
            this.HowMany = howMany;
        }
    }
}
