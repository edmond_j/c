﻿using System;
using System.Collections.Generic;
using SoC;
using SoC.Contracts.Artifact;

namespace SoC
{
    namespace Contracts
    {
        public interface ICharacter: IFightable
        {
            #region LifePoints
            int LifePoints { get; }

            void LooseLife(int nb);
            void RegainLife(int nb);
            void Die();
            event EventHandler<LifeLostEventArgs> LifeLost;
            event EventHandler<LifeRegainEventArgs> LifeRegain;
            event EventHandler<CharacterDiedEventArgs> CharacterDied;
            #endregion

            #region Hand
            ICollection<IWhiteCard> Hand { get; }

            void AddCard(IWhiteCard card);
            void RemoveCard(IWhiteCard card);
            event EventHandler<PlayerDrawnEventArgs> PlayerDrawn;
            #endregion

            #region Where
            IQuest Where { get; }

            void MoveTo(IQuest q);
            event EventHandler<PlayerMovedEventArgs> PlayerMoved;
            event EventHandler<PlayerMovedEventArgs> PlayerSuccessMoved;
            event EventHandler<PlayerMovedEventArgs> PlayerFailMoved;
            #endregion

            #region Artifact
            ICollection<IArtifact> Artifacts { get; }

            bool HasArtifact<T>()
                where T : IArtifact;
            bool HasExcalibur { get; }
            bool HasGrail { get; }
            bool HasLancelotArmor { get; }

            void GainArtifact(IArtifact art);
            void LooseArtifact(IArtifact art);
            event EventHandler<ArtifactEventArgs> ArtifactWon;
            event EventHandler<ArtifactEventArgs> ArtifactLost;
            #endregion

            #region Alignment

            Loyalties Loyalty { get; }
            bool IsRevealed { get; }

            void SetLoyalty(Loyalties l);
            void Reveal();
            event EventHandler<PlayerRevealedEventArgs> PlayerRevealed;

            #endregion

            #region InnerChoice

            IChoiceList InnerChoice { get; }

            #endregion
        }
    }
}