﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoC;

namespace SoC
{
    namespace Contracts
    {
        public interface ICamelot: IQuest
        {
            int WhiteSwords { get; }
            int BlackSwords { get; }

            event EventHandler<GameResolutionEventArgs> GameResolution; 

            void AddWhiteSwords(int nb = 1);
            void AddBlackSwords(int nb = 1);
        }
    }
}
