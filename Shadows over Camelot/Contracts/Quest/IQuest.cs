﻿using System;

namespace SoC
{
    namespace Contracts
    {
        public interface IQuest: IGameLinked
        {
            int SlotNumbers { get; }
            ICharacter[] PresentCharacters { get; }
            IChoiceList Choices { get; }

            event EventHandler<QuestCompletedEventArgs> QuestCompleted;
            event EventHandler<QuestCompletedEventArgs> QuestWon;
            event EventHandler<QuestCompletedEventArgs> QuestLost;

            void Init();
        }
    }
}
