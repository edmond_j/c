﻿/*
Copyright Julien Edmond (05/06/2014) 

edmondju@gmail.com

This library is a .NET library whose purpose is to help developpers in
diffent ways.

This software is governed by the CeCILL-C  license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using My.IO;
using System.Globalization;
using My;
using System.Text.RegularExpressions;
using System.IO;
using System.Runtime.Serialization.Formatters.Soap;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace PEGIE
{
    /// <summary>
    /// The main PEGIE class.
    /// </summary>
    public static class PEGIE
    {
        static private IPEG _pegiepeg;

        internal static IPEG Pegiepeg
        {
            get { return PEGIE._pegiepeg; }
        }

        private static IPEG BuildPEGIE()
        {
            PEGBuilder builder = new PEGBuilder();

            RuleBuilder letter = builder.NewRule("letter");
            letter.Tree = new IgnoreBlanks(false) & new IgnoreCase(true) & new Range('a', 'z');

            RuleBuilder digit = builder.NewRule("digit");
            digit.Tree = new IgnoreBlanks(false) & new Range('0', '9');

            RuleBuilder identifier = builder.NewRule("identifier");
            identifier.Tree = new IgnoreBlanks(false) & new OneOrMany(new RuleCall("letter") | new RuleCall("digit") | new Sequence("_"));

            RuleBuilder rule_name = builder.NewRule("rule_name");
            rule_name.Tree = new IgnoreBlanks(false) & new Sequence("<") & new RuleCall("identifier") & new Sequence(">");

            RuleBuilder hex = builder.NewRule("hex");
            hex.Tree = new IgnoreBlanks(false) & (new RuleCall("digit") | (new IgnoreCase(true) & new Range('a', 'f')));

            RuleBuilder oct = builder.NewRule("oct");
            oct.Tree = new IgnoreBlanks(false) & new Except(new RuleCall("digit"), new Range('8', '9'));

            RuleBuilder ascii_char = builder.NewRule("ascii_char");
            ascii_char.Tree = new IgnoreBlanks(false) &
                ((new Sequence("\\x") & new RuleCall("hex") * 2)
                | (new Sequence("\\o") & new RuleCall("oct") * 3));

            RuleBuilder range = builder.NewRule("range");
            range.Tree = new IgnoreBlanks(false) & new Sequence("[") & new Capture(new RuleCall("ascii_char") | new Any(), "p1")
                & new Sequence("-") & new Capture(new RuleCall("ascii_char") | new Any(), "p2") & new Sequence("]") & new Hook("newRange", "p1", "p2");

            RuleBuilder sequence = builder.NewRule("sequence");
            sequence.Tree = new IgnoreBlanks(false) & (
                (new Sequence("'") & new Capture(new NoneOrMany(new Sequence("\\'") | new RuleCall("ascii_char") | new Any() - new Sequence("'")), "seq") & new Sequence("'"))
                |
                (new Sequence("\"") & new Capture(new NoneOrMany(new Sequence("\\\"") | new RuleCall("ascii_char") | new Any() - new Sequence("\"")), "seq") & new Sequence("\""))
                ) & new Hook("newSequence", "seq");

            RuleBuilder parameters = builder.NewRule("parameters");
            parameters.Tree = new Capture(new RuleCall("identifier"), "param") & new Hook("newParam", "param") & new OneOrNone(new Sequence(",") & new RuleCall("parameters"));

            RuleBuilder hook = builder.NewRule("hook");
            hook.Tree = new IgnoreBlanks(false) & new Sequence("#") & new Capture(new RuleCall("identifier"), "hookName") & new OneOrNone(new Sequence("(") & new IgnoreBlanks(true) & new OneOrNone(new RuleCall("parameters")) & new Sequence(")")) & new Hook("newHook", "hookName");

            RuleBuilder boolean = builder.NewRule("boolean");
            boolean.Tree = new IgnoreCase(true) & (new Sequence("true") | new Sequence("false"));

            RuleBuilder eol_kind = builder.NewRule("eol_kind");
            eol_kind.Tree = new IgnoreCase(true) & (new Sequence("windows") | new Sequence("unix"));

            RuleBuilder unicode_category = builder.NewRule("unicode_category");
            unicode_category.Tree = new IgnoreCase(true) & (
                new Sequence("UppercaseLetter") | 
                new Sequence("LowercaseLetter") |
                new Sequence("TitlecaseLetter") |
                new Sequence("ModifierLetter") |
                new Sequence("OtherLetter") |
                new Sequence("NonSpacingMark") |
                new Sequence("SpacingCombiningMark") |
                new Sequence("EnclosingMark") |
                new Sequence("DecimalDigitNumber") |
                new Sequence("LetterNumber") |
                new Sequence("OtherNumber") |
                new Sequence("SpaceSeparator") |
                new Sequence("LineSeparator") |
                new Sequence("ParagraphSeparator") |
                new Sequence("Control") |
                new Sequence("Format") |
                new Sequence("Surrogate") |
                new Sequence("PrivateUse") |
                new Sequence("ConnectorPunctuation") |
                new Sequence("DashPunctuation") |
                new Sequence("OpenPunctuation") |
                new Sequence("ClosePunctuation") |
                new Sequence("InitialQuotePunctuation") |
                new Sequence("FinalQuotePunctuation") |
                new Sequence("OtherPunctuation") |
                new Sequence("MathSymbol") |
                new Sequence("CurrencySymbol") |
                new Sequence("ModifierSymbol") |
                new Sequence("OtherSymbol") |
                new Sequence("OtherNotAssigned"));

            RuleBuilder unicode_categories = builder.NewRule("unicode_categories");
            unicode_categories.Tree = new Capture(new RuleCall("unicode_category"), "uc") & new Hook("newUnicodeCategory", "uc")
                & new OneOrNone(new Sequence(",") & new RuleCall("unicode_categories"));

            RuleBuilder ignore_blanks = builder.NewRule("ignore_blanks");
            ignore_blanks.Tree = new IgnoreBlanks(false) & new Sequence("ignore_blanks(") & new IgnoreBlanks(true)
                & new Capture(new RuleCall("boolean"), "value") & new Sequence(")") & new Hook("newIgnoreBlanks", "value");

            RuleBuilder ignore_case = builder.NewRule("ignore_case");
            ignore_case.Tree = new IgnoreBlanks(false) & new Sequence("ignore_case(") & new IgnoreBlanks(true)
                & new Capture(new RuleCall("boolean"), "value") & new Sequence(")") & new Hook("newIgnoreCase", "value");

            RuleBuilder eol_style = builder.NewRule("eol_style");
            eol_style.Tree = new IgnoreBlanks(false) & new Sequence("eol_style(") & new IgnoreBlanks(true)
                & new Capture(new RuleCall("eol_kind"), "value") & new Sequence(")") & new Hook("newEndOfLineStyle", "value");

            RuleBuilder blanks_definition = builder.NewRule("blanks_definition");
            blanks_definition.Tree = new IgnoreBlanks(false) & new Sequence("blanks_definition(") & new IgnoreBlanks(true) & new RuleCall("unicode_categories")
                & new Sequence(")") & new Hook("newBlanksDefinition");

            RuleBuilder directive = builder.NewRule("directive");
            directive.Tree = new IgnoreBlanks(false) & new Sequence("@") & (
                new RuleCall("ignore_blanks") |
                new RuleCall("ignore_case") | 
                new RuleCall("eol_style") | 
                new RuleCall("blanks_definition"));

            RuleBuilder capture = builder.NewRule("capture");
            capture.Tree = new IgnoreBlanks(false) & new Sequence(":") & new Capture(new RuleCall("identifier"), "c") & new Hook("newCapture", "c");

            RuleBuilder expr = builder.NewRule("expr");
            expr.Tree = new RuleCall("range") |
                (new Capture(new RuleCall("rule_name"), "rn") & new Hook("newRuleCall", "rn")) |
                new RuleCall("sequence") |
                new RuleCall("hook") |
                new RuleCall("directive") |
                (new Sequence("(") & new RuleCall("binary_expr") & new Sequence(")"));

            RuleBuilder unary_op = builder.NewRule("unary_op");
            unary_op.Tree = (
                (new Capture(new Sequence("?"), "op") | new Capture(new Sequence("*"), "op") | new Capture(new Sequence("+"), "op")) & new Hook("newUnary", "op")
//                |
//                (new Sequence("{") & new Capture(new OneOrMany(new RuleCall("digit")), "q1") & new OneOrNone(new Capture(new Sequence(","), "q2") &  new OneOrNone(new Capture(new OneOrMany(new RuleCall("digit")), "q2"))) & new Sequence("}") & new Hook("newQuantifier", "q1", "q2"))
                );
            
            
            RuleBuilder unary_expr = builder.NewRule("unary_expr");
            unary_expr.Tree = new RuleCall("expr") & new OneOrNone(new RuleCall("unary_op")) & new OneOrNone(new IgnoreBlanks(false) & new RuleCall("capture"));

            RuleBuilder chain_expr = builder.NewRule("chain_expr");
            chain_expr.Tree = new RuleCall("unary_expr") & new OneOrNone(new RuleCall("chain_expr") & new Hook("newAnd"));

            RuleBuilder binary_op = builder.NewRule("binary_op");
            binary_op.Tree = new Sequence("-") | new Sequence("|");

            RuleBuilder binary_expr = builder.NewRule("binary_expr");
            binary_expr.Tree = new RuleCall("chain_expr") & new OneOrNone(new Capture(new RuleCall("binary_op"), "op") & new RuleCall("binary_expr") & new Hook("newBinary", "op"));

            RuleBuilder rule = builder.NewRule("rule");
            rule.Tree = new Capture(new RuleCall("rule_name") | (new IgnoreBlanks(false) & new Sequence("<") & new RuleCall("rule_name") & new Sequence(">") & new IgnoreBlanks(true)), "rn") & new Sequence(":=") & new RuleCall("binary_expr") & new Sequence(";") & new Hook("newRule", "rn");

            RuleBuilder main = builder.NewRule("main");
            main.Tree = new BlanksDefinition(UnicodeCategory.SpaceSeparator, UnicodeCategory.Control) & new OneOrMany(new RuleCall("rule")) & new EndOfStream();

            builder.StartingRule = main;

            return builder.Finalize();
        }

        static PEGIE()
        {
#if !NO_COMPILED_PEG
            if (File.Exists("PEGIE.peg"))
                using (FileStream fs = new FileStream("PEGIE.peg", FileMode.Open, FileAccess.Read))
                    _pegiepeg = PEGIE.LoadPEG(fs);
            else
            {
                _pegiepeg = BuildPEGIE();
                using (FileStream fs = new FileStream("PEGIE.peg", FileMode.Create, FileAccess.Write))
                    PEGIE.SavePEG(fs, _pegiepeg);
            }
#else
            _pegiepeg = BuildPEGIE();
#endif
        }

        /// <summary>
        /// Parses a PEG.
        /// </summary>
        /// <param name="reader">The reader to read the PEG from.</param>
        /// <returns>The parsed PEG.</returns>
        public static IPEG ParsePEG(IReader<int> reader)
        {
            return ParsePEG(reader, PEGBuilder.GetDefaultRenderer());
        }

        /// <summary>
        /// Parses the PEG described in a <see cref="String"/>.
        /// </summary>
        /// <param name="stream">The PEG definition.</param>
        /// <returns>The parsed PEG.</returns>
        public static IPEG ParsePEG(string stream)
        {
            return ParsePEG(ReaderHelper.NewStringReader(stream), PEGBuilder.GetDefaultRenderer());
        }
        
        /// <summary>
        /// Parses the PEG described in a file.
        /// </summary>
        /// <param name="fileName">The name of the file where the PEG is defined.</param>
        /// <returns>The parsed PEG.</returns>
        public static IPEG ParsePEGFromFile(string fileName)
        {
            return ParsePEG(ReaderHelper.NewStreamReader(fileName), PEGBuilder.GetDefaultRenderer());
        }


        /// <summary>
        /// Parses a PEG.
        /// </summary>
        /// <param name="reader">The reader to read the PEG from.</param>
        /// <param name="renderer">The renderer that will be use for the PEG generation.</param>
        /// <returns>The parsed PEG.</returns>
        public static IPEG ParsePEG(IReader<int> reader, IPEGRenderer renderer)
        {
            BufferizedReader bReader = new BufferizedReader(reader);
            IPEGRun run = _pegiepeg.NewRun(bReader);
            Compiler comp = new Compiler(run, renderer);

            if (!run.Execute())
                throw new PEGBuildException("Syntax error after: " + Environment.NewLine + bReader.GetBuffer());
            return comp.Finalize();
        }

        /// <summary>
        /// Parses the PEG described in a <see cref="String"/>.
        /// </summary>
        /// <param name="stream">The PEG definition.</param>
        /// <param name="renderer">The renderer that will be use for the PEG generation.</param>
        /// <returns>The parsed PEG.</returns>
        public static IPEG ParsePEG(string stream, IPEGRenderer renderer)
        {
            return ParsePEG(ReaderHelper.NewStringReader(stream), renderer);
        }

        /// <summary>
        /// Parses the PEG described in a file.
        /// </summary>
        /// <param name="fileName">The name of the file where the PEG is defined.</param>
        /// <param name="renderer">The renderer that will be use for the PEG generation.</param>
        /// <returns>The parsed PEG.</returns>
        public static IPEG ParsePEGFromFile(string fileName, IPEGRenderer renderer)
        {
            return ParsePEG(ReaderHelper.NewStreamReader(fileName), renderer);
        }

        /// <summary>
        /// Saves the peg into a stream for fast loading.
        /// </summary>
        /// <param name="output">The output stream.</param>
        /// <param name="peg">The peg.</param>
        public static void SavePEG(Stream output, IPEG peg)
        {
            IFormatter s = new BinaryFormatter();

            s.Serialize(output, peg);
        }

        /// <summary>
        /// Fast loads the peg from a stream, the peg must have been saved using the <see cref="SavePEG"/> method.
        /// </summary>
        /// <param name="input">The input stream.</param>
        /// <returns>The loaded peg.</returns>
        public static IPEG LoadPEG(Stream input)
        {
            IFormatter s = new BinaryFormatter();

            return (IPEG)s.Deserialize(input);
        }


        #region Compiler

        private class Compiler
        {
            private HookDispatcher _dispatcher;
            private Stack<INode> _nodes;
            private PEGBuilder _builder;
            private HashSet<UnicodeCategory> _unicodeCategories;
            private List<string> _hookParams;

            public Compiler(IPEGRun run, IPEGRenderer renderer)
            {
                _dispatcher = new HookDispatcher();
                _nodes = new Stack<INode>();
                _builder = new PEGBuilder(renderer);
                _unicodeCategories = new HashSet<UnicodeCategory>();
                _hookParams = new List<string>();

                //run.Hook += run_Hook;
                run.Hook += _dispatcher.OnHook;
                _dispatcher["newRange"] += run_newRange;
                _dispatcher["newSequence"] += run_newSequence;
                _dispatcher["newParam"] += run_newParam;
                _dispatcher["newHook"] += run_newHook;
                _dispatcher["newUnicodeCategory"] += run_newUnicodeCategory;
                _dispatcher["newIgnoreBlanks"] += run_newIgnoreBlanks;
                _dispatcher["newIgnoreCase"] += run_newIgnoreCase;
                _dispatcher["newEndOfLineStyle"] += run_newEndOfLineStyle;
                _dispatcher["newBlanksDefinition"] += run_newBlanksDefinition;
                _dispatcher["newCapture"] += run_newCapture;
                _dispatcher["newRuleCall"] += run_newRuleCall;
                _dispatcher["newUnary"] += run_newUnary;
                _dispatcher["newAnd"] += run_newAnd;
                _dispatcher["newBinary"] += run_newBinary;
                _dispatcher["newRule"] += run_newRule;

                _builder.NewRule("any").Tree = new Any();
                _builder.NewRule("eos").Tree = new EndOfStream();
                _builder.NewRule("eol").Tree = new EndOfLine();
            }

            void run_Hook(object sender, HookEventArg e)
            {
                StringBuilder output = new StringBuilder();

                output.AppendFormat("#{0}", e.HookName);
                if (e.HookArgs.Length > 0)
                {
                    output.Append('(');
                    for (int i = 0; i < e.HookArgs.Length; i++)
                    {
                        if (i > 0)
                            output.Append(", ");
                        output.Append(e.HookArgs[i]);
                    }
                    output.Append(')');
                }
                Console.WriteLine(output.ToString());
            }

            public IPEG Finalize()
            {
                return _builder.Finalize();
            }

            private void run_newRule(object sender, HookEventArg e)
            {
                string ruleName = e.HookArgs[0].Substring(1, e.HookArgs[0].Length - 2);
                bool isStart = ruleName[0] == '<';

                if (isStart)
                    ruleName = ruleName.Substring(1, ruleName.Length - 2);

                RuleBuilder rule = _builder.NewRule(ruleName);
                rule.Tree = _nodes.Pop();

                if (isStart)
                    _builder.StartingRule = rule;
            }

            private void run_newBinary(object sender, HookEventArg e)
            {
                INode right = _nodes.Pop();
                INode left = _nodes.Pop();

                switch (e.HookArgs[0])
                {
                    case "|":
                        _nodes.Push(new Or(left, right));
                        break;
                    case "-":
                        _nodes.Push(new Except(left, right));
                        break;
                    default:
                        throw new PEGBuildException("This should not happen.");
                }
            }

            private void run_newAnd(object sender, HookEventArg e)
            {
                INode right = _nodes.Pop();
                INode left = _nodes.Pop();

                _nodes.Push(new And(left, right));
            }

            private void run_newUnary(object sender, HookEventArg e)
            {
                INode node = _nodes.Pop();

                switch (e.HookArgs[0])
                {
                    case "+":
                        _nodes.Push(new OneOrMany(node));
                        break;
                    case "*":
                        _nodes.Push(new NoneOrMany(node));
                        break;
                    case "?":
                        _nodes.Push(new OneOrNone(node));
                        break;
                    default:
                        throw new PEGBuildException("This should not happen.");
                }
            }

            private void run_newRuleCall(object sender, HookEventArg e)
            {
                string ruleName = e.HookArgs[0].Substring(1, e.HookArgs[0].Length - 2);
                _nodes.Push(new RuleCall(ruleName));
            }

            private void run_newCapture(object sender, HookEventArg e)
            {
                _nodes.Push(new Capture(_nodes.Pop(), e.HookArgs[0]));
            }

            private void run_newBlanksDefinition(object sender, HookEventArg e)
            {
                _nodes.Push(new BlanksDefinition(_unicodeCategories.ToArray()));
                _unicodeCategories.Clear();
            }

            private void run_newEndOfLineStyle(object sender, HookEventArg e)
            {
                _nodes.Push(new EndOfLineStyle(EnumHelper.ParseEnum<EndOfLineStyle.EolStyle>(e.HookArgs[0])));
            }

            private void run_newIgnoreCase(object sender, HookEventArg e)
            {
                _nodes.Push(new IgnoreCase(Boolean.Parse(e.HookArgs[0])));
            }

            private void run_newIgnoreBlanks(object sender, HookEventArg e)
            {
                _nodes.Push(new IgnoreBlanks(Boolean.Parse(e.HookArgs[0])));
            }

            private void run_newUnicodeCategory(object sender, HookEventArg e)
            {
                _unicodeCategories.Add(EnumHelper.ParseEnum<UnicodeCategory>(e.HookArgs[0]));
            }

            private void run_newHook(object sender, HookEventArg e)
            {
                _nodes.Push(new Hook(e.HookArgs[0], _hookParams.ToArray()));
                _hookParams.Clear();
            }

            private void run_newParam(object sender, HookEventArg e)
            {
                _hookParams.Add(e.HookArgs[0]);
            }

            private void run_newSequence(object sender, HookEventArg e)
            {
                _nodes.Push(new Sequence(AsciiFormater(e.HookArgs[0])));
            }

            private void run_newRange(object sender, HookEventArg e)
            {
                _nodes.Push(new Range(AsciiFormater(e.HookArgs[0])[0], AsciiFormater(e.HookArgs[1])[0]));
            }

            private static Regex hexaRegex = new Regex(@"\\x((([0-9]|[a-f]){2}))", RegexOptions.IgnoreCase);
            private static Regex octRegex = new Regex(@"\\o(([0-7]{3}))", RegexOptions.IgnoreCase);

            private static string AsciiFormater(string value)
            {
                string output = octRegex.Replace(hexaRegex.Replace(value, ReplaceHex), ReplaceOct);

                return output;
            }

            private static string ReplaceHex(Match match)
            {
                return ((char)Int16.Parse(match.Groups[1].Value, NumberStyles.HexNumber)).ToString();
            }

            private static string ReplaceOct(Match match)
            {
                return ((char)Convert.ToInt16(match.Groups[1].Value, 8)).ToString();
            }
        }

        #endregion
    }
}
