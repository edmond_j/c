﻿/*
Copyright Julien Edmond (05/06/2014) 

edmondju@gmail.com

This library is a .NET library whose purpose is to help developpers in
diffent ways.

This software is governed by the CeCILL-C  license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PEGIE.StringRenderer
{
    /// <summary>
    /// Renderer decorator that will write the PEG inside a <see cref="StringBuilder"/>.
    /// </summary>
    public sealed class StringRenderer : IPEGRenderer
    {
        private delegate void NodeRenderer(INode node);

        private IPEGRenderer _realRenderer;
        private StringBuilder _output;
        private Dictionary<Type, NodeRenderer> _renderers;

        /// <summary>
        /// Initializes a new instance of the <see cref="StringRenderer"/> class.
        /// </summary>
        /// <param name="output">The output.</param>
        /// <param name="realRenderer">The real renderer.</param>
        public StringRenderer(StringBuilder output, IPEGRenderer realRenderer)
        {
            _output = output;
            _realRenderer = realRenderer;
            _renderers = new Dictionary<Type, NodeRenderer>()
            {
                {typeof(And), this._RendererAnd},
                {typeof(Any), this._RendererAny},
                {typeof(BlanksDefinition), this._RendererBlanksDefinition},
                {typeof(Capture), this._RendererCapture},
                {typeof(EndOfLine), this._RendererEol},
                {typeof(EndOfLineStyle), this._RendererEolStyle},
                {typeof(EndOfStream), this._RendererEos},
                {typeof(Hook), this._RendererHook},
                {typeof(IgnoreBlanks), this._RendererIgnoreBlanks},
                {typeof(IgnoreCase), this._RendererIgnoreCase},
                {typeof(NoneOrMany), this._RendererNoneOrMany},
                {typeof(OneOrMany), this._RendererOneOrMany},
                {typeof(OneOrNone), this._RendererOneOrNone},
                {typeof(Or), this._RendererOr},
                {typeof(RuleCall), this._RendererRuleCall},
                {typeof(Sequence), this._RendererSequence},
                {typeof(Except), this._RenderExcept},
                {typeof(Range), this._RenderRange}
            };
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="StringRenderer"/> class using the default renderer.
        /// </summary>
        /// <param name="output">The output.</param>
        public StringRenderer(StringBuilder output)
            : this(output, PEGBuilder.GetDefaultRenderer())
        { }

        /// <summary>
        /// Renders the specified builder.
        /// </summary>
        /// <param name="builder">The builder.</param>
        /// <returns>
        /// The rendered <see cref="IPEG" />
        /// </returns>
        public IPEG Render(PEGBuilder builder)
        {
            foreach (KeyValuePair<string, RuleBuilder> rule in builder.Rules)
            {

                _output.Append(rule.Value == builder.StartingRule ? "<<" : "<").Append(rule.Key).Append(rule.Value == builder.StartingRule ? ">> := " : "> := ");
                _RenderNode(rule.Value.Tree);
                _output.AppendLine(";");
            }
            return _realRenderer.Render(builder);
        }

        private void _RenderNode(INode node)
        {
            this._renderers[node.GetType()](node);
        }

        private void _RendererAnd(INode node)
        {
            And and = (And)node;

            _output.Append('(');
            _RenderNode(and.Left);
            _output.Append(' ');
            _RenderNode(and.Right);
            _output.Append(')');
        }

        private void _RendererAny(INode node)
        {
            _output.Append("<any>");
        }

        private void _RendererBlanksDefinition(INode node)
        {
            BlanksDefinition bd = (BlanksDefinition)node;

            _output.Append("@blanks_definition(");
            for (int i = 0; i < bd.Categories.Length; ++i)
            {
                if (i > 0)
                    _output.Append(", ");
                _output.Append(bd.Categories[i].ToString());
            }
            _output.Append(')');
        }

        private void _RendererCapture(INode node)
        {
            Capture cap = (Capture)node;

            _output.Append('(');
            _RenderNode(cap.Node);
            _output.Append("):").Append(cap.Name);
        }

        private void _RendererEol(INode node)
        {
            _output.Append("<eol>");
        }

        private void _RendererEos(INode node)
        {
            _output.Append("<eos>");
        }

        private void _RendererEolStyle(INode node)
        {
            _output.Append("@eol_style(").Append(((EndOfLineStyle)node).Value.ToString()).Append(')');
        }

        private void _RenderExcept(INode node)
        {
            Except except = (Except)node;

            _output.Append('(');
            _RenderNode(except.Left);
            _output.Append(" - ");
            _RenderNode(except.Right);
            _output.Append(')');
        }

        private void _RendererHook(INode node)
        {
            Hook hook = (Hook)node;


            _output.Append('#').Append(hook.Name).Append('(');
            foreach (string str in hook.Args)
	        {
                _output.Append(str);
	        }
            _output.Append(')');
        }

        private void _RendererIgnoreBlanks(INode node)
        {
            _output.Append("@ignore_blanks(").Append(((IgnoreBlanks)node).Value).Append(')');
        }

        private void _RendererIgnoreCase(INode node)
        {
            _output.Append("@ignore_case(").Append(((IgnoreCase)node).Value).Append(')');
        }

        private void _RendererNoneOrMany(INode node)
        {
            NoneOrMany nom = (NoneOrMany)node;

            _output.Append('(');
            _RenderNode(nom.Node);
            _output.Append(")*");
        }

        private void _RendererOneOrMany(INode node)
        {
            OneOrMany oom = (OneOrMany)node;

            _output.Append('(');
            _RenderNode(oom.Node);
            _output.Append(")+");
        }

        private void _RendererOneOrNone(INode node)
        {
            OneOrNone oon = (OneOrNone)node;

            _output.Append('(');
            _RenderNode(oon.Node);
            _output.Append(")?");
        }

        private void _RendererOr(INode node)
        {
            Or or = (Or)node;

            _output.Append('(');
            _RenderNode(or.Left);
            _output.Append(" | ");
            _RenderNode(or.Right);
            _output.Append(')');
        }

        private void _RenderRange(INode node)
        {
            Range range = (Range)node;

            _output.Append('[').Append(range.From).Append('-').Append(range.To).Append(']');
        }
        
        private void _RendererSequence(INode node)
        {
            Sequence sq = (Sequence)node;

            _output.Append('"').Append(sq.Value).Append('"');
        }

        private void _RendererRuleCall(INode node)
        {
            RuleCall rc = (RuleCall)node;

            _output.Append('<').Append(rc.Rule).Append('>');
        }
    }
}
