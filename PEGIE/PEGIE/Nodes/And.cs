﻿/*
Copyright Julien Edmond (05/06/2014) 

edmondju@gmail.com

This library is a .NET library whose purpose is to help developpers in
diffent ways.

This software is governed by the CeCILL-C  license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/
using My;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PEGIE
{
    /// <summary>
    /// And
    /// </summary>
    public class And : AbstractNode
    {
        private INode _left;
        private INode _right;

        /// <summary>
        /// Makes a chain of and.
        /// </summary>
        /// <param name="left">The left node.</param>
        /// <param name="right">The right node.</param>
        /// <param name="nodes">The nodes to come.</param>
        /// <returns>A chain of ands between all these nodes.</returns>
        public static INode MakeAnd(INode left, INode right, params INode[] nodes)
        {
            if (nodes.Length == 0)
                return new And(left, right);
            return MakeAnd(new And(left, right), nodes[0], nodes.SubArray(1));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="And"/> class.
        /// </summary>
        /// <param name="left">The left node.</param>
        /// <param name="right">The right node.</param>
        public And(INode left, INode right)
        {
            _left = left;
            _right = right;
        }

        /// <summary>
        /// Gets the right node.
        /// </summary>
        /// <value>
        /// The right node.
        /// </value>
        public INode Right
        {
            get { return _right; }
        }

        /// <summary>
        /// Gets the left node.
        /// </summary>
        /// <value>
        /// The left node.
        /// </value>
        public INode Left
        {
            get { return _left; }
        }

        /// <summary>
        /// Validates the specified rules.
        /// </summary>
        /// <param name="rules">The rules defined in the builder.</param>
        public override void Validate(IEnumerable<string> rules)
        {
            _left.Validate(rules);
            _right.Validate(rules);
        }
    }
}
