﻿/*
Copyright Julien Edmond (05/06/2014) 

edmondju@gmail.com

This library is a .NET library whose purpose is to help developpers in
diffent ways.

This software is governed by the CeCILL-C  license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PEGIE
{
    /// <summary>
    /// Abstract class representing a <see cref="INode"/>
    /// </summary>
    public abstract class AbstractNode : INode
    {
        /// <summary>
        /// Implements the operator &amp;.
        /// </summary>
        /// <param name="left">The left node.</param>
        /// <param name="right">The right node.</param>
        /// <returns>
        /// A node corresponding to left and right.
        /// </returns>
        public static AbstractNode operator&(AbstractNode left, AbstractNode right)
        {
            return new And(left, right);
        }

        /// <summary>
        /// Implements the operator |.
        /// </summary>
        /// <param name="left">The left node.</param>
        /// <param name="right">The right node.</param>
        /// <returns>
        /// A node corresponding to left or right.
        /// </returns>
        public static AbstractNode operator|(AbstractNode left, AbstractNode right)
        {
            return new Or(left, right);
        }

        /// <summary>
        /// Implements the operator -.
        /// </summary>
        /// <param name="left">The left node.</param>
        /// <param name="right">The right node.</param>
        /// <returns>
        /// A node corresponding to left except right.
        /// </returns>
        public static AbstractNode operator -(AbstractNode left, AbstractNode right)
        {
            return new Except(left, right);
        }

        /// <summary>
        /// Implements the operator *.
        /// </summary>
        /// <param name="left">The left node.</param>
        /// <param name="repeat">The number of time it must be repeated.</param>
        /// <returns>
        /// A node corresponding to left repeated <paramref name="repeat"/> times.
        /// </returns>
        /// <exception cref="System.InvalidOperationException">if repeat is lesser than or equal to 0.</exception>
        public static AbstractNode operator*(AbstractNode left, int repeat)
        {
            if (repeat <= 0)
                throw new InvalidOperationException();
            if (repeat == 1)
                return left;
            return new And(left, left * (repeat - 1));
        }

        /// <summary>
        /// Validates the specified rules.
        /// </summary>
        /// <param name="rules">The rules defined in the builder.</param>
        public virtual void Validate(IEnumerable<string> rules)
        { }
    }
}
