﻿/*
Copyright Julien Edmond (05/06/2014) 

edmondju@gmail.com

This library is a .NET library whose purpose is to help developpers in
diffent ways.

This software is governed by the CeCILL-C  license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/
using My;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PEGIE.StateMachineRenderer
{
    /* INCOMPLETE
    internal class TransitionContext : ICloneable<TransitionContext>, IEquatable<TransitionContext>
    {
        private bool _ignoreBlanks;
        private bool _ignoreCase;
        private EndOfLineStyle.EolStyle _eolStyle;

        public TransitionContext()
        {
            _ignoreBlanks = true;
            _ignoreCase = false;
            _eolStyle = (Environment.OSVersion.Platform == PlatformID.MacOSX || Environment.OSVersion.Platform == PlatformID.Unix
            ? EndOfLineStyle.EolStyle.Unix
            : EndOfLineStyle.EolStyle.Windows);
        }

        public TransitionContext(TransitionContext other)
        {
             _ignoreBlanks = other._ignoreBlanks;
            _ignoreCase = other._ignoreCase;
            _eolStyle = other._eolStyle;
        }

        public EndOfLineStyle.EolStyle EolStyle
        {
            get { return _eolStyle; }
            set { _eolStyle = value; }
        }

        public bool IgnoreCase
        {
            get { return _ignoreCase; }
            set { _ignoreCase = value; }
        }

        public bool IgnoreBlanks
        {
            get { return _ignoreBlanks; }
            set { _ignoreBlanks = value; }
        }

        public TransitionContext Clone()
        {
            return new TransitionContext(this);
        }

        public bool Equals(TransitionContext other)
        {
            return _ignoreBlanks == other._ignoreBlanks
                && _ignoreCase == other._ignoreCase
                && _eolStyle == other._eolStyle;
        }
    }
    */
}
