﻿/*
Copyright Julien Edmond (05/06/2014) 

edmondju@gmail.com

This library is a .NET library whose purpose is to help developpers in
diffent ways.

This software is governed by the CeCILL-C  license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using My.StateMachine;

namespace PEGIE.StateMachineRenderer
{
    /* INCOMPLETE
    internal class StateMachineRenderer : IPEGRenderer
    {
        private class StateMachineNode
        {
            public IStateBuilder<int> Entry;
            public IStateBuilder<int> Exit;
        }

        private delegate StateMachineNode NodeRenderer(INode node, IBaseStateMachineBuilder<int> builder, TransitionContext context);

        private StateMachineBuilder<int> _machineBuilder;
        private Dictionary<string, IBaseStateMachineBuilder<int>> _rules;
        private Dictionary<Type, NodeRenderer> _renderers;

        public StateMachineRenderer()
        {
            _machineBuilder = new StateMachineBuilder<int>();
            _rules = new Dictionary<string, IBaseStateMachineBuilder<int>>();
            _renderers = new Dictionary<Type, NodeRenderer>()
            {
                {typeof(And), this._RendererAnd},
                {typeof(Any), this._RendererAny},
                {typeof(Capture), this._RendererCapture},
                {typeof(EndOfLine), this._RendererEol},
                {typeof(EndOfLineStyle), this._RendererEolStyle},
                {typeof(EndOfStream), this._RendererEos},
                {typeof(Hook), this._RendererHook},
                {typeof(IgnoreBlanks), this._RendererIgnoreBlanks},
                {typeof(IgnoreCase), this._RendererIgnoreCase},
                {typeof(NoneOrMany), this._RendererNoneOrMany},
                {typeof(OneOrMany), this._RendererOneOrMany},
                {typeof(OneOrNone), this._RendererOneOrNone},
                {typeof(Or), this._RendererOr},
                {typeof(RuleCall), this._RendererRuleCall},
                {typeof(Sequence), this._RendererSequence},
                {typeof(Except), this._RendererExcept},
                {typeof(Range), this._RendererRange}
            };
        }

        public IPEG Render(PEGBuilder builder)
        {
            foreach (KeyValuePair<string, RuleBuilder> rule in builder.Rules)
            {
                _rules[rule.Key] = _machineBuilder.CreateSubmachine();
            }

            foreach (KeyValuePair<string, RuleBuilder> rule in builder.Rules)
            {
                StateMachineNode node = _RenderNode(rule.Value.Tree, _rules[rule.Key], new TransitionContext());

                _rules[rule.Key].InitialState = node.Entry;
                node.Exit.IsFinal = true;
            }

            IPlaceHolder<int> start = _machineBuilder.CreatePlaceHolder(_rules[builder.StartingRule.Name]);
            _machineBuilder.InitialState = start;
            start.IsFinal = true;

            IStateMachine<int> machine = _machineBuilder.Finalize();

            return new StateMachinePEG(machine);
        }

        private void _HandleIgnoreBlanks(IStateBuilder<int> node)
        {
            node.AddTransition(' ', node);
            node.AddTransition('\t', node);
        }

        private void _AddCharTransition(IStateBuilder<int> from, IStateBuilder<int> to, char c, TransitionContext context)
        {
            if (!context.IgnoreCase)
                from.AddTransition(c, to);
            else
            {
                from.AddTransition(System.Char.ToLower(c), to);
                from.AddTransition(System.Char.ToUpper(c), to);
            }
        }

        private StateMachineNode _RenderNode(INode node, IBaseStateMachineBuilder<int> builder, TransitionContext context)
        {
            return this._renderers[node.GetType()](node, builder, context);
        }

        private StateMachineNode _RendererAnd(INode node, IBaseStateMachineBuilder<int> builder, TransitionContext context)
        {
            And and = (And)node;
            StateMachineNode left = _RenderNode(and.Left, builder, context);
            StateMachineNode right = _RenderNode(and.Right, builder, context);

            left.Exit.AddEpsilonTransition(right.Entry);
            return new StateMachineNode() { Entry = left.Entry, Exit = right.Exit };
        }

        private StateMachineNode _RendererAny(INode node, IBaseStateMachineBuilder<int> builder, TransitionContext context)
        {
            IStateBuilder<int> entry = builder.CreateState();
            IStateBuilder<int> exit = builder.CreateState();

            if (context.IgnoreBlanks)
                _HandleIgnoreBlanks(entry);
            for (int i = System.Char.MinValue; i < System.Char.MaxValue; ++i)
            {
                entry.AddTransition(i, exit);
            }
            return new StateMachineNode() { Entry = entry, Exit = exit };
        }

        private StateMachineNode _RendererCapture(INode node, IBaseStateMachineBuilder<int> builder, TransitionContext context)
        {
            StateMachineNode sent = _RenderNode(node, builder, context);

            // TODO Add handlers for capture
            return sent;
        }

        private StateMachineNode _RendererEol(INode node, IBaseStateMachineBuilder<int> builder, TransitionContext context)
        {
            IStateBuilder<int> entry = builder.CreateState();
            IStateBuilder<int> exit = builder.CreateState();

            entry.AddTransition('\n', exit);
            if (context.EolStyle == EndOfLineStyle.EolStyle.Windows)
            {
                IStateBuilder<int> tmp = builder.CreateState();

                tmp.AddTransition('\r', entry);
                entry = tmp;
            }
            if (context.IgnoreBlanks)
                _HandleIgnoreBlanks(entry);
            return new StateMachineNode() { Entry = entry, Exit = exit };
        }

        private StateMachineNode _RendererEolStyle(INode node, IBaseStateMachineBuilder<int> builder, TransitionContext context)
        {
            IStateBuilder<int> dummy = builder.CreateState();

            context.EolStyle = ((EndOfLineStyle)node).Value;
            return new StateMachineNode() { Entry = dummy, Exit = dummy };
        }

        private StateMachineNode _RendererEos(INode node, IBaseStateMachineBuilder<int> builder, TransitionContext context)
        {
            IStateBuilder<int> entry = builder.CreateState();
            IStateBuilder<int> exit = builder.CreateState();

            entry.AddTransition(-1, exit);
            if (context.IgnoreBlanks)
                _HandleIgnoreBlanks(entry);
            return new StateMachineNode() { Entry = entry, Exit = exit };
        }

        private StateMachineNode _RendererHook(INode node, IBaseStateMachineBuilder<int> builder, TransitionContext context)
        {
            StateMachineNode sent = _RenderNode(node, builder, context);

            // TODO Add handlers for hook
            return sent;
        }

        private StateMachineNode _RendererIgnoreBlanks(INode node, IBaseStateMachineBuilder<int> builder, TransitionContext context)
        {
            IStateBuilder<int> dummy = builder.CreateState();

            context.IgnoreBlanks = ((IgnoreBlanks)node).Value;
            return new StateMachineNode() { Entry = dummy, Exit = dummy };
        }

        private StateMachineNode _RendererIgnoreCase(INode node, IBaseStateMachineBuilder<int> builder, TransitionContext context)
        {
            IStateBuilder<int> dummy = builder.CreateState();

            context.IgnoreCase = ((IgnoreBlanks)node).Value;
            return new StateMachineNode() { Entry = dummy, Exit = dummy };
        }

        private StateMachineNode _RendererNoneOrMany(INode node, IBaseStateMachineBuilder<int> builder, TransitionContext context)
        {
            NoneOrMany nom = (NoneOrMany)node;
            StateMachineNode sent = _RenderNode(nom.Node, builder, context);

            sent.Exit.AddEpsilonTransition(sent.Entry);
            sent.Exit = sent.Entry;
            return sent;
        }

        private StateMachineNode _RendererOneOrMany(INode node, IBaseStateMachineBuilder<int> builder, TransitionContext context)
        {
            OneOrMany oom = (OneOrMany)node;
            StateMachineNode sent = _RenderNode(oom.Node, builder, context);

            sent.Exit.AddEpsilonTransition(sent.Entry);
            return sent;
        }

        private StateMachineNode _RendererOneOrNone(INode node, IBaseStateMachineBuilder<int> builder, TransitionContext context)
        {
            OneOrNone oon = (OneOrNone)node;
            StateMachineNode sent = _RenderNode(oon.Node, builder, context);
            IStateBuilder<int> newExit = builder.CreateState();

            sent.Entry.AddEpsilonTransition(newExit);
            sent.Exit.AddEpsilonTransition(newExit);
            sent.Exit = newExit;
            return sent;
        }

        private StateMachineNode _RendererOr(INode node, IBaseStateMachineBuilder<int> builder, TransitionContext context)
        {
            IStateBuilder<int> entry = builder.CreateState();
            IStateBuilder<int> exit = builder.CreateState();
            Or or = (Or)node;
            StateMachineNode left = _RenderNode(or.Left, builder, context.Clone());
            StateMachineNode right = _RenderNode(or.Right, builder, context.Clone());

            entry.AddEpsilonTransition(left.Entry);
            entry.AddEpsilonTransition(right.Entry);
            left.Exit.AddEpsilonTransition(exit);
            right.Exit.AddEpsilonTransition(exit);
            return new StateMachineNode() { Entry = entry, Exit = exit };
        }

        private StateMachineNode _RendererRuleCall(INode node, IBaseStateMachineBuilder<int> builder, TransitionContext context)
        {
            // TODO Push context on stack?
            IStateBuilder<int> placeholder = builder.CreatePlaceHolder(_rules[((RuleCall)node).Rule]);

            return new StateMachineNode() { Entry = placeholder, Exit = placeholder };
        }

        private StateMachineNode _RendererSequence(INode node, IBaseStateMachineBuilder<int> builder, TransitionContext context)
        {
            Sequence sq = (Sequence)node;
            IStateBuilder<int> entry = builder.CreateState();
            IStateBuilder<int> last = entry;

            if (context.IgnoreBlanks)
                _HandleIgnoreBlanks(entry);
            for (int i = 0; i < sq.Value.Length; ++i)
            {
                IStateBuilder<int> tmp = builder.CreateState();
                _AddCharTransition(last, tmp, sq.Value[i], context);
                last = tmp;
            }
            return new StateMachineNode() { Entry = entry, Exit = last };
        }

        private StateMachineNode _RendererExcept(INode node, IBaseStateMachineBuilder<int> builder, TransitionContext context)
        {
            // TODO Render except
            return _RenderNode(((Except)node).Left, builder, context);
        }

        private StateMachineNode _RendererRange(INode node, IBaseStateMachineBuilder<int> builder, TransitionContext context)
        {
            Range rng = (Range)node;
            IStateBuilder<int> entry = builder.CreateState();
            IStateBuilder<int> exit = builder.CreateState();

            if (context.IgnoreBlanks)
                _HandleIgnoreBlanks(entry);
            for (char i = rng.From; i <= rng.To; ++i)
            {
                _AddCharTransition(entry, exit, i, context);
            }
            return new StateMachineNode() { Entry = entry, Exit = exit };
        }
    }
    */
}
