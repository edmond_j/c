﻿/*
Copyright Julien Edmond (05/06/2014) 

edmondju@gmail.com

This library is a .NET library whose purpose is to help developpers in
diffent ways.

This software is governed by the CeCILL-C  license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace PEGIE
{
    /// <summary>
    /// Object that builds a PEG.
    /// </summary>
    public class PEGBuilder
    {
        private IPEGRenderer _renderer;
        private Dictionary<string, RuleBuilder> _rules;
        private RuleBuilder _startingRule;

        private static Regex _ruleNameValidator = new Regex("([a-z]|[0-9]|_)+", RegexOptions.IgnoreCase);


        /// <summary>
        /// Initializes a new instance of the <see cref="PEGBuilder"/> class.
        /// </summary>
        /// <param name="renderer">The renderer that will be used to finalize the PEG.</param>
        public PEGBuilder(IPEGRenderer renderer)
        {
            _renderer = renderer;
            _rules = new Dictionary<string, RuleBuilder>();
            _startingRule = null;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PEGBuilder"/> class using the default renderer.
        /// </summary>
        public PEGBuilder() : this(GetDefaultRenderer())
        {

        }

        /// <summary>
        /// Gets the rules defined inside the PEG.
        /// </summary>
        /// <value>
        /// The rules defined inside the PEG.
        /// </value>
        public IReadOnlyDictionary<string, RuleBuilder> Rules
        {
            get { return _rules; }
        }

        /// <summary>
        /// Gets or sets the starting rule.
        /// </summary>
        /// <value>
        /// The starting rule.
        /// </value>
        public RuleBuilder StartingRule
        {
            get { return _startingRule; }
            set { _startingRule = value; }
        }

        /// <summary>
        /// Create a new rule.
        /// </summary>
        /// <param name="name">The name of the new rule.</param>
        /// <returns>A <see cref="RuleBuilder"/> corresponding to the newly created rule.</returns>
        public RuleBuilder NewRule(string name)
        {
            _ValidateRuleName(name);

            RuleBuilder sent = new RuleBuilder(name);

            _rules[name] = sent;
            return sent;
        }

        private void _ValidateRuleName(string name)
        {
            if (!_ruleNameValidator.IsMatch(name))
                throw new PEGBuildException("Badly formatted rule name: " + name);
            if (_rules.ContainsKey(name))
                throw new PEGBuildException("Duplicate rule " + name);
        }

        /// <summary>
        /// Finalizes the build.
        /// </summary>
        /// <returns>The newly created IPEG.</returns>
        public IPEG Finalize()
        {
            this.Validate();
            return _renderer.Render(this);
        }

        /// <summary>
        /// Gets the default renderer.
        /// </summary>
        /// <returns>The default renderer.</returns>
        public static IPEGRenderer GetDefaultRenderer()
        {
            return new VMRenderer.VMPEGRenderer();
            //return new TreeRenderer.TreeRenderer();
        }

        /// <summary>
        /// Validates this instance.
        /// </summary>
        public void Validate()
        {
            foreach (RuleBuilder rule in _rules.Values)
                rule.Tree.Validate(_rules.Keys);
            if (this._startingRule == null)
                throw new PEGBuildException("No starting rule defined.");
        }
    }
}
