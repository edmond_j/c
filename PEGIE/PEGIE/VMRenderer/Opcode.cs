﻿/*
Copyright Julien Edmond (05/06/2014) 

edmondju@gmail.com

This library is a .NET library whose purpose is to help developpers in
diffent ways.

This software is governed by the CeCILL-C  license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PEGIE.VMRenderer
{
    internal struct Opcode
    {
        public byte Code;
        public bool RequireInput;

        public readonly static Opcode Char = new Opcode() { Code = 0, RequireInput = true };
        public readonly static Opcode Range = new Opcode() { Code = 1, RequireInput = true };
        public readonly static Opcode Match = new Opcode() { Code = 2 };
        public readonly static Opcode Jmp = new Opcode() { Code = 3 };
        public readonly static Opcode Split = new Opcode() { Code = 4 };
        public readonly static Opcode Call = new Opcode() { Code = 5 };
        public readonly static Opcode Except = new Opcode() { Code = 6 };
        public readonly static Opcode Any = new Opcode() { Code = 7, RequireInput = true };
        public readonly static Opcode CaptureStart = new Opcode() { Code = 8 };
        public readonly static Opcode CaptureEnd = new Opcode() { Code = 9 };
        public readonly static Opcode Hook = new Opcode() { Code = 10 };
        public readonly static Opcode Blank = new Opcode() { Code = 11, RequireInput = true };
        public readonly static Opcode Eos = new Opcode() { Code = 12, RequireInput = true };
        public readonly static Opcode Jmpnb = new Opcode() { Code = 13 };
        public readonly static Opcode AntiMatch = new Opcode() { Code = 14 };

        public readonly static List<Opcode> Opcodes = new List<Opcode>()
        {
            Char,
            Range,
            Match,
            Jmp,
            Split,
            Call,
            Except,
            Any,
            CaptureStart,
            CaptureEnd,
            Hook,
            Blank,
            Eos,
            Jmpnb,
            AntiMatch
        };

        public static implicit operator byte(Opcode c)
        {
            return c.Code;
        }

        public static Opcode Decode(byte b)
        {
            return Opcodes[(int)b];
        }
    }
}
