﻿/*
Copyright Julien Edmond (05/06/2014) 

edmondju@gmail.com

This library is a .NET library whose purpose is to help developpers in
diffent ways.

This software is governed by the CeCILL-C  license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PEGIE.VMRenderer
{
    internal class VMBinaryStream
    {
        private byte[] _bytes;
        private uint _length;
        private List<VMBinaryLabel> _labels;
        private List<KeyValuePair<uint, VMBinaryLabel>> _toSolve;

        public VMBinaryStream()
            : this(1)
        {
        }

        public VMBinaryStream(int capacity)
        {
            this._bytes = new byte[capacity];
            this._length = 0;
            this._labels = new List<VMBinaryLabel>();
            this._toSolve = new List<KeyValuePair<uint, VMBinaryLabel>>();
        }

        public byte[] RetrieveBytes()
        {
            byte[] sent = new byte[this._length];

            Array.Copy(this._bytes, sent, this._length);

            foreach (KeyValuePair<uint, VMBinaryLabel> toSolve in this._toSolve)
            {
                uint value = toSolve.Value.Value;

                for (int i = 0; i < 4; i++)
                {
                    sent[toSolve.Key + i] = (byte)value;
                    value >>= 8;
                }
            }
            return sent;
        }

        private void EnsureCapacity(uint size)
        {
            if (this._length + size >= this._bytes.Length)
            {
                if (this._length + size >= 2 * this._bytes.Length)
                    this.EnlargeArray(this._length + size);
                else
                    this.EnlargeArray();
            }
        }

        private void EnlargeArray()
        {
            this.EnlargeArray(2 * (uint)this._bytes.Length);
        }

        private void EnlargeArray(uint newSize)
        {
            byte[] newArray = new byte[newSize];

            Array.Copy(this._bytes, newArray, this._bytes.Length);
            this._bytes = newArray;
        }

        public void WriteByte(byte b)
        {
            this.EnsureCapacity(1);
            this._bytes[this._length] = b;
            ++this._length;
        }

        public void WriteInt32(int p)
        {
            this.EnsureCapacity(4);
            for (int i = 0; i < 4; ++i)
            {
                this._bytes[this._length] = (byte)p;
                ++this._length;
                p >>= 8;
            }
        }

        public void WriteBytes(byte[] bytes)
        {
            this.EnsureCapacity((uint)bytes.Length);
            for (int i = 0; i < bytes.Length; i++)
            {
                this._bytes[this._length] = bytes[i];
                ++this._length;
            }
        }

        public void WriteString(string value)
        {
            this.WriteInt32(Encoding.Unicode.GetByteCount(value));
            this.WriteBytes(Encoding.Unicode.GetBytes(value));
        }

        public VMBinaryLabel DefineLabel()
        {
            return DefineLabel(null);
        }

        public VMBinaryLabel DefineLabel(string name)
        {
            VMBinaryLabel sent = new VMBinaryLabel() { Name = name };

            this._labels.Add(sent);
            return sent;
        }

        public void WriteLabel(string name)
        {
            this.WriteLabel(this._FindLabel(name));
        }

        public void WriteLabel(VMBinaryLabel label)
        {
            if (!this._labels.Contains(label))
                throw new InvalidOperationException("Label not defined in this stream."); // TODO Put some custom exception.
            this.EnsureCapacity(4);
            this._toSolve.Add(new KeyValuePair<uint, VMBinaryLabel>(this._length, label));
            this._length += 4;
        }

        public void WriteChar(char c)
        {
            this.WriteBytes(Encoding.Unicode.GetBytes(new string(c, 1)));
        }

        public void MarkLabel(string name)
        {
            this.MarkLabel(this._FindLabel(name));
        }

        public void MarkLabel(VMBinaryLabel label)
        {
            label.Value = this._length;
        }

        private VMBinaryLabel _FindLabel(string name)
        {
            if (!this._labels.Any(l => l.Name == name))
                throw new InvalidOperationException("Label not defined in this stream."); // TODO Put some custom exception.
            return this._labels.Find(l => l.Name == name);
        }

        internal void WriteInt16(int p)
        {
            this.EnsureCapacity(2);
            for (int i = 0; i < 2; ++i)
            {
                this._bytes[this._length] = (byte)p;
                ++this._length;
                p >>= 8;
            }
        }
    }
}
