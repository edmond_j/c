﻿/*
Copyright Julien Edmond (05/06/2014) 

edmondju@gmail.com

This library is a .NET library whose purpose is to help developpers in
diffent ways.

This software is governed by the CeCILL-C  license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using My;

namespace PEGIE.VMRenderer
{
    internal class Thread : ICloneable<Thread>
    {
        public Thread(bool antiThread)
        {
            this.Depth = 0;
            this.Offset = 0;
//            this.Contexts = new Stack<Context>();
            this.Pointers = new List<int>();
            this.Hooks = new List<HookEventArg>();
            this.AntiThread = antiThread;
            this.ExceptId = new Stack<int>();
        }

        public Thread(int startOffset, bool antiThread)
            : this(antiThread)
        {
            this.Offset = startOffset;
//            this.Contexts.Push(new Context(startOffset));
            this.Pointers.Add(startOffset);
        }

        public List<int> Pointers;

        public int Offset;
        public int Depth;
//        public Stack<Context> Contexts;
        public List<HookEventArg> Hooks;
        public bool AntiThread;
        public Stack<int> ExceptId;


//        public Context Context { get { return Contexts.Peek(); } }

        public int InstructionPointer
        {
            get { return this.Pointers[this.Depth]; }
            set { this.Pointers[this.Depth] = value; }
//            get { return this.Contexts.Peek().InstructionPointer; }
//            set { this.Contexts.Peek().InstructionPointer = value; }
        }

        public Thread Clone()
        {
            Thread sent = new Thread(this.AntiThread);

            sent.Depth = this.Depth;
            sent.Offset = this.Offset;
//            foreach (Context context in this.Contexts.Reverse())
//                sent.Contexts.Push(context.Clone());
            sent.Pointers.AddMany(this.Pointers);
            sent.Hooks.AddMany(this.Hooks);
            sent.ExceptId = new Stack<int>(this.ExceptId);
            return sent;
        }
    }
}
