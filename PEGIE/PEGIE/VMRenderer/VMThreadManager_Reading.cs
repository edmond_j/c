﻿/*
Copyright Julien Edmond (05/06/2014) 

edmondju@gmail.com

This library is a .NET library whose purpose is to help developpers in
diffent ways.

This software is governed by the CeCILL-C  license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PEGIE.VMRenderer
{
    internal partial class VMThreadManager
    {
        private int ReadInt32(int offset)
        {
            return this.ReadInt32(ref offset);
        }

        private int ReadInt32(ref int offset)
        {
            int sent = 0;

            for (int i = 0; i < 4; i++)
            {
                sent |= (this._binary[offset] << i * 8);
                ++offset;
            }
            return sent;
        }

        private string ReadString(ref int offset)
        {
            int size = this.ReadInt32(ref offset);
            byte[] bytes;

            bytes = new byte[size];
            for (int i = 0; i < size; i++)
            {
                bytes[i] = this._binary[offset + i];
            }

            offset += size;
            return new string(Encoding.Unicode.GetChars(bytes));
        }

        private char ReadChar(int offset)
        {
            return this.ReadChar(ref offset);
        }

        private char ReadChar(ref int offset)
        {
            using (MemoryStream ms = new MemoryStream(this._binary, offset, 2))
            using (BinaryReader bn = new BinaryReader(ms, Encoding.Unicode))
            {
                offset += 2;
                return bn.ReadChar();
            }
        }
    }
}
