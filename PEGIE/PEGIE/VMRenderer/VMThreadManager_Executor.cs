﻿/*
Copyright Julien Edmond (05/06/2014) 

edmondju@gmail.com

This library is a .NET library whose purpose is to help developpers in
diffent ways.

This software is governed by the CeCILL-C  license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PEGIE.VMRenderer
{
    internal partial class VMThreadManager
    {
        private static bool CharBelongsToCode(char c, int code)
        {
            return (code & (1 << (int)Char.GetUnicodeCategory(c))) != 0;
        }

        private enum ExecutionResult
        {
            Nothing,
            Match,
            Kill
        };

        private delegate ExecutionResult Executor(VMThreadManager manager, Thread thread);
        private delegate ExecutionResult InputExecutor(VMThreadManager manager, Thread thread, int input);

        private static Dictionary<Opcode, Executor> Executors = new Dictionary<Opcode, Executor>()
        {
            {Opcode.Match, ExecuteMatch},
            {Opcode.Jmp, ExecuteJmp},
            {Opcode.Split, ExecuteSplit},
            {Opcode.Call, ExecuteCall},
            {Opcode.Except, ExecuteExcept},
            {Opcode.CaptureStart, ExecuteCaptureStart},
            {Opcode.CaptureEnd, ExecuteCaptureEnd},
            {Opcode.Hook, ExecuteHook},
            {Opcode.Jmpnb, ExecuteJmpnb},
            {Opcode.AntiMatch, ExecuteAntiMatch}
        };

        private static ExecutionResult ExecuteMatch(VMThreadManager manager, Thread thread)
        {
//            thread.Contexts.Pop();
//            if (thread.Contexts.Count <= 0)
//                return ExecutionResult.Match;
            manager.PopContext(thread);
            if (manager.ThreadDone(thread))
                return ExecutionResult.Match;
            return ExecutionResult.Nothing;
        }

        private static ExecutionResult ExecuteJmp(VMThreadManager manager, Thread thread)
        {
            thread.InstructionPointer = manager.ReadInt32(thread.InstructionPointer + 1);
            return ExecutionResult.Nothing;
        }

        private static ExecutionResult ExecuteSplit(VMThreadManager manager, Thread thread)
        {
            int offset = thread.InstructionPointer + 1;
            int left = manager.ReadInt32(ref offset);
            int right = manager.ReadInt32(ref offset);

            manager.CloneThread(thread, right);
            thread.InstructionPointer = left;
            return ExecutionResult.Nothing;
        }

        private static ExecutionResult ExecuteCall(VMThreadManager manager, Thread thread)
        {
            int prev = thread.InstructionPointer + 1;
            int offset = manager.ReadInt32(ref prev);

            thread.InstructionPointer = prev;
            manager.PushContext(thread, offset);
//            thread.Contexts.Push(new Context(offset));
            return ExecutionResult.Nothing;
        }

        private static ExecutionResult ExecuteExcept(VMThreadManager manager, Thread thread)
        {
            int offset = thread.InstructionPointer + 1;
            int left = manager.ReadInt32(ref offset);
            int right = manager.ReadInt32(ref offset);

            manager.CreateAntiThread(thread, right);
            thread.InstructionPointer = left;
            return ExecutionResult.Nothing;
        }

        private static ExecutionResult ExecuteCaptureStart(VMThreadManager manager, Thread thread)
        {
            int offset = thread.InstructionPointer + 1;
            string capture = manager.ReadString(ref offset);

            manager.StartCapture(thread, capture);
//            thread.Context.OnGoingCaptures.Add(capture);
//            thread.Context.Captures[capture] = new StringBuilder();
            thread.InstructionPointer = offset;
            return ExecutionResult.Nothing;
        }

        private static ExecutionResult ExecuteCaptureEnd(VMThreadManager manager, Thread thread)
        {
            int offset = thread.InstructionPointer + 1;
            string capture = manager.ReadString(ref offset);

            manager.EndCapture(thread, capture);
//            thread.Context.OnGoingCaptures.Remove(capture);
            thread.InstructionPointer = offset;
            return ExecutionResult.Nothing;
        }
        
        private static ExecutionResult ExecuteHook(VMThreadManager manager, Thread thread)
        {
            int offset = thread.InstructionPointer + 1;
            string name = manager.ReadString(ref offset);
            int size = manager.ReadInt32(ref offset);
            List<string> param = new List<string>();

            for (int i = 0; i < size; i++)
            {
                string p = manager.ReadString(ref offset);

                p = manager.GetCaptureValue(thread, p);
//                if (thread.Context.Captures.ContainsKey(p))
//                    p = thread.Context.Captures[p].ToString();
                param.Add(p);
            }
            thread.Hooks.Add(new HookEventArg(name, param.ToArray()));
            thread.InstructionPointer = offset;
            return ExecutionResult.Nothing;
        }

        private static ExecutionResult ExecuteJmpnb(VMThreadManager manager, Thread thread)
        {
            int offset = thread.InstructionPointer + 1;
            int code = manager.ReadInt32(ref offset);
            int jmpOffset = manager.ReadInt32(ref offset);

            if (manager._reader.Peek() == -1 || !CharBelongsToCode(Convert.ToChar(manager._reader.Peek()), code))
                thread.InstructionPointer = jmpOffset;
            else
                thread.InstructionPointer = offset;
            return ExecutionResult.Nothing;
        }

        private static ExecutionResult ExecuteAntiMatch(VMThreadManager manager, Thread thread)
        {
            if (thread.AntiThread)
                return ExecutionResult.Match;
            else if (manager.AntithreadMatched(thread))
                return ExecutionResult.Kill;
            thread.InstructionPointer += 1;
            return ExecutionResult.Nothing;
        }

        private static Dictionary<Opcode, InputExecutor> InputExecutors = new Dictionary<Opcode, InputExecutor>()
        {
            {Opcode.Char, ExecuteChar},
            {Opcode.Range, ExecuteRange},
            {Opcode.Any, ExecuteAny},
            {Opcode.Blank, ExecuteBlank},
            {Opcode.Eos, ExecuteEos}
        };

        private static ExecutionResult ExecuteChar(VMThreadManager manager, Thread thread, int input)
        {
            int offset = thread.InstructionPointer + 1;
            char c = manager.ReadChar(ref offset);

            if (input == -1 || c != (char)input)
                return ExecutionResult.Kill;
            thread.InstructionPointer = offset;
            return ExecutionResult.Nothing;
        }

        private static ExecutionResult ExecuteRange(VMThreadManager manager, Thread thread, int input)
        {
            int offset = thread.InstructionPointer + 1;
            char from = manager.ReadChar(ref offset);
            char to = manager.ReadChar(ref offset);

            if (from > input || to < input)
                return ExecutionResult.Kill;
            thread.InstructionPointer = offset;
            return ExecutionResult.Nothing;
        }

        private static ExecutionResult ExecuteAny(VMThreadManager manager, Thread thread, int input)
        {
            thread.InstructionPointer += 1;
            return ExecutionResult.Nothing;
        }

        private static ExecutionResult ExecuteBlank(VMThreadManager manager, Thread thread, int input)
        {
            int code = manager.ReadInt32(thread.InstructionPointer + 1);

            if (input == -1 || !CharBelongsToCode(Convert.ToChar(input), code))
                return ExecutionResult.Kill;
            thread.InstructionPointer += 5;
            return ExecutionResult.Nothing;
        }

        private static ExecutionResult ExecuteEos(VMThreadManager manager, Thread thread, int input)
        {
            if (input != -1)
                return ExecutionResult.Kill;
            thread.InstructionPointer += 1;
            return ExecutionResult.Nothing;
        }
    }
}
