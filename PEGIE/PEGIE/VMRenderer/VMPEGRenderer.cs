﻿/*
Copyright Julien Edmond (05/06/2014) 

edmondju@gmail.com

This library is a .NET library whose purpose is to help developpers in
diffent ways.

This software is governed by the CeCILL-C  license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PEGIE.VMRenderer
{
    /// <summary>
    /// Renderer that will render using a PEGIE binary language.
    /// </summary>
    public sealed class VMPEGRenderer : IPEGRenderer
    {
        #region Renderers

        private class RenderingContext
        {
            public bool IgnoreBlanks;
            public bool IgnoreCase;
            public EndOfLineStyle.EolStyle EolStyle;
            public UnicodeCategory[] BlanksDefinition;

            public RenderingContext()
            {
                IgnoreBlanks = true;
                IgnoreCase = false;
                EolStyle = (Environment.OSVersion.Platform == PlatformID.MacOSX || Environment.OSVersion.Platform == PlatformID.Unix
                ? EndOfLineStyle.EolStyle.Unix
                : EndOfLineStyle.EolStyle.Windows);
                BlanksDefinition = new UnicodeCategory[] { UnicodeCategory.SpaceSeparator };
            }

            public int BlanksCode
            {
                get
                {
                    int sent = 0;

                    foreach (UnicodeCategory category in this.BlanksDefinition)
                    {
                        sent |= (1 << (int)category);
                    }
                    return sent;
                }
            }
        }

        private delegate void NodeRenderer(INode node, VMBinaryStream stream, RenderingContext context);

        private static Dictionary<Type, NodeRenderer> _renderers;

        static VMPEGRenderer()
        {
            _renderers = new Dictionary<Type, NodeRenderer>()
            {
                {typeof(And), _RenderAnd},
                {typeof(Any), _RenderAny},
                {typeof(BlanksDefinition), _RenderBlanksDefinition},
                {typeof(Capture), _RenderCapture},
                {typeof(EndOfLine), _RenderEol},
                {typeof(EndOfLineStyle), _RenderEolStyle},
                {typeof(EndOfStream), _RenderEos},
                {typeof(Hook), _RenderHook},
                {typeof(IgnoreBlanks), _RenderIgnoreBlanks},
                {typeof(IgnoreCase), _RenderIgnoreCase},
                {typeof(NoneOrMany), _RenderNoneOrMany},
                {typeof(OneOrMany), _RenderOneOrMany},
                {typeof(OneOrNone), _RenderOneOrNone},
                {typeof(Or), _RenderOr},
                {typeof(RuleCall), _RenderRuleCall},
                {typeof(Sequence), _RenderSequence},
                {typeof(Except), _RenderExcept},
                {typeof(Range), _RenderRange}
            };
        }

        private static void _IgnoreBlanks(VMBinaryStream stream, RenderingContext context)
        {
            if (context.IgnoreBlanks)
            {
                /*
                 * start:   jmpnb code :end
                 *          blanks code
                 *          jmp :start
                 * end:
                 */

                VMBinaryLabel start = stream.DefineLabel();
                VMBinaryLabel end = stream.DefineLabel();

                stream.MarkLabel(start);
                stream.WriteByte(Opcode.Jmpnb);
                stream.WriteInt32(context.BlanksCode);
                stream.WriteLabel(end);

                stream.WriteByte(Opcode.Blank);
                stream.WriteInt32(context.BlanksCode);

                stream.WriteByte(Opcode.Jmp);
                stream.WriteLabel(start);

                stream.MarkLabel(end);


                /*
                 * start:   split :blank :end
                 * blank:   blank blank_code
                 *          jmp :start
                 * end:
                 * ...
                 *

                VMBinaryLabel start = stream.DefineLabel();
                VMBinaryLabel blank = stream.DefineLabel();
                VMBinaryLabel end = stream.DefineLabel();

                stream.MarkLabel(start);
                stream.WriteByte(Opcode.Split);
                stream.WriteLabel(blank);
                stream.WriteLabel(end);

                stream.MarkLabel(blank);
                stream.WriteByte(Opcode.Blank);
                stream.WriteInt32(context.BlanksCode);

                stream.WriteByte(Opcode.Jmp);
                stream.WriteLabel(start);

                stream.MarkLabel(end);
                 */
            }
        }

        private static void _RenderNode(INode node, VMBinaryStream stream, RenderingContext context)
        {
            _renderers[node.GetType()](node, stream, context);
        }

        private static void _RenderAnd(INode node, VMBinaryStream stream, RenderingContext context)
        {
            And and = (And)node;

            _RenderNode(and.Left, stream, context);
            _RenderNode(and.Right, stream, context);
        }

        private static void _RenderAny(INode node, VMBinaryStream stream, RenderingContext context)
        {
            _IgnoreBlanks(stream, context);
            stream.WriteByte(Opcode.Any);
        }

        private static void _RenderBlanksDefinition(INode node, VMBinaryStream stream, RenderingContext context)
        {
            BlanksDefinition bd = (BlanksDefinition)node;

            context.BlanksDefinition = bd.Categories;
        }

        private static void _RenderCapture(INode node, VMBinaryStream stream, RenderingContext context)
        {
            Capture c = (Capture)node;

            stream.WriteByte(Opcode.CaptureStart);
            stream.WriteString(c.Name);
            _RenderNode(c.Node, stream, context);
            stream.WriteByte(Opcode.CaptureEnd);
            stream.WriteString(c.Name);
        }

        private static void _RenderEol(INode node, VMBinaryStream stream, RenderingContext context)
        {
            if (context.EolStyle == EndOfLineStyle.EolStyle.Unix)
                _RenderSequence(new Sequence("\n"), stream, context);
            else
                _RenderSequence(new Sequence("\r\n"), stream, context);
        }

        private static void _RenderEolStyle(INode node, VMBinaryStream stream, RenderingContext context)
        {
            EndOfLineStyle eols = (EndOfLineStyle)node;

            context.EolStyle = eols.Value;
        }

        private static void _RenderEos(INode node, VMBinaryStream stream, RenderingContext context)
        {
            _IgnoreBlanks(stream, context);
            stream.WriteByte(Opcode.Eos);
//            stream.WriteByte(Opcode.Char);
//            stream.WriteInt16(-1);
        }

        private static void _RenderHook(INode node, VMBinaryStream stream, RenderingContext context)
        {
            Hook h = (Hook)node;

            stream.WriteByte(Opcode.Hook);
            stream.WriteString(h.Name);
            stream.WriteInt32(h.Args.Length);
            foreach (string arg in h.Args)
            {
                stream.WriteString(arg);
            }
        }

        private static void _RenderIgnoreBlanks(INode node, VMBinaryStream stream, RenderingContext context)
        {
            IgnoreBlanks ib = (IgnoreBlanks)node;

            context.IgnoreBlanks = ib.Value;
        }

        private static void _RenderIgnoreCase(INode node, VMBinaryStream stream, RenderingContext context)
        {
            IgnoreCase ic = (IgnoreCase)node;

            context.IgnoreCase = ic.Value;
        }

        private static void _RenderNoneOrMany(INode node, VMBinaryStream stream, RenderingContext context)
        {
            /*
             * split :start :end
             * start:
             * ...
             * split :start :end
             * end:
             */

            NoneOrMany nom = (NoneOrMany)node;
            VMBinaryLabel start = stream.DefineLabel();
            VMBinaryLabel end = stream.DefineLabel();

            stream.WriteByte(Opcode.Split);
            stream.WriteLabel(start);
            stream.WriteLabel(end);

            stream.MarkLabel(start);
            _RenderNode(nom.Node, stream, context);

            stream.WriteByte(Opcode.Split);
            stream.WriteLabel(start);
            stream.WriteLabel(end);

            stream.MarkLabel(end);
        }

        private static void _RenderOneOrMany(INode node, VMBinaryStream stream, RenderingContext context)
        { 
            /*
             * start:
             * ...
             * split :start :end
             * end:
             */

            OneOrMany oom = (OneOrMany)node;
            VMBinaryLabel start = stream.DefineLabel();
            VMBinaryLabel end = stream.DefineLabel();

            stream.MarkLabel(start);
            _RenderNode(oom.Node, stream, context);

            stream.WriteByte(Opcode.Split);
            stream.WriteLabel(start);
            stream.WriteLabel(end);

            stream.MarkLabel(end);
        }

        private static void _RenderOneOrNone(INode node, VMBinaryStream stream, RenderingContext context)
        {
            /*
             * split :start :end
             * start:
             * ...
             * end:
             */

            OneOrNone oon = (OneOrNone)node;
            VMBinaryLabel start = stream.DefineLabel();
            VMBinaryLabel end = stream.DefineLabel();

            stream.WriteByte(Opcode.Split);
            stream.WriteLabel(start);
            stream.WriteLabel(end);

            stream.MarkLabel(start);
            _RenderNode(oon.Node, stream, context);

            stream.MarkLabel(end);
        }

        private static void _RenderOr(INode node, VMBinaryStream stream, RenderingContext context)
        {
            /*
             * split :left :right
             * left:
             * ...
             * jmp :end
             * right:
             * ...
             * end:
             */

            Or or = (Or)node;
            VMBinaryLabel left = stream.DefineLabel();
            VMBinaryLabel right = stream.DefineLabel();
            VMBinaryLabel end = stream.DefineLabel();

            stream.WriteByte(Opcode.Split);
            stream.WriteLabel(left);
            stream.WriteLabel(right);

            stream.MarkLabel(left);
            _RenderNode(or.Left, stream, context);

            stream.WriteByte(Opcode.Jmp);
            stream.WriteLabel(end);

            stream.MarkLabel(right);
            _RenderNode(or.Right, stream, context);

            stream.MarkLabel(end);
        }

        private static void _RenderRuleCall(INode node, VMBinaryStream stream, RenderingContext context)
        {
            _IgnoreBlanks(stream, context);
            stream.WriteByte(Opcode.Call);
            stream.WriteLabel(((RuleCall)node).Rule);
        }

        private static void _RenderSequence(INode node, VMBinaryStream stream, RenderingContext context)
        {
            Sequence sq = (Sequence)node;

            _IgnoreBlanks(stream, context);
            foreach (char c in sq.Value)
            {
                if (context.IgnoreCase && Char.IsLetter(c))
                {
                    /*
                     * split :left :right
                     * left: char lower_c
                     * jmp :end
                     * right: char upper_c
                     * end:
                     * ...
                     */
                    VMBinaryLabel left = stream.DefineLabel();
                    VMBinaryLabel right = stream.DefineLabel();
                    VMBinaryLabel end = stream.DefineLabel();

                    stream.WriteByte(Opcode.Split);
                    stream.WriteLabel(left);
                    stream.WriteLabel(right);

                    stream.MarkLabel(left);
                    stream.WriteByte(Opcode.Char);
                    stream.WriteChar(Char.ToLower(c));

                    stream.WriteByte(Opcode.Jmp);
                    stream.WriteLabel(end);

                    stream.MarkLabel(right);
                    stream.WriteByte(Opcode.Char);
                    stream.WriteChar(Char.ToUpper(c));

                    stream.MarkLabel(end);
                }
                else
                {
                    stream.WriteByte(Opcode.Char);
                    stream.WriteChar(c);
                }
            }
        }

        private static void _RenderExcept(INode node, VMBinaryStream stream, RenderingContext context)
        {
            /*
             * except :left :right
             * left:
             * ...
             * jmp :end
             * right:
             * ...
             * end: anti_match
             */

            Except e = (Except)node;
            VMBinaryLabel left = stream.DefineLabel();
            VMBinaryLabel right = stream.DefineLabel();
            VMBinaryLabel end = stream.DefineLabel();

            stream.WriteByte(Opcode.Except);
            stream.WriteLabel(left);
            stream.WriteLabel(right);

            stream.MarkLabel(left);
            _RenderNode(e.Left, stream, context);

            stream.WriteByte(Opcode.Jmp);
            stream.WriteLabel(end);

            stream.MarkLabel(right);
            _RenderNode(e.Right, stream, context);

            stream.MarkLabel(end);
            stream.WriteByte(Opcode.AntiMatch);
        }

        private static void _RenderRange(INode node, VMBinaryStream stream, RenderingContext context)
        {
            Range r = (Range)node;

            _IgnoreBlanks(stream, context);

            if (context.IgnoreCase && (Char.IsLetter(r.From) || Char.IsLetter(r.To)))
            {
                /*
                 * split :left :right
                 * left: range lower_from lower_to
                 * jmp :end
                 * right: range upper_from upper_to
                 * end:
                 * ...
                 */
                VMBinaryLabel left = stream.DefineLabel();
                VMBinaryLabel right = stream.DefineLabel();
                VMBinaryLabel end = stream.DefineLabel();

                stream.WriteByte(Opcode.Split);
                stream.WriteLabel(left);
                stream.WriteLabel(right);

                stream.MarkLabel(left);
                stream.WriteByte(Opcode.Range);
                stream.WriteChar(Char.ToLower(r.From));
                stream.WriteChar(Char.ToLower(r.To));

                stream.WriteByte(Opcode.Jmp);
                stream.WriteLabel(end);

                stream.MarkLabel(right);
                stream.WriteByte(Opcode.Range);
                stream.WriteChar(Char.ToUpper(r.From));
                stream.WriteChar(Char.ToUpper(r.To));

                stream.MarkLabel(end);
            }
            else
            {
                stream.WriteByte(Opcode.Range);
                stream.WriteChar(r.From);
                stream.WriteChar(r.To);
            }
        }

        #endregion

        private static byte[] Magic = new byte[4] { 0x00, 0x17, 0xE6, 0x1E };

        /// <summary>
        /// Construct the renderer.
        /// </summary>
        public VMPEGRenderer()
        {
        }

        /// <summary>
        /// Renders the specified builder.
        /// </summary>
        /// <param name="builder">The builder.</param>
        /// <returns>The rendered <see cref="IPEG"/></returns>
        public IPEG Render(PEGBuilder builder)
        {
            VMBinaryStream stream = new VMBinaryStream();
            VMBinaryLabel headerSize = stream.DefineLabel();
            VMBinaryLabel fileSize = stream.DefineLabel();

            foreach (string ruleName in builder.Rules.Keys)
                stream.DefineLabel(ruleName);
            stream.WriteBytes(Magic);
            stream.WriteLabel(headerSize);
            stream.WriteLabel(fileSize);
            stream.WriteInt32(builder.Rules.Count);
            stream.WriteLabel(builder.StartingRule.Name);
            foreach (string ruleName in builder.Rules.Keys)
            {
                stream.WriteString(ruleName);
                stream.WriteLabel(ruleName);
            }
            stream.MarkLabel(headerSize);

            foreach (RuleBuilder rule in builder.Rules.Values)
            {
                stream.MarkLabel(rule.Name);
                _RenderNode(rule.Tree, stream, new RenderingContext());
                stream.WriteByte(Opcode.Match);
            }

            stream.MarkLabel(fileSize);

            using (BinaryReader br = new BinaryReader(new MemoryStream(stream.RetrieveBytes()), Encoding.Unicode))
            using (FileStream fs = File.OpenWrite("tmp.vmpeg"))
            using (TextWriter tw = new StreamWriter(fs))
                BinaryToText(br, tw);
            return new VMInterpreter(stream.RetrieveBytes());
        }

        #region Helper

        /// <summary>
        /// Decompile a PEGIE binary code to a human readable language.
        /// </summary>
        /// <param name="input">The binary code input stream.</param>
        /// <param name="output">The text code output stream.</param>
        public void BinaryToText(BinaryReader input, TextWriter output)
        {
 //           TextWriter realOutput = output;
 //           output = new StringWriter();

            int position = 0;

            byte[] magic = input.ReadBytes(4);
            position += 4;
            output.WriteLine("Magic: [{0:x}, {1:x}, {2:x}, {3:x}]", magic[0], magic[1], magic[2], magic[3]);

            int headerSize = input.ReadInt32();
            position += 4;
            output.WriteLine("Header size: {0}", headerSize);

            int fileSize = input.ReadInt32();
            position += 4;
            output.WriteLine("File size: {0}", fileSize);

            int entriesCount = input.ReadInt32();
            position += 4;
            output.WriteLine("Entries count: {0}", entriesCount);

            int startOffset = input.ReadInt32();
            position += 4;
            output.WriteLine("Starting offset: {0:x}", startOffset);

            Dictionary<int, string> rules = new Dictionary<int, string>();

            output.WriteLine("Rule {");
            for (int i = 0; i < entriesCount; i++)
            {
                int size = input.ReadInt32();
                position += 4;
                byte[] name = input.ReadBytes((int)size);
                position += size;
                int offset = input.ReadInt32();
                position += 4;

                rules.Add(offset, new string(Encoding.Unicode.GetChars(name)));
                output.WriteLine("\t{0} starts at {1:x}", new string(Encoding.Unicode.GetChars(name)), offset);
            }
            output.WriteLine("}");

            output.WriteLine("Real header size: {0}", position);
            
            while (position < fileSize)
            {
                if (rules.Keys.Contains(position))
                {
                    output.WriteLine();
                    output.WriteLine("{0}:", rules[position]);
                }
                byte opcode = input.ReadByte();

                output.Write("{0:x}:\t", position);
                if (opcode == Opcode.Char)
                {
                    char value = input.ReadChar();

                    if (!Char.IsControl(value))
                        output.WriteLine("char '{0}'", (char)value);
                    else
                        output.WriteLine("char {0:x}", value);
                    position += 3;
                }
                else if (opcode == Opcode.Range)
                {
                    char l = input.ReadChar();
                    char r = input.ReadChar();

                    output.WriteLine("range " + (!Char.IsControl(l) ? "'{0}' " : "{0:x} ") + (!Char.IsControl(r) ? "'{1}'" : "{1:x}"), (char)l, (char)r);
                    position += 5;
                }
                else if (opcode == Opcode.Match)
                {
                    output.WriteLine("match");
                    position += 1;
                }
                else if (opcode == Opcode.Jmp)
                {
                    output.WriteLine("jmp {0:x}", input.ReadInt32());
                    position += 5;
                }
                else if (opcode == Opcode.Split)
                {
                    output.WriteLine("split {0:x} {1:x}", input.ReadInt32(), input.ReadInt32());
                    position += 9;
                }
                else if (opcode == Opcode.Call)
                {
                    output.WriteLine("call {0:x}", input.ReadInt32());
                    position += 5;
                }
                else if (opcode == Opcode.Except)
                {
                    output.WriteLine("except {0:x} {1:x}", input.ReadInt32(), input.ReadInt32());
                    position += 9;
                }
                else if (opcode == Opcode.Any)
                {
                    output.WriteLine("any");
                    position += 1;
                }
                else if (opcode == Opcode.CaptureStart)
                {
                    int size = input.ReadInt32();
                    string str = new string(Encoding.Unicode.GetChars(input.ReadBytes((int)size)));

                    output.WriteLine("capture_start {0}", str);
                    position += 5 + size;
                }
                else if (opcode == Opcode.CaptureEnd)
                {
                    int size = input.ReadInt32();
                    string str = new string(Encoding.Unicode.GetChars(input.ReadBytes((int)size)));

                    output.WriteLine("capture_end {0}", str);
                    position += 5 + size;
                }
                else if (opcode == Opcode.Hook)
                {
                    string name = new string(Encoding.Unicode.GetChars(input.ReadBytes(input.ReadInt32())));
                    int size = input.ReadInt32();
                    position += 9 + (int)Encoding.Unicode.GetByteCount(name);

                    output.Write("hook {0}", name);
                    for (int i = 0; i < size; i++)
                    {
                        string param = new string(Encoding.Unicode.GetChars(input.ReadBytes(input.ReadInt32())));
                        output.Write(" {0}", param);
                        position += 4 + (int)Encoding.Unicode.GetByteCount(param);
                    }
                    output.WriteLine();
                }
                else if (opcode == Opcode.Blank)
                {
                    int categories = input.ReadInt32();
                    output.WriteLine("blank {0}", categories);
                    position += 5;
                }
                else if (opcode == Opcode.Eos)
                {
                    output.WriteLine("eos");
                    position += 1;
                }
                else if (opcode == Opcode.Jmpnb)
                {
                    int categories = input.ReadInt32();
                    int offset = input.ReadInt32();

                    output.WriteLine("jmpnb {0} {1:x}", categories, offset);
                    position += 9;
                }
                else if (opcode == Opcode.AntiMatch)
                {
                    output.WriteLine("anti_match");
                    position += 1;
                }
                else
                {
                    output.WriteLine("???");
                    position += 1;
                }
            }

            output.WriteLine("Real size: {0}", position);
            output.Flush();
//            realOutput.Write(output.ToString());
        }

        #endregion
    }
}
