﻿/*
Copyright Julien Edmond (05/06/2014) 

edmondju@gmail.com

This library is a .NET library whose purpose is to help developpers in
diffent ways.

This software is governed by the CeCILL-C  license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using My;
using System.Globalization;
using My.IO;

namespace PEGIE.VMRenderer
{
    internal partial class VMThreadManager : IPEGRun
    {
        private class CaptureContext
        {
            public Dictionary<string, string> CompletedCaptures;
            public Dictionary<string, StringBuilder> Captures;

            public CaptureContext()
            {
                CompletedCaptures = new Dictionary<string, string>();
                Captures = new Dictionary<string, StringBuilder>();
            }

            public CaptureContext(CaptureContext other)
            {
                this.CompletedCaptures = new Dictionary<string,string>(other.CompletedCaptures);
                this.Captures = new Dictionary<string, StringBuilder>();
                foreach (KeyValuePair<string, StringBuilder> entry in other.Captures)
	            {
                    this.Captures[entry.Key] = new StringBuilder(entry.Value.ToString());
                    //this.Captures[entry.Key] = other.OnGoingCaptures.Contains(entry.Key) ? new StringBuilder(entry.Value.ToString()) : entry.Value;
	            }
            }
        }

        private IReader<int> _reader;
        private bool _greedy;
        private byte[] _binary;
        private LinkedList<Thread> _threads;
        private LinkedList<Thread> _antiThreads;
        private LinkedList<Thread> _matchedAntithreads;
        private Thread _lastSuccess;
        private int _nextExceptId;
        private Dictionary<Thread, Dictionary<int, CaptureContext>> _captures;

        public VMThreadManager(byte[] binary, IReader<int> reader, bool greedy)
        {
            this._binary = binary;
            this._reader = reader;
            this._greedy = greedy;
            this._threads = new LinkedList<Thread>();
            this._antiThreads = new LinkedList<Thread>();
            this._matchedAntithreads = new LinkedList<Thread>();
            this._lastSuccess = null;
            this._nextExceptId = 0;
            this._captures = new Dictionary<Thread, Dictionary<int, CaptureContext>>();
        }

        private ExecutionResult ExecuteNoInput(Thread thread)
        {
            ExecutionResult sent = ExecutionResult.Nothing;
            Opcode op = Opcode.Decode(this._binary[thread.InstructionPointer]);

            while (!op.RequireInput && sent == ExecutionResult.Nothing)
            {
                sent = Executors[op](this, thread);
            }
            return sent;
        }

        private void ExecuteNoInput()
        {
            // TODO Infinite loop detection

            LinkedList<Thread> newThreads = new LinkedList<Thread>();
            LinkedList<Thread> newAntiThread = new LinkedList<Thread>();

            while (this._antiThreads.Count > 0 || this._threads.Count > 0)
            {
                ICollection<Thread> pool = (this._antiThreads.Count > 0 ? this._antiThreads : this._threads);
                bool keepOn = true;
                Thread current = pool.First();

                Opcode op = Opcode.Decode(this._binary[current.InstructionPointer]);

                while (keepOn)
                {
                    if (op.RequireInput)
                        keepOn = false;
                    else
                    {
                        switch (Executors[op](this, current))
                        {
                            case ExecutionResult.Nothing:
                                keepOn = true;
                                op = Opcode.Decode(this._binary[current.InstructionPointer]);
                                break;
                            case ExecutionResult.Match:
                                if (current.AntiThread)
                                    this._matchedAntithreads.AddLast(current);
                                else
                                    this._lastSuccess = current;
                                keepOn = false;
                                break;
                            case ExecutionResult.Kill:
                                keepOn = false;
                                break;
                            default:
                                break;
                        }
                    }
                }
                pool.Remove(current);
                if (op.RequireInput)
                    (current.AntiThread ? newAntiThread : newThreads).AddLast(current);
                else
                    this._captures.Remove(current);

            }
            this._threads = newThreads;
            this._antiThreads = newAntiThread;
            this._matchedAntithreads.Clear();
        }

        private void ExecuteInput(int input)
        {
            this.FeedCaptures(input);

            LinkedList<Thread> newAntiThread = new LinkedList<Thread>();
            foreach (Thread th in this._antiThreads)
            {
                if (InputExecutors[Opcode.Decode(this._binary[th.InstructionPointer])](this, th, input) != ExecutionResult.Kill)
                    newAntiThread.AddLast(th);
            }
            this._antiThreads = newAntiThread;

            LinkedList<Thread> newThread = new LinkedList<Thread>();
            foreach (Thread th in this._threads)
            {
                if (InputExecutors[Opcode.Decode(this._binary[th.InstructionPointer])](this, th, input) != ExecutionResult.Kill)
                    newThread.AddLast(th);
            }
            this._threads = newThread;
        }

        private void FeedCaptures(int input)
        {
            if (input != -1)
                foreach (Thread thread in this._threads)
                {
                    if (this._captures.ContainsKey(thread) && Opcode.Decode(this._binary[thread.InstructionPointer]) != Opcode.Blank)
                        foreach (CaptureContext context in this._captures[thread].Values)
                        {
                            foreach (StringBuilder capture in context.Captures.Values)
                            {
                                capture.Append(Convert.ToChar(input));
                            }
                            //foreach (string capture in context.OnGoingCaptures)
                            //{
                            //    context.Captures[capture].Append(Convert.ToChar(input));
                            //}
                        }
                }
        }

        public bool Execute()
        {
            Thread starting = new Thread(this.StartOffset, false);
            int value;

            this._threads.AddLast(starting);

            this.ExecuteNoInput();
            do
            {
                value = this._reader.Read();

                this.ExecuteInput(value);
                this.ExecuteNoInput();
            }
            while (((!this._greedy && this._lastSuccess == null) || this._threads.Count > 0) && value != -1);
            if (this._lastSuccess != null)
                foreach (HookEventArg hook in this._lastSuccess.Hooks)
                {
                    this.Hook(this, hook);
                }
            return this._lastSuccess != null;
        }

        public event EventHandler<HookEventArg> Hook;

        public int StartOffset
        {
            get
            {
                return this.ReadInt32(16);
            }
        }

        private bool AntithreadMatched(Thread th)
        {
            bool result = false;

            foreach (Thread at in this._matchedAntithreads)
            {
                if (at.ExceptId.Peek() == th.ExceptId.Peek())
                {
                    result = true;
                    at.ExceptId.Pop();
                    break;
                }
            }
            th.ExceptId.Pop();
            return result;
        }

        private void CloneThread(Thread thread, int offset)
        {
            Thread child = thread.Clone();

            child.InstructionPointer = offset;
            if (!thread.AntiThread)
            {
                if (this._captures.ContainsKey(thread))
                {
                    this._captures[child] = new Dictionary<int, CaptureContext>();
                    foreach (KeyValuePair<int, CaptureContext> entry in this._captures[thread])
                    {
                        this._captures[child][entry.Key] = new CaptureContext(entry.Value);
                    }
                }
                this._threads.AddLast(child);
            }
            else
                this._antiThreads.AddLast(child);
        }

        private int GetNextExceptId()
        {
            return this._nextExceptId++;
        }

        private void CreateAntiThread(Thread th, int offset)
        {
            Thread anti = new Thread(offset, true);
            int eid = this.GetNextExceptId();

            th.ExceptId.Push(eid);
            anti.ExceptId.Push(eid);
            this._antiThreads.AddLast(anti);
        }

        private string GetCaptureValue(Thread th, string value)
        {
            if (this._captures.ContainsKey(th) && this._captures[th].ContainsKey(th.Depth) && this._captures[th][th.Depth].CompletedCaptures.ContainsKey(value))
                value = this._captures[th][th.Depth].CompletedCaptures[value];
            //if (this._captures.ContainsKey(th) && this._captures[th].ContainsKey(th.Depth) && this._captures[th][th.Depth].Captures.ContainsKey(value))
            //    value = this._captures[th][th.Depth].Captures[value].ToString();
            return value;
        }

        private void StartCapture(Thread th, string capture)
        {
            if (!this._captures.ContainsKey(th))
                this._captures[th] = new Dictionary<int, CaptureContext>();
            if (!this._captures[th].ContainsKey(th.Depth))
                this._captures[th][th.Depth] = new CaptureContext();
            //this._captures[th][th.Depth].OnGoingCaptures.Add(capture);
            this._captures[th][th.Depth].Captures[capture] = new StringBuilder();
        }

        private void EndCapture(Thread th, string capture)
        {
            this._captures[th][th.Depth].CompletedCaptures[capture] = this._captures[th][th.Depth].Captures[capture].ToString();
            this._captures[th][th.Depth].Captures.Remove(capture);
            //this._captures[th][th.Depth].OnGoingCaptures.Remove(capture);
        }

        private bool ThreadDone(Thread thread)
        {
            return thread.Depth < 0;
        }

        private void PopContext(Thread thread)
        {
            if (this._captures.ContainsKey(thread))
                this._captures[thread].Remove(thread.Depth);
            --thread.Depth;
//            thread.Contexts.Pop();
        }

        private void PushContext(Thread thread, int offset)
        {
            ++thread.Depth;
            if (thread.Pointers.Count <= thread.Depth)
                thread.Pointers.Add(offset);
            else
                thread.InstructionPointer = offset;
//            thread.Contexts.Push(new Context(offset));
        }
    }
}
