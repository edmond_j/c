﻿/*
Copyright Julien Edmond (05/06/2014) 

edmondju@gmail.com

This library is a .NET library whose purpose is to help developpers in
diffent ways.

This software is governed by the CeCILL-C  license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using My;

namespace PEGIE.TreeRenderer.NodeProviders
{
    [Serializable]
    internal sealed class AndNodeProvider : ABinaryPEGNodeProvider
    {
        public AndNodeProvider(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public AndNodeProvider(And and)
            : base(TreeRenderer.Convert(and.Left), TreeRenderer.Convert(and.Right))
        {}

        public override IPEGNode Create(Context context)
        {
            return new Node(context, _left, _right);
        }

        #region Node
        
        private class Node : APEGNode
        {
            IPEGNodeProvider _rightProvider;

            IPEGNode _leftNode;
            private List<IPEGNode> _rightNodes;
            private List<IPEGNode> _rightNodesToAdd;

            public Node(Context context, IPEGNodeProvider left, IPEGNodeProvider right)
            {
                _rightProvider = right;
                _leftNode = left.Create(context);
                _rightNodes = new List<IPEGNode>();
                _rightNodesToAdd = new List<IPEGNode>();
            }

            public override bool Init()
            {
                _leftNode.Finished += leftNode_Finished;
                if (!_leftNode.Init())
                    _leftNode = null;
                _rightNodes.AddMany(_rightNodesToAdd);
                _rightNodesToAdd.Clear();
                return _leftNode != null || _rightNodes.Count > 0;
            }

            void leftNode_Finished(object sender, Context e)
            {
                IPEGNode rightNode = _rightProvider.Create(e.Clone());

                rightNode.Finished += rightNode_Finished;
                if (rightNode.Init())
                    _rightNodesToAdd.Add(rightNode);
            }

            void rightNode_Finished(object sender, Context e)
            {
                this.HasFinished(e);
            }

            public override bool Accept(int value)
            {
                if (_leftNode != null && !_leftNode.Accept(value))
                    _leftNode = null;

                _rightNodes.RemoveAll(node => !node.Accept(value));
                _rightNodes.AddMany(_rightNodesToAdd);
                _rightNodesToAdd.Clear();
                return _leftNode != null || _rightNodes.Count > 0;
            }
        }

        #endregion
    }
}
