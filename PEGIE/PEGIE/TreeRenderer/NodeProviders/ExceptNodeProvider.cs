﻿/*
Copyright Julien Edmond (05/06/2014) 

edmondju@gmail.com

This library is a .NET library whose purpose is to help developpers in
diffent ways.

This software is governed by the CeCILL-C  license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace PEGIE.TreeRenderer.NodeProviders
{
    [Serializable]
    internal sealed class ExceptNodeProvider : ABinaryPEGNodeProvider
    {
        public ExceptNodeProvider(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {}

        public ExceptNodeProvider(Except except)
            : base(TreeRenderer.Convert(except.Left), TreeRenderer.Convert(except.Right))
        {}

        public override IPEGNode Create(Context context)
        {
            return new Node(_left.Create(context.Clone()), _right.Create(context.Clone()));
        }

        #region Node

        private class Node : APEGNode
        {
            private IPEGNode _left;
            private IPEGNode _right;
            private bool _rightFinished;

            public Node(IPEGNode left, IPEGNode right)
            {
                _left = left;
                _right = right;

                _left.Finished += left_Finished;
                _right.Finished += right_Finished;
            }

            void right_Finished(object sender, Context e)
            {
                _rightFinished = true;
            }

            void left_Finished(object sender, Context e)
            {
                if (!_rightFinished)
                    this.HasFinished(e);
            }

            public override bool Init()
            {
                _rightFinished = false;
                _right.Init();
                return _left.Init();
            }

            public override bool Accept(int value)
            {
                _rightFinished = false;
                if (_right != null && !_right.Accept(value))
                    _right = null;
                return _left.Accept(value);
            }
        }

        #endregion

    }
}
