﻿/*
Copyright Julien Edmond (05/06/2014) 

edmondju@gmail.com

This library is a .NET library whose purpose is to help developpers in
diffent ways.

This software is governed by the CeCILL-C  license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using My.Runtime.Serialization;

namespace PEGIE.TreeRenderer.NodeProviders
{
    [Serializable]
    internal sealed class CaptureNodeProvider : AUnaryPEGNodeProvider
    {
        private string _name;

        public CaptureNodeProvider(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            _name = info.GetValue<string>("name");
        }

        public CaptureNodeProvider(Capture capture)
            : base(TreeRenderer.Convert(capture.Node))
        {
            _name = capture.Name;
        }

        public override IPEGNode Create(Context context)
        {
            return new Node(context, _node, _name);
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValue("name", _name);
        }

        #region Node

        private class Node : APEGNode
        {
            private IPEGNode _node;
            private string _name;
            private StringBuilder _result;
            private APEGLeafIgnoringBlanks _igNode;

            public Node(Context context, IPEGNodeProvider nodeProvider, string name)
            {
                _node = nodeProvider.Create(context);
                _igNode = _node as APEGLeafIgnoringBlanks;
                _name = name;
                _result = new StringBuilder();

                _node.Finished += node_Finished;
            }

            void node_Finished(object sender, Context e)
            {
                e.Captures[_name] = _result.ToString();
                this.HasFinished(e);
            }

            public override bool Init()
            {
                return _node.Init();
            }

            public override bool Accept(int value)
            {
                if (_igNode == null || _igNode.Context.IgnoreBlanks == false || _igNode.BlanksIgnored || !_igNode.IsBlanks(value))
                    _result.Append((char)value);
                return _node.Accept(value);
            }
        }

        #endregion
    }
}
