﻿/*
Copyright Julien Edmond (05/06/2014) 

edmondju@gmail.com

This library is a .NET library whose purpose is to help developpers in
diffent ways.

This software is governed by the CeCILL-C  license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PEGIE.TreeRenderer.NodeProviders
{
    [Serializable]
    internal sealed class OneOrManyNodeProvider : AUnaryPEGNodeProvider
    {
        public OneOrManyNodeProvider(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {}

        public OneOrManyNodeProvider(OneOrMany nom)
            : base(TreeRenderer.Convert(nom.Node))
        {}

        public override IPEGNode Create(Context context)
        {
            return new Node(context, _node);
        }

        #region Node

        private class Node : APEGLeaf
        {
            private IPEGNodeProvider _nodeProvider;
            private List<IPEGNode> _nodes;
            private List<IPEGNode> _nodesToAdd;

            public Node(Context context, IPEGNodeProvider nodeProvider)
                : base(context)
            {
                _nodeProvider = nodeProvider;
                _nodes = new List<IPEGNode>();
                _nodesToAdd = new List<IPEGNode>();
            }

            void node_Finished(object sender, Context e)
            {
                this.HasFinished(e);
                if (_nodes.Contains((IPEGNode)sender))
                    this._PushNode(e);
            }

            public override bool Init()
            {
                this._PushNode(this.Context);
                _nodes.AddRange(_nodesToAdd);
                _nodesToAdd.Clear();
                return _nodes.Count > 0;
            }

            private void _PushNode(Context context)
            {
                IPEGNode node;

                node = _nodeProvider.Create(context.Clone());
                node.Finished += node_Finished;
                if (node.Init())
                    _nodesToAdd.Add(node);
            }

            public override bool Accept(int value)
            {
                _nodes.RemoveAll(node => !node.Accept(value));
                _nodes.AddRange(_nodesToAdd);
                _nodesToAdd.Clear();
                return _nodes.Count > 0;
            }
        }

        #endregion
    }
}
