﻿/*
Copyright Julien Edmond (05/06/2014) 

edmondju@gmail.com

This library is a .NET library whose purpose is to help developpers in
diffent ways.

This software is governed by the CeCILL-C  license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using My;
using My.IO;
using PEGIE.TreeRenderer.NodeProviders;
using System.Runtime.Serialization;
using My.Runtime.Serialization;

namespace PEGIE.TreeRenderer
{
    /// <summary>
    /// Renderer that will generate a parsing tree.
    /// </summary>
    public sealed class TreeRenderer : IPEGRenderer
    {
        private static Dictionary<Type, Type> _converter = new Dictionary<Type, Type>()
        {
            {typeof(And), typeof(AndNodeProvider)},
            {typeof(Any), typeof(AnyNodeProvider)},
            {typeof(BlanksDefinition), typeof(BlanksDefinitionNodeProvider)},
            {typeof(Capture), typeof(CaptureNodeProvider)},
            {typeof(EndOfLine), typeof(EndOfLineNodeProvider)},
            {typeof(EndOfLineStyle), typeof(EndOfLineStyleNodeProvider)},
            {typeof(EndOfStream), typeof(EndOfStreamNodeProvider)},
            {typeof(Except), typeof(ExceptNodeProvider)},
            {typeof(Hook), typeof(HookNodeProvider)},
            {typeof(IgnoreBlanks), typeof(IgnoreBlanksNodeProvider)},
            {typeof(IgnoreCase), typeof(IgnoreCaseNodeProvider)},
            {typeof(NoneOrMany), typeof(NoneOrManyNodeProvider)},
            {typeof(OneOrMany), typeof(OneOrManyNodeProvider)},
            {typeof(OneOrNone), typeof(OneOrNoneNodeProvider)},
            {typeof(Or), typeof(OrNodeProvider)},
            {typeof(Range), typeof(RangeNodeProvider)},
            {typeof(RuleCall), typeof(RuleCallNodeProvider)},
            {typeof(Sequence), typeof(SequenceNodeProvider)}
        };

        internal static IPEGNodeProvider Convert(INode node)
        {
            return (IPEGNodeProvider)Activator.CreateInstance(_converter[node.GetType()], node);
        }

        /// <summary>
        /// Renders the specified builder.
        /// </summary>
        /// <param name="builder">The builder.</param>
        /// <returns>The rendered <see cref="IPEG"/></returns>
        public IPEG Render(PEGBuilder builder)
        {
            PEG result = new PEG(builder);

            return result;
        }

        [Serializable]
        private sealed class PEG : IPEG
        {
            private string _start;
            private Dictionary<string, IPEGNodeProvider> _rules;

            public PEG(SerializationInfo info, StreamingContext context)
            {
                _start = info.GetValue<string>("start");
                _rules = info.GetValue<Dictionary<string, IPEGNodeProvider>>("rules");
            }

            public PEG(PEGBuilder builder)
            {
                _rules = new Dictionary<string, IPEGNodeProvider>();
                foreach (KeyValuePair<string, RuleBuilder> rule in builder.Rules)
                {
                    IPEGNodeProvider provider = Convert(rule.Value.Tree);

                    _rules.Add(rule.Key, provider);
                    if (rule.Value == builder.StartingRule)
                        _start = rule.Key;
                }
            }

            public IPEGRun NewRun(IReader<int> reader, bool greedy = true)
            {
                return new PEGRun(reader, greedy, _rules[_start], _rules);
            }

            public void GetObjectData(SerializationInfo info, StreamingContext context)
            {
                info.AddValue("start", _start);
                info.AddValue("rules", _rules);
            }

            #region PEGRun

            private class PEGRun : IPEGRun
            {
                private IReader<int> _reader;
                private bool _greedy;
                private IPEGNodeProvider _start;
                private Dictionary<string, IPEGNodeProvider> _rules;
                private Context _lastSuccess;

                public PEGRun(IReader<int> reader, bool greedy, IPEGNodeProvider start, Dictionary<string, IPEGNodeProvider> rules)
                {
                    _reader = reader;
                    _greedy = greedy;
                    _start = start;
                    _rules = rules;
                    _lastSuccess = null;
                }

                public bool Execute()
                {
                    IPEGNode node = _start.Create(new Context(_rules));
                    int value;

                    node.Finished += node_Finished;
                    node.Init();
                    do
                    {
                        value = _reader.Read();
                    } while ((_greedy || _lastSuccess == null) && node.Accept(value) && value != -1);
                    if (_lastSuccess != null)
                        foreach (HookEventArg hook in _lastSuccess.Hooks)
                            EventHelper.BroadcastEvent<HookEventArg>(this.Hook, this, hook);
                    return _lastSuccess != null;
                }

                void node_Finished(object sender, Context e)
                {
                    _lastSuccess = e.Clone();
                }

                public event EventHandler<HookEventArg> Hook;
            }

            #endregion
        }
    }
}
