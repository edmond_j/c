﻿/*
Copyright Julien Edmond (05/06/2014) 

edmondju@gmail.com

This library is a .NET library whose purpose is to help developpers in
diffent ways.

This software is governed by the CeCILL-C  license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PEGIE.DotRenderer
{
    /// <summary>
    /// A renderer that will output a dot graph inside a stream.
    /// </summary>
    public sealed class DotRenderer : IPEGRenderer
    {
        private delegate void NodeRenderer(INode node);

        private IPEGRenderer _realRenderer;
        private TextWriter _output;
        private Dictionary<Type, NodeRenderer> _renderers;

        /// <summary>
        /// Initializes a new instance of the <see cref="DotRenderer"/> class.
        /// </summary>
        /// <param name="output">The output stream.</param>
        /// <param name="realRenderer">The real renderer.</param>
        public DotRenderer(TextWriter output, IPEGRenderer realRenderer)
        {
            _output = output;
            _realRenderer = realRenderer;
            _renderers = new Dictionary<Type, NodeRenderer>()
            {
                {typeof(And), this._RendererAnd},
                {typeof(Any), this._RendererAny},
                {typeof(BlanksDefinition), this._RendererBlanksDefinition},
                {typeof(Capture), this._RendererCapture},
                {typeof(EndOfLine), this._RendererEol},
                {typeof(EndOfLineStyle), this._RendererEolStyle},
                {typeof(EndOfStream), this._RendererEos},
                {typeof(Hook), this._RendererHook},
                {typeof(IgnoreBlanks), this._RendererIgnoreBlanks},
                {typeof(IgnoreCase), this._RendererIgnoreCase},
                {typeof(NoneOrMany), this._RendererNoneOrMany},
                {typeof(OneOrMany), this._RendererOneOrMany},
                {typeof(OneOrNone), this._RendererOneOrNone},
                {typeof(Or), this._RendererOr},
                {typeof(RuleCall), this._RendererRuleCall},
                {typeof(Sequence), this._RendererSequence},
                {typeof(Except), this._RenderExcept},
                {typeof(Range), this._RenderRange}
            };
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DotRenderer"/> class using a default renderer.
        /// </summary>
        /// <param name="output">The output stream.</param>
        public DotRenderer(TextWriter output)
            : this(output, PEGBuilder.GetDefaultRenderer())
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="DotRenderer"/> class.
        /// </summary>
        /// <param name="filename">The filename where to save the output.</param>
        /// <param name="realRenderer">The real renderer.</param>
        public DotRenderer(string filename, IPEGRenderer realRenderer)
            : this(new StreamWriter(File.Exists(filename) ? File.Open(filename, FileMode.Truncate) : File.Create(filename)), realRenderer)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="DotRenderer"/> class using a default renderer.
        /// </summary>
        /// <param name="filename">The filename where to save the output.</param>
        public DotRenderer(string filename)
            : this(filename, PEGBuilder.GetDefaultRenderer())
        { }

        /// <summary>
        /// Renders the specified builder.
        /// </summary>
        /// <param name="builder">The builder.</param>
        /// <returns>
        /// The rendered <see cref="IPEG" />
        /// </returns>
        public IPEG Render(PEGBuilder builder)
        {
            int ruleNum = 0;

            _output.WriteLine("digraph {{");
            foreach (KeyValuePair<string, RuleBuilder> rule in builder.Rules)
            {
                _output.WriteLine("subgraph cluster_{0} {{", ruleNum);
                _output.WriteLine("label=\"{0}\";", rule.Key);
                if (rule.Value == builder.StartingRule)
                    _output.WriteLine("style=filled; color=lightgrey;");
                _RenderNode(rule.Value.Tree);
                _output.WriteLine("}");
                ++ruleNum;
            }
            _output.WriteLine("}}");
            _output.Close();

            return _realRenderer.Render(builder);
        }

        private void _RenderNode(INode node)
        {
            this._renderers[node.GetType()](node);
        }

        private void _WriteArrow(INode node1, INode node2)
        {
            _output.WriteLine("n{0} -> n{1};", node1.GetHashCode(), node2.GetHashCode());
        }

        private void _WriteNode(string p, INode node)
        {
            _output.WriteLine("n{0} [label=\"{1}\"];", node.GetHashCode(), p.Replace("\\", "\\\\").Replace("\"", "\\\""));
        }

        private void _RendererAnd(INode node)
        {
            _WriteNode("And", node);
            _WriteArrow(node, ((And)node).Left);
            _WriteArrow(node, ((And)node).Right);

            _RenderNode(((And)node).Left);
            _RenderNode(((And)node).Right);
        }

        private void _RendererAny(INode node)
        {
            _WriteNode("Any", node);
        }

        private void _RendererBlanksDefinition(INode node)
        {
            BlanksDefinition bd = (BlanksDefinition)node;
            StringBuilder sb = new StringBuilder("@blanks_definition(");

            foreach (System.Globalization.UnicodeCategory category in bd.Categories)
            {
                sb.Append(category.ToString());
            }
            _WriteNode(sb.Append(')').ToString(), node);
        }

        private void _RendererCapture(INode node)
        {
            _WriteNode("Capture: " + ((Capture)node).Name, node);
            _WriteArrow(node, ((Capture)node).Node);

            _RenderNode(((Capture)node).Node);
        }

        private void _RendererEol(INode node)
        {
            _WriteNode("<eol>", node);
        }

        private void _RendererEos(INode node)
        {
            _WriteNode("<eos>", node);
        }

        private void _RendererEolStyle(INode node)
        {
            _WriteNode("@eol_style(" + (((((EndOfLineStyle)node).Value) == EndOfLineStyle.EolStyle.Unix ? "Unix" : "Windows") + ")"), node);
        }

        private void _RenderExcept(INode node)
        {
            Except except = (Except)node;

            _WriteNode("Except", node);
            _WriteArrow(node, except.Left);
            _WriteArrow(node, except.Right);

            _RenderNode(except.Left);
            _RenderNode(except.Right);
        }

        private void _RendererHook(INode node)
        {
            Hook hook = (Hook)node;
            string result = "";

            foreach (string str in hook.Args)
	        {
                result += str;
	        }

            _WriteNode("#" + hook.Name + "(" + result + ")", node);
        }

        private void _RendererIgnoreBlanks(INode node)
        {
            _WriteNode("@ignore_blanks(" + ((IgnoreBlanks)node).Value + ')', node);
        }

        private void _RendererIgnoreCase(INode node)
        {
            _WriteNode("@ignore_case(" + ((IgnoreCase)node).Value + ')', node);
        }

        private void _RendererNoneOrMany(INode node)
        {
            NoneOrMany nom = (NoneOrMany)node;

            _WriteNode("*", nom);
            _WriteArrow(nom, nom.Node);

            _RenderNode(nom.Node);
        }

        private void _RendererOneOrMany(INode node)
        {
            OneOrMany oom = (OneOrMany)node;

            _WriteNode("+", oom);
            _WriteArrow(oom, oom.Node);

            _RenderNode(oom.Node);
        }

        private void _RendererOneOrNone(INode node)
        {
            OneOrNone oon = (OneOrNone)node;

            _WriteNode("?", oon);
            _WriteArrow(oon, oon.Node);

            _RenderNode(oon.Node);
        }

        private void _RendererOr(INode node)
        {
            Or or = (Or)node;

            _WriteNode("Or", or);
            _WriteArrow(or, or.Left);
            _WriteArrow(or, or.Right);

            _RenderNode(or.Left);
            _RenderNode(or.Right);
        }

        private void _RenderRange(INode node)
        {
            Range range = (Range)node;

            _WriteNode("[" + range.From + "-" + range.To + "]", range);
        }
        
        private void _RendererSequence(INode node)
        {
            Sequence sq = (Sequence)node;

            _WriteNode("Sequence: " + sq.Value, sq);
        }

        private void _RendererRuleCall(INode node)
        {
            RuleCall rc = (RuleCall)node;

            _WriteNode("<" + rc.Rule + ">", node);
        }
    }
}
