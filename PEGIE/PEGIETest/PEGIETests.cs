﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NUnit.Framework;
using TestClassAttribute = NUnit.Framework.TestFixtureAttribute;
using TestMethodAttribute = NUnit.Framework.TestCaseAttribute;
using PEGIE;
using My.IO;
using PEGIE.StringRenderer;

namespace PEGIETest
{
    [TestClass]
    class PEGIETests
    {
        public static string EvalExprPEG = 
@"     <<main>> := <expr> <eos>;

        <expr> := <add_expr>;

        <add_expr> := <mul_expr> (<add_op>:op <add_expr> #push_op(op))?;

        <add_op> := '+' | '-';

        <mul_expr> := <number> (<mul_op>:op <mul_expr> #push_op(op))?;

        <mul_op> := '*' | '/' | '%';

        <number> := <digit>+:number #push_number(number) | '(' <expr> ')';

        <digit> := [\o060-\x39]+;
";

        [TestMethod]
        public void Draft()
        {
            IPEG evalExpr = PEGIE.PEGIE.ParsePEG(EvalExprPEG);
            
            _TestRun(evalExpr, " 42 *  3   /   6 ", true);
            _TestRun(evalExpr, "1 + 2 * 3", true);
            _TestRun(evalExpr, "1 * 2 + 3", true);
            _TestRun(evalExpr, "1 * (2 + 3)", true);
            _TestRun(evalExpr, "0 * 9", true);
            
            PEGIETest.EvalExpr.Evaluator e = new PEGIETest.EvalExpr.Evaluator(evalExpr);

            Assert.AreEqual(2, e.Evaluate("1+1"));
            Assert.AreEqual(1196589, e.Evaluate("1337*895-(78/3)"));
            Assert.AreEqual(0, e.Evaluate("0*9"));
        }

        [TestMethod]
        public void TestExcept()
        {
            PEGBuilder builder = new PEGBuilder();

            builder.NewRule("main").Tree = new OneOrMany(new Capture(new Any() - new Sequence("a"), "c") & new Hook("newChar", "c"));
            builder.StartingRule = builder.Rules["main"];

            IPEG peg = builder.Finalize();

            _TestRun(peg, "aqwe", false);
            _TestRun(peg, "iuha", true);
        }

        private void _TestRun(IPEG peg, string feed, bool expected)
        {
            Console.WriteLine("Input: {0}", feed);
            IPEGRun run = peg.NewRun(ReaderHelper.NewStringReader(feed));

            run.Hook += run_Hook;
            Assert.AreEqual(expected, run.Execute());
            Console.WriteLine();
        }

        void run_Hook(object sender, HookEventArg e)
        {
            StringBuilder output = new StringBuilder();

            output.AppendFormat("Hook: {0}", e.HookName);
            if (e.HookArgs.Length > 0)
            {
                output.Append(" (");
                for (int i = 0; i < e.HookArgs.Length; ++i)
                {
                    if (i > 0)
                        output.Append(", ");
                    output.Append(e.HookArgs[i]);
                }
                output.Append(')');
            }
            Console.WriteLine(output);
        }
/*
        [TestMethod]
        public void Quantifiers()
        {
            StringBuilder tmp = new StringBuilder();
            IPEG peg = PEGIE.PEGIE.ParsePEG(
                @"
<<main>> := ('a'{1,3}:a 'b' #a(a))+;

<dummy> := 'a'{5} 'a'{13,};
", new StringRenderer(tmp)
                );

            Console.WriteLine("PEG: {0}", tmp);
            _TestRun(peg, "abaabaaabaaaabaaaaabaaaaaa", true);
        }
 */
    }
}
