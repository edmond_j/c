﻿using My.IO;
using NUnit.Framework;
using PEGIE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PEGIETest
{
    [TestFixture]
    class Chinese
    {
        [TestCase]
        public void Hello()
        {
            IPEG peg = PEGIE.PEGIE.ParsePEG(@"<<main>> := '我是' (<any>+):n #name(n);");

            _TestRun(peg, @"我是 Julien", true);
            _TestRun(peg, @"我是 Jing", true);
        }

        private void _TestRun(IPEG peg, string feed, bool expected)
        {
            Console.WriteLine("Input: {0}", feed);
            IPEGRun run = peg.NewRun(ReaderHelper.NewStringReader(feed));

            run.Hook += run_Hook;
            Assert.AreEqual(expected, run.Execute());
            Console.WriteLine();
        }

        private void run_Hook(object sender, HookEventArg e)
        {
            Console.WriteLine("你好 {0}", e.HookArgs[0]);
        }
    }
}
