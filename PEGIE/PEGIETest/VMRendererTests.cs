﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using TestClassAttribute = NUnit.Framework.TestFixtureAttribute;
using TestMethodAttribute = NUnit.Framework.TestCaseAttribute;
using PEGIE;
using PEGIE.VMRenderer;
using My.IO;

namespace PEGIETest
{
    [TestClass]
    public class VMRendererTests
    {
        public static string Test = PEGIETests.EvalExprPEG;

        [TestMethod]
        public void MyTestMethod()
        {
            IPEG peg = PEGIE.PEGIE.ParsePEG(Test, new VMPEGRenderer());
        }

        [TestMethod]
        public void IgnoreCaseRange()
        {
            IPEG peg = PEGIE.PEGIE.ParsePEG("<<main>> := @ignore_case(true) [a-z]+ <eos>;");

            Assert.AreEqual(true, peg.NewRun(ReaderHelper.NewStringReader("ab")).Execute());
            Assert.AreEqual(true, peg.NewRun(ReaderHelper.NewStringReader("AB")).Execute());
            Assert.AreEqual(true, peg.NewRun(ReaderHelper.NewStringReader("aBbA")).Execute());
        }
    }
}
