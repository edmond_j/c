﻿using System;
using PEGIE;
using PEGIE.DotRenderer;
using System.Text;
using PEGIE.StringRenderer;
using My.IO;
using System.Collections.Generic;

using NUnit.Framework;
using TestClassAttribute = NUnit.Framework.TestFixtureAttribute;
using TestMethodAttribute = NUnit.Framework.TestCaseAttribute;
using System.IO;

namespace PEGIETest
{
    [TestClass]
    public class EvalExpr
    {
        private static IPEG _BuildPEG()
        {
            return _BuildPEG(PEGBuilder.GetDefaultRenderer());
        }

        private static IPEG _BuildPEG(IPEGRenderer renderer)
        {
            PEGBuilder builder = new PEGBuilder(renderer);

            RuleBuilder main = builder.NewRule("main");
            main.Tree = new RuleCall("expr") & new EndOfStream();
            
            RuleBuilder expr = builder.NewRule("expr");
            expr.Tree = new RuleCall("add_expr");
            
            RuleBuilder add_expr = builder.NewRule("add_expr");
            add_expr.Tree = And.MakeAnd(new RuleCall("mul_expr"), new OneOrNone(And.MakeAnd(new Capture(new RuleCall("add_op"), "op"), new RuleCall("add_expr"), new Hook("push_op", "op"))));
            
            RuleBuilder add_op = builder.NewRule("add_op");
            add_op.Tree = new Or(new Sequence("+"), new Sequence("-"));
            
            RuleBuilder mul_expr = builder.NewRule("mul_expr");
            mul_expr.Tree = new And(new RuleCall("number"), new OneOrNone(And.MakeAnd(new Capture(new RuleCall("mul_op"), "op"), new RuleCall("mul_expr"), new Hook("push_op", "op"))));
            
            RuleBuilder mul_op = builder.NewRule("mul_op");
            mul_op.Tree = Or.MakeOr(new Sequence("*"), new Sequence("/"), new Sequence("%"));
            
            RuleBuilder number = builder.NewRule("number");
            number.Tree = new Or(And.MakeAnd(new Capture(new OneOrMany(new RuleCall("digit")), "number"), new Hook("push_number", "number")), And.MakeAnd(new Sequence("("), new RuleCall("expr"), new Sequence(")")));

            RuleBuilder digit = builder.NewRule("digit");
            digit.Tree = new Range('0', '9');

            builder.StartingRule = main;
            return builder.Finalize();
        }
        
        [TestMethod]
        public void DotRendering()
        {
            _BuildPEG(new DotRenderer("evalexpr.gv"));
        }

        [TestMethod]
        public void StringRendering()
        {
            StringBuilder tmp = new StringBuilder();
            _BuildPEG(new StringRenderer(tmp));
            Console.WriteLine(tmp);
        }

        [TestMethod]
        public void Serialization()
        {
            IPEG peg;

            using (Stream file = File.OpenWrite("EvalExpr.peg"))
                PEGIE.PEGIE.SavePEG(file, _BuildPEG());
            using (Stream file = File.OpenRead("EvalExpr.peg"))
                peg = PEGIE.PEGIE.LoadPEG(file);

            Evaluator e = new Evaluator(peg);

            Assert.AreEqual(2, e.Evaluate("1+1"));
            Assert.AreEqual(1196589, e.Evaluate("1337*895-(78/3)"));
        }

        #region Test

        [TestMethod]
        public void Test()
        {
            IPEG peg = _BuildPEG();

            _TestRun(peg, "13+26*35/64", true);
            _TestRun(peg, " 42 *  3   /   6 ", true);
            _TestRun(peg, "1 + 2 * 3", true);
            _TestRun(peg, "1 * 2 + 3", true);
            _TestRun(peg, "1 * (2 + 3)", true);
        }

        private void _TestRun(IPEG peg, string feed, bool expected)
        {
            Console.WriteLine("Input: {0}", feed);
            IPEGRun run = peg.NewRun(ReaderHelper.NewStringReader(feed));

            run.Hook += run_Hook;
            Assert.AreEqual(expected, run.Execute());
            Console.WriteLine();
        }

        void run_Hook(object sender, HookEventArg e)
        {
            StringBuilder output = new StringBuilder();

            output.AppendFormat("Hook: {0}", e.HookName);
            if (e.HookArgs.Length > 0)
            {
                output.Append(" (");
                for (int i = 0; i < e.HookArgs.Length; ++i)
                {
                    if (i > 0)
                        output.Append(", ");
                    output.Append(e.HookArgs[i]);
                }
                output.Append(')');
            }
            Console.WriteLine(output);
        }

        #endregion

        [TestMethod]
        public void Evaluation()
        {
            Evaluator e = new Evaluator(_BuildPEG());

            Assert.AreEqual(2, e.Evaluate("1+1"));
            Assert.AreEqual(1196589, e.Evaluate("1337*895-(78/3)"));
        }

        public class Evaluator
        {
            private IPEG _peg;

            public Evaluator(IPEG peg)
            {
                _peg = peg;
            }

            public int Evaluate(string input)
            {
                Instance i = new Instance();
                IPEGRun run = _peg.NewRun(ReaderHelper.NewStringReader(input));

                run.Hook += i.OnHook;
                if (!run.Execute())
                    throw new InvalidOperationException();
                return i.Result;
            }

            private class Instance
            {
                private Stack<int> _stack = new Stack<int>();

                public void OnHook(object sender, HookEventArg hook)
                {
                    if (hook.HookName == "push_number")
                        _stack.Push(Int32.Parse(hook.HookArgs[0]));
                    else if (hook.HookName == "push_op")
                    {
                        int right = _stack.Pop();
                        int left = _stack.Pop();

                        switch (hook.HookArgs[0].Trim())
                        {
                            case "+":
                                _stack.Push(left + right);
                                break;
                            case "-":
                                _stack.Push(left - right);
                                break;
                            case "*":
                                _stack.Push(left * right);
                                break;
                            case "/":
                                _stack.Push(left / right);
                                break;
                            case "%":
                                _stack.Push(left % right);
                                break;
                            default:
                                throw new InvalidOperationException();
                        }
                    }
                }

                public int Result
                {
                    get { return _stack.Peek(); }
                }
            }
        }
    }
}
