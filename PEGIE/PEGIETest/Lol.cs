﻿using System;
using PEGIE;
using PEGIE.DotRenderer;
using System.Text;
using PEGIE.StringRenderer;
using My.IO;

using NUnit.Framework;
using TestClassAttribute = NUnit.Framework.TestFixtureAttribute;
using TestMethodAttribute = NUnit.Framework.TestCaseAttribute;

namespace PEGIETest
{
    [TestClass]
    public class Lol
    {
        private IPEG _BuildPEG()
        {
            return _BuildPEG(PEGBuilder.GetDefaultRenderer());
        }

        private IPEG _BuildPEG(IPEGRenderer renderer)
        {
            PEGBuilder builder = new PEGBuilder(renderer);

            RuleBuilder main = builder.NewRule("main");
            main.Tree = new RuleCall("lol");

            RuleBuilder lol = builder.NewRule("lol");
            lol.Tree = And.MakeAnd(new Sequence("lol"), new OneOrNone(And.MakeAnd(new Sequence("o"), new RuleCall("lol"))));

            builder.StartingRule = main;
            return builder.Finalize();
        }
        
        [TestMethod]
        public void DotRendering()
        {
            _BuildPEG(new DotRenderer("lol.gv"));
        }

        [TestMethod]
        public void StringRendering()
        {
            StringBuilder tmp = new StringBuilder();
            _BuildPEG(new StringRenderer(tmp));
            Console.WriteLine(tmp);
        }

        [TestMethod]
        public void Tests()
        {
            IPEG peg = _BuildPEG();

            Assert.IsTrue(peg.NewRun(ReaderHelper.NewStringReader("lol")).Execute());
            Assert.IsFalse(peg.NewRun(ReaderHelper.NewStringReader("lul")).Execute());
            Assert.IsTrue(peg.NewRun(ReaderHelper.NewStringReader("lolololololololololololololololololololololololol")).Execute());
            Assert.IsTrue(peg.NewRun(ReaderHelper.NewStringReader("lolololololololololololololololololololololololol"), false).Execute());
        }
    }
}
