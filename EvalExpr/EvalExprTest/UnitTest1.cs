﻿/*
Copyright Julien Edmond (05/06/2014)

edmondju@gmail.com

This library is a .NET library whose purpose is to test the EvalExpr
library.

This software is governed by the CeCILL-C  license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EvalExprTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            Assert.AreEqual(3, EvalExpr.EvalExpr.Parse("1+2"));
            Assert.AreEqual(42, EvalExpr.EvalExpr.Parse("84/2"));
            Assert.AreEqual(1.5, EvalExpr.EvalExpr.Parse("3/2"));
            Assert.AreEqual(-1, EvalExpr.EvalExpr.Parse("1+2*(3-4)"));
            Assert.AreEqual(-176.3, EvalExpr.EvalExpr.Parse("-176.3"));
            Assert.AreEqual(1000, EvalExpr.EvalExpr.Parse("0.05 * 20000"));
        }

        [TestMethod]
        public void BenchMark10000()
        {
            for (int i = 0; i < 10000; i++)
            {
                Assert.AreEqual(176276, EvalExpr.EvalExpr.Parse("1+2-84/2*3/2+1+2*(3-4)-176.3*0.05 * 20000"));
            }
        }

        [TestMethod]
        public void BenchMark1000()
        {
            for (int i = 0; i < 1000; i++)
            {
                Assert.AreEqual(176276, EvalExpr.EvalExpr.Parse("1+2-84/2*3/2+1+2*(3-4)-176.3*0.05 * 20000"));
            }
        }

        [TestMethod]
        public void BenchMark100000()
        {
            for (int i = 0; i < 100000; i++)
            {
                Assert.AreEqual(176276, EvalExpr.EvalExpr.Parse("1+2-84/2*3/2+1+2*(3-4)-176.3*0.05 * 20000"));
            }
        }
    }
}
