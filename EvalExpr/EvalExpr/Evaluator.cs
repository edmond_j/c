﻿/*
Copyright Julien Edmond (05/06/2014)

edmondju@gmail.com

This library is a .NET library whose purpose is to provide a mathematical
syntax parser to developpers.

This software is governed by the CeCILL-C  license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

using PEGIE.Abstract;
using PEGIE.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvalExpr
{
    internal sealed class Evaluator : AHookHandler
    {
        private static CultureInfo _doubleCulture = new CultureInfo("en-US");
        private double _result;
        private Stack<double> _stack;

        public double Result
        {
            get { return _result; }
        }

        [HookHandler("start")]
        public void onStart()
        {
            _result = 0;
            _stack = new Stack<double>();
        }

        [HookHandler("end")]
        public void onEnd()
        {
            if (_stack.Count > 0)
                _result = _stack.Pop();
            else
                throw new EvalExprException("No stack size at end of parsing, something went wrong...");
            _stack = null;
        }

        [HookHandler("push_op")]
        public void onPushOp(char op)
        {
            if (_stack.Count < 2)
                throw new EvalExprException("No stack size lesser than 2 when arriving at an operand, something went wrong...");

            double right = _stack.Pop();
            double left = _stack.Pop();
            double res;

            try
            {
                switch (op)
                {
                    case '+':
                        res = left + right;
                        break;
                    case '-':
                        res = left - right;
                        break;
                    case '*':
                        res = left * right;
                        break;
                    case '/':
                        res = left / right;
                        break;
                    case '%':
                        res = left % right;
                        break;
                    default:
                        throw new EvalExprException("Operand unknown, something went wrong...");
                }
                _stack.Push(res);
            }
            catch (Exception e)
            {
                throw new EvalExprException("Exception on operand", e);
            }
        }

        private class DoubleConverter : TypeConverter
        {
            public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
            {
                if (value.GetType() == typeof(String))
                    return Double.Parse((string)value, _doubleCulture);
                return base.ConvertFrom(context, culture, value);
            }
        }

        [HookHandler("push_number")]
        public void onPushNumber([TypeConverter(typeof(DoubleConverter))] double nb)
        {
            _stack.Push(nb);
        }

        //[HookHandler("push_number")]
        //public void onPushNumber(string nb)
        //{
        //    try
        //    {
        //        _stack.Push(Double.Parse(nb, _doubleCulture));
        //    }
        //    catch (Exception e)
        //    {
        //        throw new EvalExprException(String.Format("Exception on number {0}", nb), e);
        //    }
        //}

        [HookHandler("unary_neg")]
        public void onUnaryNeg(string neg)
        {
            if (!String.IsNullOrWhiteSpace(neg))
                _stack.Push(-1.0 * _stack.Pop());
        }
    }
}
