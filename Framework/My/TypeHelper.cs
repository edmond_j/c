﻿/*
Copyright Julien Edmond (05/06/2014) 

edmondju@gmail.com

This library is a .NET library whose purpose is to help developpers in
diffent ways.

This software is governed by the CeCILL-C  license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace My
{
    /// <summary>
    /// Helper for Types
    /// </summary>
    public static class TypeHelper
    {
        /// <summary>
        /// Test if a type is nullable (can be set to null)
        /// </summary>
        /// <param name="t">The type to test</param>
        /// <returns>True if you can set this type to null</returns>
        public static bool IsNullable(this Type t)
        {
            return !t.IsValueType || Nullable.GetUnderlyingType(t) != null;
        }

        /// <summary>
        /// Test if a type implements an interface
        /// </summary>
        /// <param name="t">The type to test</param>
        /// <param name="itName">The name of the interface that must be implemented</param>
        /// <param name="ignoreCase">True to ignore case</param>
        /// <returns>True if <paramref name="t"/> implements the interface <paramref name="itName"/></returns>
        public static bool ImplementsInterface(this Type t, string itName, bool ignoreCase = false)
        {
            return t.GetInterface(itName, ignoreCase) != null;
        }

        /// <summary>
        /// Test if a type implements an interface
        /// </summary>
        /// <param name="t">The type to test</param>
        /// <param name="it">The type of the interface to implement</param>
        /// <returns>True if <paramref name="t"/> implements the interface <paramref name="it"/></returns>
        /// <exception cref="System.ArgumentException"><paramref name="it"/> is not an interface</exception>
        public static bool ImplementsInterface(this Type t, Type it)
        {
            if (!it.IsInterface)
                throw new ArgumentException("Type is not an interface", "it");
            return t.ImplementsInterface(it.FullName, false);
        }
    }
}
