﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace My.Runtime.Serialization
{
    /// <summary>
    /// Extensions for this namespace.
    /// </summary>
    public static class Extend
    {
        /// <summary>
        /// Retrieves a value from the System.Runtime.Serialization.SerializationInfo store.
        /// </summary>
        /// <typeparam name="T">The type of the value to retrieve. If the stored value cannot be converted to this type, the system will throw a <see cref="System.InvalidCastException"/>.</typeparam>
        /// <param name="info">The instace of the current <see cref="SerializationInfo"/>.</param>
        /// <param name="name">The name associated with the value to retrieve.</param>
        /// <returns>The object of the specified System.Type associated with name.</returns>
        /// <exception cref="System.ArgumentNullException">name or type is null.</exception>
        /// <exception cref="System.InvalidCastException">The value associated with name cannot be converted to type.</exception>
        /// <exception cref="System.Runtime.Serialization.SerializationException">An element with the specified name is not found in the current instance.</exception>
        public static T GetValue<T>(this SerializationInfo info, string name)
        {
            return (T)info.GetValue(name, typeof(T));
        }
    }
}
