﻿/*
Copyright Julien Edmond (05/06/2014) 

edmondju@gmail.com

This library is a .NET library whose purpose is to help developpers in
diffent ways.

This software is governed by the CeCILL-C  license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace My
{
    /// <summary>
    /// Extensions for <see cref="System.Collections.Generic.IDictionary&lt;TKey, TValue&gt;"/>.
    /// </summary>
    public static class DictionnaryExtend
    {
        /// <summary>
        /// Gets a value inside a Dictionnary or a default value.
        /// </summary>
        /// <typeparam name="TKey">The Type of the keys.</typeparam>
        /// <typeparam name="TValue">The Type of the values.</typeparam>
        /// <param name="dict">The Dictionnary.</param>
        /// <param name="key">The key where the value might be stored.</param>
        /// <param name="def">The default value.</param>
        /// <returns>The value stored at the key <paramref name="key"/> or the value of <paramref name="def"/></returns>
        public static TValue GetValueOrDefault<TKey, TValue>(this IDictionary<TKey, TValue> dict, TKey key, TValue def = default(TValue))
        {
            TValue sent;

            if (!dict.TryGetValue(key, out sent))
                sent = def;
            return sent;
        }
    }
}
