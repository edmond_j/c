﻿/*
Copyright Julien Edmond (05/06/2014) 

edmondju@gmail.com

This library is a .NET library whose purpose is to help developpers in
diffent ways.

This software is governed by the CeCILL-C  license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace My
{
    /// <summary>
    /// Classic implementation of <see cref="ITypedVariable"/>.
    /// </summary>
    public class ClassicTypedVariable : ITypedVariable
    {
        Type _type;
        object _value;

        /// <summary>
        /// Initializes a new instance of the <see cref="ClassicTypedVariable"/> class.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="value">The value.</param>
        public ClassicTypedVariable(Type type, object value)
        {
            _type = type;
            _value = Caster.ReflectiveCast(value, type);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ClassicTypedVariable"/> class.
        /// </summary>
        /// <param name="value">The value.</param>
        public ClassicTypedVariable(object value)
        {
            _type = value.GetType();
            _value = value;
        }

        /// <summary>
        /// Gets the type.
        /// </summary>
        /// <value>
        /// The type of the value.
        /// </value>
        public Type Type
        {
            get { return _type; }
        }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        public object Value
        {
            get
            {
                return _value;
            }
            set
            {
                _value = Caster.ReflectiveCast(value, _type);
            }
        }
    }

    /// <summary>
    /// Generic implementation of the <see cref="ITypedVariable"/>.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class GenericTypedVariable<T> : ITypedVariable
    {
        T _value;

        /// <summary>
        /// Initializes a new instance of the <see cref="GenericTypedVariable{T}"/> class.
        /// </summary>
        /// <param name="value">The value.</param>
        public GenericTypedVariable(T value)
        {
            _value = value;
        }

        /// <summary>
        /// Gets the type.
        /// </summary>
        /// <value>
        /// The type of the value.
        /// </value>
        public Type Type
        {
            get { return typeof(T); }
        }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        public object Value
        {
            get
            {
                return _value;
            }
            set
            {
                _value = (T)value;
            }
        }
    }
}
