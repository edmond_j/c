﻿/*
Copyright Julien Edmond (05/06/2014) 

edmondju@gmail.com

This library is a .NET library whose purpose is to help developpers in
diffent ways.

This software is governed by the CeCILL-C  license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

#if DEBUG
using System.IO;
#endif

namespace My.Collection.Generic
{
    /// <summary>
    /// Class representing a Red Black Tree (an optimized sorted collection).
    /// </summary>
    /// <typeparam name="T">Type of the items to store.</typeparam>
    [Serializable]
    public class RedBlackTree<T> : AbstractDisposable, IDisposable, ICollection<T>, IReadOnlyCollection<T>, IEnumerable<T>, ICollection, IEnumerable, ISerializable, IDeserializationCallback
    {
        #region Node

        internal class Node : AbstractDisposable
        {
            public bool IsRed = true;
            public T Item;
            public Node Left = null;
            public Node Right = null;
            public Node Parent = null;

            public Node(T item)
            {
                Item = item;
            }

            protected override void DisposeManagedObjects()
            {
                try
                {
                    Item = default(T);
                    Left = null;
                    Right = null;
                    Parent = null;
                }
                finally
                {
                    base.DisposeManagedObjects();
                }
            }

            public override string ToString()
            {
                return Item.ToString();
            }

            internal void RecurseDispose()
            {
                if (Left != null)
                    Left.RecurseDispose();
                if (Right != null)
                    Right.RecurseDispose();
                this.Dispose();
            }
        }

        #endregion

        #region Enumerator

        private class NodeEnumerator : IEnumerator<T>
        {
            private Node _current = null;
            private Queue<Node> _nodes;
            private RedBlackTree<T> _collection;

            public NodeEnumerator(RedBlackTree<T> collection)
            {
                _nodes = null;
                _collection = collection;
                Reset();
            }

            private void _AddNode(Node node)
            {
                if (node != null)
                {
                    _AddNode(node.Left);
                    _nodes.Enqueue(node);
                    _AddNode(node.Right);
                }
            }

            public T Current
            {
                get { return _current != null ? _current.Item : default(T); }
            }

            public void Dispose()
            {
                _nodes = null;
            }

            object IEnumerator.Current
            {
                get { return Current; }
            }

            public bool MoveNext()
            {
                if (_nodes.Count <= 0)
                    return false;
                _current = _nodes.Dequeue();
                return true;
            }

            public void Reset()
            {
                _nodes = new Queue<Node>(_collection.Count);
                _AddNode(_collection._tree);
            }
        }

        #endregion

        private Node _tree = null;
        private int _count = 0;
        private SerializationInfo _si = null;
        private IComparer<T> _comparer;
        private object _syncroot = new object();

        /// <summary>
        /// Dispose of the managed objects inside your object
        /// </summary>
        protected override void DisposeManagedObjects()
        {
            try
            {
                if (_tree != null)
                {
                    _tree.RecurseDispose();
                    _tree = null;
                }
                _si = null;
                _comparer = null;
                _syncroot = null;
            }
            finally
            {
                base.DisposeManagedObjects();
            }
        }

        /// <summary>
        /// Gets or sets the comparer.
        /// </summary>
        /// <value>
        /// The comparer.
        /// </value>
        public IComparer<T> Comparer
        {
            get { return _comparer; }
            set { _comparer = value; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RedBlackTree{T}"/> class.
        /// </summary>
        public RedBlackTree()
        {
            _comparer = Comparer<T>.Default;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RedBlackTree{T}"/> class.
        /// </summary>
        /// <param name="comparer">The comparer.</param>
        public RedBlackTree(IComparer<T> comparer)
        {
            if (comparer != null)
                _comparer = comparer;
            else
                _comparer = Comparer<T>.Default;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RedBlackTree{T}"/> class.
        /// </summary>
        /// <param name="collection">The collection.</param>
        public RedBlackTree(IEnumerable<T> collection) : this(collection, Comparer<T>.Default)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RedBlackTree{T}"/> class.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="context">The context.</param>
        protected RedBlackTree(SerializationInfo info, StreamingContext context)
        {
            _si = info;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RedBlackTree{T}"/> class.
        /// </summary>
        /// <param name="collection">The collection.</param>
        /// <param name="comparer">The comparer.</param>
        /// <exception cref="System.ArgumentNullException">collection</exception>
        public RedBlackTree(IEnumerable<T> collection, IComparer<T> comparer) : this(comparer)
        {
            if (collection == null)
            {
                throw new ArgumentNullException("collection");
            }
            
            // TODO Optimize that shit
            foreach (T item in collection)
            {
                this.Add(item);
            }
        }

        /// <summary>
        /// Adds an item to the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <param name="item">The object to add to the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        public void Add(T item)
        {
            lock (_syncroot)
            {
                Node node = new Node(item);
                _TreeInsert(node);

                _EnsureTreeValidityOnAdd(node);

                ++_count;
            }
        }

        private void _EnsureTreeValidityOnAdd(Node node)
        {
            Node parent = node.Parent;
            Node grandParent = null;
            Node uncle = null;

            // Case 1: root of the tree
            if (parent == null)
            {
                node.IsRed = false;
                return;
            }

            // Case 2: tree is valid
            if (!parent.IsRed)
                return;

            grandParent = parent.Parent;

            if (grandParent != null)
                uncle = grandParent.Left == parent ? grandParent.Right : grandParent.Left;

            // Case 3: parent and uncle are red
            if (uncle != null && uncle.IsRed)
            {
                parent.IsRed = false;
                uncle.IsRed = false;
                grandParent.IsRed = true;
                _EnsureTreeValidityOnAdd(grandParent);
                return;
            }

            // Case 4: parent is red, uncle is black and parent and node are on opposite side
            if (node == parent.Right && parent == grandParent.Left)
            {
                _RotateLeft(parent);
                node = node.Left;
            }
            else if (node == parent.Left && parent == grandParent.Right)
            {
                _RotateRight(parent);
                node = node.Right;
            }

            parent = node.Parent;
            grandParent = parent.Parent;
 
            // Case 5: parent is red, uncle is black and parent and node are on the same side

            parent.IsRed = false;
            grandParent.IsRed = true;

            if (node == parent.Left)
                _RotateRight(grandParent);
            else
                _RotateLeft(grandParent);
        }

        private void _RotateLeft(Node node)
        {
            // right son become parent

            Node y = node.Right;

            node.Right = y.Left;

            if (y.Left != null)
                y.Left.Parent = node;
            y.Parent = node.Parent;
            if (node.Parent == null)
                _tree = y;
            else if (node == node.Parent.Left)
                node.Parent.Left = y;
            else
                node.Parent.Right = y;
            y.Left = node;
            node.Parent = y;
        }

        private void _RotateRight(Node node)
        {
            // left son become parent

            Node y = node.Left;

            node.Left = y.Right;

            if (y.Right != null)
                y.Right.Parent = node;
            y.Parent = node.Parent;
            if (node.Parent == null)
                _tree = y;
            else if (node == node.Parent.Left)
                node.Parent.Left = y;
            else
                node.Parent.Right = y;
            y.Right = node;
            node.Parent = y;
        }

        private void _TreeInsert(Node node)
        {
            Node wanderer;
            bool goLeft;
            if (_tree == null)
                _tree = node;
            else
            {
                wanderer = _tree;
                
                while (node.Parent == null)
                {
                    goLeft = Comparer.Compare(wanderer.Item, node.Item) >= 0;

                    if (goLeft)
                    {
                        if (wanderer.Left == null)
                        {
                            wanderer.Left = node;
                            node.Parent = wanderer;
                        }
                        else
                            wanderer = wanderer.Left;
                    }
                    else
                    {
                        if (wanderer.Right == null)
                        {
                            wanderer.Right = node;
                            node.Parent = wanderer;
                        }
                        else
                            wanderer = wanderer.Right;
                    }
                }
            }
        }

        /// <summary>
        /// Removes all items from the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <exception cref="System.NotImplementedException"></exception>
        public void Clear()
        {
            _count = 0;
            _tree.RecurseDispose();
            _tree = null;
        }

        /// <summary>
        /// Determines whether the <see cref="T:System.Collections.Generic.ICollection`1" /> contains a specific value.
        /// </summary>
        /// <param name="item">The object to locate in the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        /// <returns>
        /// true if <paramref name="item" /> is found in the <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise, false.
        /// </returns>
        public bool Contains(T item)
        {
            return _Contains(_tree, item);
        }

        private bool _Contains(Node node, T item)
        {
            int res;

            if (node == null)
                return false;
            res = Comparer.Compare(node.Item, item);

            if (res == 0)
                return true;
            if (res < 0)
                return _Contains(node.Left, item);
            return _Contains(node.Right, item);
        }

        /// <summary>
        /// Copies to.
        /// </summary>
        /// <param name="array">The array.</param>
        /// <param name="arrayIndex">Index of the array.</param>
        public void CopyTo(T[] array, int arrayIndex)
        {
            foreach (T item in this)
            {
                array[arrayIndex] = item;
                ++arrayIndex;
            }
        }

        /// <summary>
        /// Gets the number of elements contained in the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        public int Count
        {
            get { return _count; }
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="T:System.Collections.Generic.ICollection`1" /> is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return false; }
        }

        /// <summary>
        /// Removes the first occurrence of a specific object from the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <param name="item">The object to remove from the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        /// <returns>
        /// true if <paramref name="item" /> was successfully removed from the <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise, false. This method also returns false if <paramref name="item" /> is not found in the original <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </returns>
        public bool Remove(T item)
        {
            lock (_syncroot)
            {
                Node toRemove = _FindNode(_tree, item);
                
                if (toRemove == null)
                    return false;

                _RemoveNode(toRemove);

                //_RemoveNode(toRemove);

                //toRemove.Dispose();

                return true;
            }
        }

        private void _RemoveNode(Node node)
        {
            Node toRemove;

            if (node.Left == null || node.Right == null)
                toRemove = node;
            else
                toRemove = _FindSuccessor(node);

            Node child;

            if (toRemove.Left == null)
                child = toRemove.Right;
            else
                child = toRemove.Left;

            if (child != null)
                child.Parent = toRemove.Parent;

            Node parent = toRemove.Parent;
            bool isLeftSon = false;

            if (parent == null)
                _tree = child;
            else if (parent.Left == toRemove)
            {
                isLeftSon = true;
                parent.Left = child;
            }
            else
                parent.Right = child;

            if (!ReferenceEquals(toRemove, node))
                node.Item = toRemove.Item;

            if (!toRemove.IsRed)
                _EnsureTreeValidityOnRemove(child, parent, isLeftSon);

            toRemove.Dispose();
        }

        private void _EnsureTreeValidityOnRemove(Node node, Node parent, bool isLeftSon)
        {
            Node tmp;

            while (node != _tree && _IsBlack(node))
            {
                if (isLeftSon)
                {
                    tmp = parent.Right;
                    if (_IsRed(tmp))
                    {
                        tmp.IsRed = false;
                        parent.IsRed = true;
                        _RotateLeft(parent);
                        tmp = parent.Right;
                    }
                    if (_IsBlack(tmp.Left) && _IsBlack(tmp.Right))
                    {
                        tmp.IsRed = true;
                        node = parent;
                        parent = parent.Parent;
                        isLeftSon = (node == parent.Left);
                    }
                    else
                    {
                        if (_IsBlack(tmp.Right))
                        {
                            tmp.Left.IsRed = false;
                            tmp.IsRed = true;
                            _RotateRight(tmp);
                            tmp = parent.Right;
                        }
                        tmp.IsRed = parent.IsRed;
                        parent.IsRed = false;
                        if (tmp.Right != null)
                            tmp.Right.IsRed = false;
                        _RotateLeft(parent);
                        node = _tree;
                        parent = null;
                    }
                }
                else
                {
                    tmp = parent.Left;
                    if (_IsRed(tmp))
                    {
                        tmp.IsRed = false;
                        parent.IsRed = true;
                        _RotateRight(parent);
                        tmp = parent.Left;
                    }

                    if (_IsBlack(tmp.Left) && _IsBlack(tmp.Right))
                    {
                        tmp.IsRed = true;
                        node = parent;
                        parent = node.Parent;
                        isLeftSon = (node == parent.Left);
                    }
                    else
                    {
                        if (_IsBlack(tmp.Left))
                        {
                            tmp.Right.IsRed = false;
                            tmp.IsRed = true;
                            _RotateLeft(tmp);
                            tmp = parent.Left;
                        }
                        tmp.IsRed = parent.IsRed;
                        parent.IsRed = false;
                        if (tmp.Left != null)
                            tmp.Left.IsRed = false;
                        _RotateRight(parent);
                        node = _tree;
                        parent = null;
                    }
                }
            }
        }

        private static bool _IsRed(Node tmp)
        {
            return !_IsBlack(tmp);
        }

        private static bool _IsBlack(Node node)
        {
            return node == null || !node.IsRed;
        }

        private static Node _FindSuccessor(Node node)
        {
            Node sent;
            bool goLeft;

            if (node.Right == null)
            {
                sent = node.Left;
                goLeft = false;
            }
            else
            {
                sent = node.Right;
                goLeft = true;
            }

            while ((goLeft && sent.Left != null) || sent.Right != null)
                sent = goLeft ? sent.Left : sent.Right;

            return sent;
        }

        private Node _FindNode(Node node, T item)
        {
            int res;

            if (node == null)
                return null;
            res = Comparer.Compare(node.Item, item);

            if (res == 0)
                return node;
            if (res > 0)
                return _FindNode(node.Left, item);
            return _FindNode(node.Right, item);
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.
        /// </returns>
        public IEnumerator<T> GetEnumerator()
        {
            return new NodeEnumerator(this);
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        /// <summary>
        /// Populates a <see cref="T:System.Runtime.Serialization.SerializationInfo" /> with the data needed to serialize the target object.
        /// </summary>
        /// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> to populate with data.</param>
        /// <param name="context">The destination (see <see cref="T:System.Runtime.Serialization.StreamingContext" />) for this serialization.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
                throw new ArgumentNullException("info");
            info.AddValue("Count", _count);
            info.AddValue("Comparer", _comparer, typeof(IComparer<T>));
            if (_tree != null)
            {
                T[] array = new T[this.Count];
                this.CopyTo(array, 0);
                info.AddValue("Items", array, typeof(T[]));
            }
        }

        /// <summary>
        /// Copies the elements of the <see cref="T:System.Collections.ICollection" /> to an <see cref="T:System.Array" />, starting at a particular <see cref="T:System.Array" /> index.
        /// </summary>
        /// <param name="array">The one-dimensional <see cref="T:System.Array" /> that is the destination of the elements copied from <see cref="T:System.Collections.ICollection" />. The <see cref="T:System.Array" /> must have zero-based indexing.</param>
        /// <param name="index">The zero-based index in <paramref name="array" /> at which copying begins.</param>
        public void CopyTo(Array array, int index)
        {
            foreach (T item in this)
            {
                array.SetValue(item, index);
                ++index;
            }
        }

        /// <summary>
        /// Gets a value indicating whether access to the <see cref="T:System.Collections.ICollection" /> is synchronized (thread safe).
        /// </summary>
        public bool IsSynchronized
        {
            get { return true; }
        }

        /// <summary>
        /// Gets an object that can be used to synchronize access to the <see cref="T:System.Collections.ICollection" />.
        /// </summary>
        public object SyncRoot
        {
            get { return _syncroot; }
        }

        /// <summary>
        /// Runs when the entire object graph has been deserialized.
        /// </summary>
        /// <param name="sender">The object that initiated the callback. The functionality for this parameter is not currently implemented.</param>
        public void OnDeserialization(object sender)
        {
            if (_comparer != null)
                return;
            if (_si == null)
                throw new SerializationException("Invalid deserialization");
            _comparer = (IComparer<T>)_si.GetValue("Comparer", typeof(IComparer<T>));
            int @int = _si.GetInt32("Count");
            if (@int != 0)
            {
                T[] array = (T[])_si.GetValue("Items", typeof(T[]));
                if (array == null)
                    throw new SerializationException("Missing values");
                for (int i = 0; i < array.Length; i++)
                    this.Add(array[i]);
            }
            if (_count != @int)
            {
                throw new SerializationException("Mismatch count");
            }
            _si = null;
        }

#if DEBUG

        public void ToDot(string fileName)
        {
            TextWriter writer = new StreamWriter((File.Exists(fileName) ? File.Open(fileName, FileMode.Truncate) : File.Create(fileName)));

            writer.WriteLine("digraph tree {");

            _WriteNode(writer, _tree);

            writer.WriteLine("}");

            writer.Close();
        }

        private void _WriteNode(TextWriter writer, Node node)
        {
            if (node != null)
            {
                writer.WriteLine("{0} [label=\"{1}\"] [style=filled] [fontcolor=\"white\"] [fontsize=18] [fillcolor=\"{2}\"]", node.GetHashCode(), node.Item, node.IsRed ? "red" : "black");
                if (node.Left != null)
                {
                    writer.WriteLine("{0} -> {1} [label=left]", node.GetHashCode(), node.Left.GetHashCode());
                    _WriteNode(writer, node.Left);
                }
                if (node.Right != null)
                {
                    writer.WriteLine("{0} -> {1} [label=right]", node.GetHashCode(), node.Right.GetHashCode());
                    _WriteNode(writer, node.Right);
                }
            }
        }

#endif
    }
}
