﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace My.Reflection.Emit
{
    public abstract class ILGeneratorCollection: ABaseILGenerator, ICollection<IILGenerator>
    {
        private ICollection<IILGenerator> _generators;

        public ILGeneratorCollection(ICollection<IILGenerator> generators)
        {
            _generators = generators;
        }

        public int ILOffset
        {
            get { throw new InvalidOperationException("Cannot call this method on ILGeneratorCollection"); }
        }

        public void BeginCatchBlock(Type exceptionType)
        {
            _generator.BeginCatchBlock(exceptionType);
        }

        public void BeginExceptFilterBlock()
        {
            _generator.BeginExceptFilterBlock();
        }

        public Label BeginExceptionBlock()
        {
            return _generator.BeginExceptionBlock();
        }

        public void BeginFaultBlock()
        {
            _generator.BeginFaultBlock();
        }

        public void BeginFinallyBlock()
        {
            _generator.BeginFinallyBlock();
        }

        public void BeginScope()
        {
            _generator.BeginScope();
        }

        public LocalBuilder DeclareLocal(Type localType)
        {
            return _generator.DeclareLocal(localType);
        }

        public LocalBuilder DeclareLocal(Type localType, bool pinned)
        {
            return _generator.DeclareLocal(localType, pinned);
        }

        public Label DefineLabel()
        {
            return _generator.DefineLabel();
        }

        public void Emit(OpCode opcode)
        {
            _generator.Emit(opcode);
        }

        public void Emit(OpCode opcode, byte arg)
        {
            _generator.Emit(opcode, arg);
        }

        public void Emit(OpCode opcode, ConstructorInfo con)
        {
            _generator.Emit(opcode, con);
        }

        public void Emit(OpCode opcode, double arg)
        {
            _generator.Emit(opcode, arg);
        }

        public void Emit(OpCode opcode, FieldInfo field)
        {
            _generator.Emit(opcode, field);
        }

        public void Emit(OpCode opcode, float arg)
        {
            _generator.Emit(opcode, arg);
        }

        public void Emit(OpCode opcode, int arg)
        {
            _generator.Emit(opcode, arg);
        }

        public void Emit(OpCode opcode, Label label)
        {
            _generator.Emit(opcode, label);
        }

        public void Emit(OpCode opcode, Label[] labels)
        {
            _generator.Emit(opcode, labels);
        }

        public void Emit(OpCode opcode, LocalBuilder local)
        {
            _generator.Emit(opcode, local);
        }

        public void Emit(OpCode opcode, long arg)
        {
            _generator.Emit(opcode, arg);
        }

        public void Emit(OpCode opcode, MethodInfo meth)
        {
            _generator.Emit(opcode, meth);
        }

        public void Emit(OpCode opcode, sbyte arg)
        {
            _generator.Emit(opcode, arg);
        }

        public void Emit(OpCode opcode, short arg)
        {
            _generator.Emit(opcode, arg);
        }

        public void Emit(OpCode opcode, SignatureHelper signature)
        {
            _generator.Emit(opcode, signature);
        }

        public void Emit(OpCode opcode, string str)
        {
            _generator.Emit(opcode, str);
        }

        public void Emit(OpCode opcode, Type cls)
        {
            _generator.Emit(opcode, cls);
        }

        public void EmitCall(OpCode opcode, MethodInfo methodInfo, Type[] optionalParameterTypes)
        {
            _generator.EmitCall(opcode, methodInfo, optionalParameterTypes);
        }

        public void EmitCalli(OpCode opcode, CallingConvention unmanagedCallConv, Type returnType, Type[] parameterTypes)
        {
            _generator.EmitCalli(opcode, unmanagedCallConv, returnType, parameterTypes);
        }

        public void EmitCalli(OpCode opcode, CallingConventions callingConvention, Type returnType, Type[] parameterTypes, Type[] optionalParameterTypes)
        {
            _generator.EmitCalli(opcode, callingConvention, returnType, parameterTypes, optionalParameterTypes);
        }

        public void EmitWriteLine(FieldInfo fld)
        {
            _generator.EmitWriteLine(fld);
        }

        public void EmitWriteLine(LocalBuilder localBuilder)
        {
            _generator.EmitWriteLine(localBuilder);
        }

        public void EmitWriteLine(string value)
        {
            _generator.EmitWriteLine(value);
        }

        public void EndExceptionBlock()
        {
            _generator.EndExceptionBlock();
        }

        public void EndScope()
        {
            _generator.EndScope();
        }

        public void MarkLabel(Label loc)
        {
            _generator.MarkLabel(loc);
        }

        public void MarkSequencePoint(ISymbolDocumentWriter document, int startLine, int startColumn, int endLine, int endColumn)
        {
            _generator.MarkSequencePoint(document, startLine, startColumn, endLine, endColumn);
        }

        public void ThrowException(Type excType)
        {
            _generator.ThrowException(excType);
        }

        public void UsingNamespace(string usingNamespace)
        {
            _generator.UsingNamespace(usingNamespace);
        }
        
        public void Add(IILGenerator item)
        {
            throw new NotImplementedException();
        }

        public void Clear()
        {
            throw new NotImplementedException();
        }

        public bool Contains(IILGenerator item)
        {
            throw new NotImplementedException();
        }

        public void CopyTo(IILGenerator[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public int Count
        {
            get { throw new NotImplementedException(); }
        }

        public bool IsReadOnly
        {
            get { throw new NotImplementedException(); }
        }

        public bool Remove(IILGenerator item)
        {
            throw new NotImplementedException();
        }

        public IEnumerator<IILGenerator> GetEnumerator()
        {
            throw new NotImplementedException();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }
}
