﻿/*
Copyright Julien Edmond (05/06/2014) 

edmondju@gmail.com

This library is a .NET library whose purpose is to help developpers in
diffent ways.

This software is governed by the CeCILL-C  license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;

namespace My.Reflection.Emit
{
    /// <summary>
    /// This class represent a pool of <c>LocalBuilder</c> based on their types and if they are pinned
    /// </summary>
    public class LocalPool
    {
        private IILGenerator _generator;
        private Dictionary<bool, Dictionary<Type, Queue<LocalBuilder>>> _pool;
        private bool _handleDispose;

        /// <summary>
        /// Construct a <c>LocalPool</c>
        /// </summary>
        /// <param name="generator">The IILGenerator that contains the locals</param>
        /// <param name="handleDispose">True if the local pool must generate IL code that dispose the local variable</param>
        public LocalPool(IILGenerator generator, bool handleDispose = true)
        {
            _generator = generator;
            _pool = new Dictionary<bool, Dictionary<Type, Queue<LocalBuilder>>>();
            _handleDispose = handleDispose;
        }

        /// <summary>
        /// Retrieve a LocalBuilder with the correspondant type
        /// </summary>
        /// <param name="localType">The type of the local variable</param>
        /// <param name="pinned">Whether the local is pinned or not</param>
        /// <returns>An available <c>LocalBuilder</c> or a new <c>LocalBuilder</c></returns>
        public LocalBuilder GetLocal(Type localType, bool pinned = false)
        {
            LocalBuilder sent = null;

            foreach (KeyValuePair<bool, Dictionary<Type, Queue<LocalBuilder>>> item in _pool)
            {
                if (item.Key == pinned)
                {
                    foreach (KeyValuePair<Type, Queue<LocalBuilder>> item2 in item.Value)
                    {
                        if (item2.Key == localType)
                        {
                            if (item2.Value.Count > 0)
                                sent = item2.Value.Dequeue();
                            break;
                        }
                    }
                    break;
                }
            }
            if (sent == null)
                sent = _generator.DeclareLocal(localType, pinned);
            return sent;
        }

        /// <summary>
        /// Say that a local is no longer used and can be reused. If the <c>LocalPool</c> handles the disposing of object,
        /// some IL code will be generated at this point
        /// </summary>
        /// <param name="local">The <c>LocalBuilder</c> to release</param>
        public void ReleaseLocal(LocalBuilder local)
        {
            if (!_pool.ContainsKey(local.IsPinned))
                _pool[local.IsPinned] = new Dictionary<Type, Queue<LocalBuilder>>();
            if (!_pool[local.IsPinned].ContainsKey(local.LocalType))
                _pool[local.IsPinned][local.LocalType] = new Queue<LocalBuilder>();
            _pool[local.IsPinned][local.LocalType].Enqueue(local);
            if (_handleDispose)
                _HandleDispose(local);
        }

        private static MethodInfo _DisposeMethod = typeof(IDisposable).GetMethod("Dispose");

        private void _HandleDispose(LocalBuilder local)
        {
            Label end = _generator.DefineLabel();

            if (local.LocalType.ImplementsInterface("IDisposable"))
            {
                _generator.Emit(OpCodes.Ldloc, local);
                _generator.Emit(OpCodes.Brfalse, end);
                _generator.Emit(OpCodes.Ldloc, local);
                _generator.EmitCall(OpCodes.Callvirt, _DisposeMethod, null);
            }
            if (!local.LocalType.IsValueType)
            {
                _generator.Emit(OpCodes.Ldnull);
                _generator.Emit(OpCodes.Stloc, local);
            }
            _generator.MarkLabel(end);
        }
    }
}
