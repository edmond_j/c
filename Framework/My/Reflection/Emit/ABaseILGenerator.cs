﻿/*
Copyright Julien Edmond (05/06/2014) 

edmondju@gmail.com

This library is a .NET library whose purpose is to help developpers in
diffent ways.

This software is governed by the CeCILL-C  license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace My.Reflection.Emit
{
    /// <summary>
    /// Base class for an ILGenerator that implements IILGenerator
    /// </summary>
    public abstract class ABaseILGenerator : IILGenerator
    {
        /// <summary>
        /// 
        /// </summary>
        public abstract int ILOffset { get; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="exceptionType"></param>
        public abstract void BeginCatchBlock(Type exceptionType);
        /// <summary>
        /// 
        /// </summary>
        public abstract void BeginExceptFilterBlock();
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public abstract System.Reflection.Emit.Label BeginExceptionBlock();
        /// <summary>
        /// 
        /// </summary>
        public abstract void BeginFaultBlock();
        /// <summary>
        /// 
        /// </summary>
        public abstract void BeginFinallyBlock();
        /// <summary>
        /// 
        /// </summary>
        public abstract void BeginScope();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="localType"></param>
        /// <returns></returns>
        public abstract System.Reflection.Emit.LocalBuilder DeclareLocal(Type localType);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="localType"></param>
        /// <param name="pinned"></param>
        /// <returns></returns>
        public abstract System.Reflection.Emit.LocalBuilder DeclareLocal(Type localType, bool pinned);
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public abstract System.Reflection.Emit.Label DefineLabel();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="opcode"></param>
        public abstract void Emit(System.Reflection.Emit.OpCode opcode);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="opcode"></param>
        /// <param name="arg"></param>
        public abstract void Emit(System.Reflection.Emit.OpCode opcode, byte arg);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="opcode"></param>
        /// <param name="con"></param>
        public abstract void Emit(System.Reflection.Emit.OpCode opcode, System.Reflection.ConstructorInfo con);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="opcode"></param>
        /// <param name="arg"></param>
        public abstract void Emit(System.Reflection.Emit.OpCode opcode, double arg);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="opcode"></param>
        /// <param name="field"></param>
        public abstract void Emit(System.Reflection.Emit.OpCode opcode, System.Reflection.FieldInfo field);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="opcode"></param>
        /// <param name="arg"></param>
        public abstract void Emit(System.Reflection.Emit.OpCode opcode, float arg);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="opcode"></param>
        /// <param name="arg"></param>
        public abstract void Emit(System.Reflection.Emit.OpCode opcode, int arg);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="opcode"></param>
        /// <param name="label"></param>
        public abstract void Emit(System.Reflection.Emit.OpCode opcode, System.Reflection.Emit.Label label);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="opcode"></param>
        /// <param name="labels"></param>
        public abstract void Emit(System.Reflection.Emit.OpCode opcode, System.Reflection.Emit.Label[] labels);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="opcode"></param>
        /// <param name="local"></param>
        public abstract void Emit(System.Reflection.Emit.OpCode opcode, System.Reflection.Emit.LocalBuilder local);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="opcode"></param>
        /// <param name="arg"></param>
        public abstract void Emit(System.Reflection.Emit.OpCode opcode, long arg);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="opcode"></param>
        /// <param name="meth"></param>
        public abstract void Emit(System.Reflection.Emit.OpCode opcode, System.Reflection.MethodInfo meth);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="opcode"></param>
        /// <param name="arg"></param>
        public abstract void Emit(System.Reflection.Emit.OpCode opcode, short arg);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="opcode"></param>
        /// <param name="signature"></param>
        public abstract void Emit(System.Reflection.Emit.OpCode opcode, System.Reflection.Emit.SignatureHelper signature);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="opcode"></param>
        /// <param name="str"></param>
        public abstract void Emit(System.Reflection.Emit.OpCode opcode, string str);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="opcode"></param>
        /// <param name="cls"></param>
        public abstract void Emit(System.Reflection.Emit.OpCode opcode, Type cls);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="opcode"></param>
        /// <param name="methodInfo"></param>
        /// <param name="optionalParameterTypes"></param>
        public abstract void EmitCall(System.Reflection.Emit.OpCode opcode, System.Reflection.MethodInfo methodInfo, Type[] optionalParameterTypes);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="opcode"></param>
        /// <param name="unmanagedCallConv"></param>
        /// <param name="returnType"></param>
        /// <param name="parameterTypes"></param>
        public abstract void EmitCalli(System.Reflection.Emit.OpCode opcode, System.Runtime.InteropServices.CallingConvention unmanagedCallConv, Type returnType, Type[] parameterTypes);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="opcode"></param>
        /// <param name="callingConvention"></param>
        /// <param name="returnType"></param>
        /// <param name="parameterTypes"></param>
        /// <param name="optionalParameterTypes"></param>
        public abstract void EmitCalli(System.Reflection.Emit.OpCode opcode, System.Reflection.CallingConventions callingConvention, Type returnType, Type[] parameterTypes, Type[] optionalParameterTypes);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="fld"></param>
        public abstract void EmitWriteLine(System.Reflection.FieldInfo fld);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="localBuilder"></param>
        public abstract void EmitWriteLine(System.Reflection.Emit.LocalBuilder localBuilder);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        public abstract void EmitWriteLine(string value);
        /// <summary>
        /// 
        /// </summary>
        public abstract void EndExceptionBlock();
        /// <summary>
        /// 
        /// </summary>
        public abstract void EndScope();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="loc"></param>
        public abstract void MarkLabel(System.Reflection.Emit.Label loc);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="document"></param>
        /// <param name="startLine"></param>
        /// <param name="startColumn"></param>
        /// <param name="endLine"></param>
        /// <param name="endColumn"></param>
        public abstract void MarkSequencePoint(System.Diagnostics.SymbolStore.ISymbolDocumentWriter document, int startLine, int startColumn, int endLine, int endColumn);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="excType"></param>
        public abstract void ThrowException(Type excType);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="usingNamespace"></param>
        public abstract void UsingNamespace(string usingNamespace);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="local"></param>
        public abstract void ReleaseLocal(System.Reflection.Emit.LocalBuilder local);
    }
}
