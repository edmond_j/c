﻿/*
Copyright Julien Edmond (05/06/2014) 

edmondju@gmail.com

This library is a .NET library whose purpose is to help developpers in
diffent ways.

This software is governed by the CeCILL-C  license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

using System;
using System.Collections.Generic;
using System.Diagnostics.SymbolStore;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace My.Reflection.Emit
{
    public static partial class Extends
    {
        /// <summary>
        /// Link an IILGenerator with another IILGenerator, so that when some code is emited
        /// on the returned object, it will be emited in both IILGenerator
        /// </summary>
        /// <param name="me">The first IILGenerator to link</param>
        /// <param name="son">The secont IILGenerator to link</param>
        /// <returns>An object that emit in both <see cref="IILGenerator"/></returns>
        public static IILGenerator LinkWith(this IILGenerator me, IILGenerator son)
        {
            return new LinkedILGenerator(me, son);
        }
    }

    /// <summary>
    /// This class is an IILGenerator that links two IILGenerator, so that when some code is emited
    /// on the returned object, it will be emited in both IILGenerator
    /// </summary>
    public sealed class LinkedILGenerator : ABaseILGenerator
    {
        private IILGenerator _generator;
        private IILGenerator _son;
        private IDictionary<Label, Label> _labelMap;
        private IDictionary<LocalBuilder, LocalBuilder> _localMap;

        /// <summary>
        /// The first <see cref="IILGenerator"/> linked
        /// </summary>
        public IILGenerator Me
        {
            get { return _generator; }
        }

        /// <summary>
        /// The second <see cref="IILGenerator"/> linked
        /// </summary>
        public IILGenerator Son
        {
            get { return _son; }
        }

        /// <summary>
        /// Constructs a <see cref="LinkedILGenerator"/>
        /// </summary>
        /// <param name="generator">The first <see cref="IILGenerator"/> to link</param>
        /// <param name="son">The second <see cref="IILGenerator"/> to link</param>
        /// <exception cref="System.ArgumentNullException"><paramref name="generator"/> or <paramref name="son"/> is null</exception>
        public LinkedILGenerator(IILGenerator generator, IILGenerator son)
        {
            if (generator == null)
                throw new ArgumentNullException("This parameter can not be null", "generator");
            if (son == null)
                throw new ArgumentNullException("This parameter can not be null", "son");
            _generator = generator;
            _son = son;
            _labelMap = new Dictionary<Label, Label>();
            _localMap = new Dictionary<LocalBuilder, LocalBuilder>();
        }

        /// <summary>
        /// 
        /// </summary>
        public override sealed int ILOffset
        {
            get { return _generator.ILOffset; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="exceptionType"></param>
        public override sealed void BeginCatchBlock(Type exceptionType)
        {
            _generator.BeginCatchBlock(exceptionType);
            _son.BeginCatchBlock(exceptionType);
        }

        /// <summary>
        /// 
        /// </summary>
        public override sealed void BeginExceptFilterBlock()
        {
            _generator.BeginExceptFilterBlock();
            _son.BeginExceptFilterBlock();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override sealed Label BeginExceptionBlock()
        {
            Label sent = _generator.BeginExceptionBlock();

            _labelMap[sent] = _son.BeginExceptionBlock();
            return sent;
        }

        /// <summary>
        /// 
        /// </summary>
        public override sealed void BeginFaultBlock()
        {
            _generator.BeginFaultBlock();
            _son.BeginFaultBlock();
        }

        /// <summary>
        /// 
        /// </summary>
        public override sealed void BeginFinallyBlock()
        {
            _generator.BeginFinallyBlock();
            _son.BeginFinallyBlock();
        }

        /// <summary>
        /// 
        /// </summary>
        public override sealed void BeginScope()
        {
            _generator.BeginScope();
            _son.BeginScope();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="localType"></param>
        /// <returns></returns>
        public override sealed LocalBuilder DeclareLocal(Type localType)
        {
            LocalBuilder sent = _generator.DeclareLocal(localType);

            _localMap[sent] = _son.DeclareLocal(localType);
            return sent;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="localType"></param>
        /// <param name="pinned"></param>
        /// <returns></returns>
        public override sealed LocalBuilder DeclareLocal(Type localType, bool pinned)
        {
            LocalBuilder sent = _generator.DeclareLocal(localType, pinned);

            _localMap[sent] = _son.DeclareLocal(localType, pinned);
            return sent;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override sealed Label DefineLabel()
        {
            Label sent = _generator.DefineLabel();

            _labelMap[sent] = _son.DefineLabel();
            return sent;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="opcode"></param>
        public override sealed void Emit(OpCode opcode)
        {
            _generator.Emit(opcode);
            _son.Emit(opcode);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="opcode"></param>
        /// <param name="arg"></param>
        public override sealed void Emit(OpCode opcode, byte arg)
        {
            _generator.Emit(opcode, arg);
            _son.Emit(opcode, arg);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="opcode"></param>
        /// <param name="con"></param>
        public override sealed void Emit(OpCode opcode, ConstructorInfo con)
        {
            _generator.Emit(opcode, con);
            _son.Emit(opcode, con);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="opcode"></param>
        /// <param name="arg"></param>
        public override sealed void Emit(OpCode opcode, double arg)
        {
            _generator.Emit(opcode, arg);
            _son.Emit(opcode, arg);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="opcode"></param>
        /// <param name="field"></param>
        public override sealed void Emit(OpCode opcode, FieldInfo field)
        {
            _generator.Emit(opcode, field);
            _son.Emit(opcode, field);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="opcode"></param>
        /// <param name="arg"></param>
        public override sealed void Emit(OpCode opcode, float arg)
        {
            _generator.Emit(opcode, arg);
            _son.Emit(opcode, arg);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="opcode"></param>
        /// <param name="arg"></param>
        public override sealed void Emit(OpCode opcode, int arg)
        {
            _generator.Emit(opcode, arg);
            _son.Emit(opcode, arg);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="opcode"></param>
        /// <param name="label"></param>
        public override sealed void Emit(OpCode opcode, Label label)
        {
            _generator.Emit(opcode, label);
            _son.Emit(opcode, _labelMap[label]);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="opcode"></param>
        /// <param name="labels"></param>
        public override sealed void Emit(OpCode opcode, Label[] labels)
        {
            Label[] tmp = new Label[labels.Length];

            for (int i = 0; i < labels.Length; i++)
            {
                tmp[i] = _labelMap[labels[i]];
            }
            _generator.Emit(opcode, labels);
            _son.Emit(opcode, tmp);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="opcode"></param>
        /// <param name="local"></param>
        public override sealed void Emit(OpCode opcode, LocalBuilder local)
        {
            _generator.Emit(opcode, local);
            _son.Emit(opcode, _localMap[local]);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="opcode"></param>
        /// <param name="arg"></param>
        public override sealed void Emit(OpCode opcode, long arg)
        {
            _generator.Emit(opcode, arg);
            _son.Emit(opcode, arg);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="opcode"></param>
        /// <param name="meth"></param>
        public override sealed void Emit(OpCode opcode, MethodInfo meth)
        {
            _generator.Emit(opcode, meth);
            _son.Emit(opcode, meth);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="opcode"></param>
        /// <param name="arg"></param>
        public override sealed void Emit(OpCode opcode, short arg)
        {
            _generator.Emit(opcode, arg);
            _son.Emit(opcode, arg);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="opcode"></param>
        /// <param name="signature"></param>
        public override sealed void Emit(OpCode opcode, SignatureHelper signature)
        {
            _generator.Emit(opcode, signature);
            _son.Emit(opcode, signature);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="opcode"></param>
        /// <param name="str"></param>
        public override sealed void Emit(OpCode opcode, string str)
        {
            _generator.Emit(opcode, str);
            _son.Emit(opcode, str);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="opcode"></param>
        /// <param name="cls"></param>
        public override sealed void Emit(OpCode opcode, Type cls)
        {
            _generator.Emit(opcode, cls);
            _son.Emit(opcode, cls);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="opcode"></param>
        /// <param name="methodInfo"></param>
        /// <param name="optionalParameterTypes"></param>
        public override sealed void EmitCall(OpCode opcode, MethodInfo methodInfo, Type[] optionalParameterTypes)
        {
            _generator.EmitCall(opcode, methodInfo, optionalParameterTypes);
            _son.EmitCall(opcode, methodInfo, optionalParameterTypes);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="opcode"></param>
        /// <param name="unmanagedCallConv"></param>
        /// <param name="returnType"></param>
        /// <param name="parameterTypes"></param>
        public override sealed void EmitCalli(OpCode opcode, CallingConvention unmanagedCallConv, Type returnType, Type[] parameterTypes)
        {
            _generator.EmitCalli(opcode, unmanagedCallConv, returnType, parameterTypes);
            _son.EmitCalli(opcode, unmanagedCallConv, returnType, parameterTypes);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="opcode"></param>
        /// <param name="callingConvention"></param>
        /// <param name="returnType"></param>
        /// <param name="parameterTypes"></param>
        /// <param name="optionalParameterTypes"></param>
        public override sealed void EmitCalli(OpCode opcode, CallingConventions callingConvention, Type returnType, Type[] parameterTypes, Type[] optionalParameterTypes)
        {
            _generator.EmitCalli(opcode, callingConvention, returnType, parameterTypes, optionalParameterTypes);
            _son.EmitCalli(opcode, callingConvention, returnType, parameterTypes, optionalParameterTypes);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fld"></param>
        public override sealed void EmitWriteLine(FieldInfo fld)
        {
            _generator.EmitWriteLine(fld);
            _son.EmitWriteLine(fld);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="localBuilder"></param>
        public override sealed void EmitWriteLine(LocalBuilder localBuilder)
        {
            _generator.EmitWriteLine(localBuilder);
            _son.EmitWriteLine(_localMap[localBuilder]);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        public override sealed void EmitWriteLine(string value)
        {
            _generator.EmitWriteLine(value);
            _son.EmitWriteLine(value);
        }

        /// <summary>
        /// 
        /// </summary>
        public override sealed void EndExceptionBlock()
        {
            _generator.EndExceptionBlock();
            _son.EndExceptionBlock();
        }

        /// <summary>
        /// 
        /// </summary>
        public override sealed void EndScope()
        {
            _generator.EndScope();
            _son.EndScope();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="loc"></param>
        public override sealed void MarkLabel(Label loc)
        {
            _generator.MarkLabel(loc);
            _son.MarkLabel(_labelMap[loc]);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="document"></param>
        /// <param name="startLine"></param>
        /// <param name="startColumn"></param>
        /// <param name="endLine"></param>
        /// <param name="endColumn"></param>
        public override sealed void MarkSequencePoint(ISymbolDocumentWriter document, int startLine, int startColumn, int endLine, int endColumn)
        {
            _generator.MarkSequencePoint(document, startLine, startColumn, endLine, endColumn);
            _son.MarkSequencePoint(document, startLine, startColumn, endLine, endColumn);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="excType"></param>
        public override sealed void ThrowException(Type excType)
        {
            _generator.ThrowException(excType);
            _son.ThrowException(excType);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="usingNamespace"></param>
        public override sealed void UsingNamespace(string usingNamespace)
        {
            _generator.UsingNamespace(usingNamespace);
            _son.UsingNamespace(usingNamespace);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="local"></param>
        public override sealed void ReleaseLocal(LocalBuilder local)
        {
            _generator.ReleaseLocal(local);
            _son.ReleaseLocal(_localMap[local]);
        }
    }
}
