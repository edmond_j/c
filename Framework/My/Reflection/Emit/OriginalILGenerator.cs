﻿/*
Copyright Julien Edmond (05/06/2014) 

edmondju@gmail.com

This library is a .NET library whose purpose is to help developpers in
diffent ways.

This software is governed by the CeCILL-C  license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.InteropServices;
using System.Diagnostics.SymbolStore;

namespace My.Reflection.Emit
{
    /// <summary>
    /// Extensions inside My.Reflection.Emit
    /// </summary>
    public static partial class Extends
    {
        /// <summary>
        /// Get an IILGenerot from a MethodBuilder
        /// </summary>
        /// <param name="mb">A MethodBuilder</param>
        /// <returns>An <c>IILGenerator</c> that wraps the original <c>ILGenerator</c></returns>
        public static IILGenerator GetIILGenerator(this MethodBuilder mb)
        {
            return new OriginalILGenerator(mb.GetILGenerator());
        }

        /// <summary>
        /// Get an IILGenerot from a MethodBuilder
        /// </summary>
        /// <param name="mb">A MethodBuilder</param>
        /// <param name="size">The size of the IL stream</param>
        /// <returns>An <c>IILGenerator</c> that wraps the original <c>ILGenerator</c></returns>
        public static IILGenerator GetIILGenerator(this MethodBuilder mb, int size)
        {
            return new OriginalILGenerator(mb.GetILGenerator(size));
        }

        /// <summary>
        /// Get an IILGenerot from a ConstructorBuilder
        /// </summary>
        /// <param name="cb">A ConstructorBuilder</param>
        /// <returns>An <c>IILGenerator</c> that wraps the original <c>ILGenerator</c></returns>
        public static IILGenerator GetIILGenerator(this ConstructorBuilder cb)
        {
            return new OriginalILGenerator(cb.GetILGenerator());
        }

        /// <summary>
        /// Get an IILGenerot from a ConstructorBuilder
        /// </summary>
        /// <param name="cb">A ConstructorBuilder</param>
        /// <param name="size">The size of the IL stream</param>
        /// <returns>An <c>IILGenerator</c> that wraps the original <c>ILGenerator</c></returns>
        public static IILGenerator GetIILGenerator(this ConstructorBuilder cb, int size)
        {
            return new OriginalILGenerator(cb.GetILGenerator(size));
        }
    }

    /// <summary>
    /// An <c>IILGenerator</c> that wraps the original <c>ILGenerator</c>
    /// </summary>
    public sealed class OriginalILGenerator: ABaseILGenerator
    {
        private ILGenerator _generator;

        /// <summary>
        /// Wrap an <c>ILGenerator</c>
        /// </summary>
        /// <param name="generator">The <c>ILGenerator</c> to wrap.</param>
        public OriginalILGenerator(ILGenerator generator)
        {
            _generator = generator;
        }

        /// <summary>
        /// 
        /// </summary>
        public override sealed int ILOffset
        {
            get { return _generator.ILOffset; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="exceptionType"></param>
        public override sealed void BeginCatchBlock(Type exceptionType)
        {
            _generator.BeginCatchBlock(exceptionType);
        }

        /// <summary>
        /// 
        /// </summary>
        public override sealed void BeginExceptFilterBlock()
        {
            _generator.BeginExceptFilterBlock();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override sealed Label BeginExceptionBlock()
        {
            return _generator.BeginExceptionBlock();
        }

        /// <summary>
        /// 
        /// </summary>
        public override sealed void BeginFaultBlock()
        {
            _generator.BeginFaultBlock();
        }

        /// <summary>
        /// 
        /// </summary>
        public override sealed void BeginFinallyBlock()
        {
            _generator.BeginFinallyBlock();
        }

        /// <summary>
        /// 
        /// </summary>
        public override sealed void BeginScope()
        {
            _generator.BeginScope();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="localType"></param>
        /// <returns></returns>
        public override sealed LocalBuilder DeclareLocal(Type localType)
        {
            return _generator.DeclareLocal(localType);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="localType"></param>
        /// <param name="pinned"></param>
        /// <returns></returns>
        public override sealed LocalBuilder DeclareLocal(Type localType, bool pinned)
        {
            return _generator.DeclareLocal(localType, pinned);
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override sealed Label DefineLabel()
        {
            return _generator.DefineLabel();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="opcode"></param>
        public override sealed void Emit(OpCode opcode)
        {
            _generator.Emit(opcode);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="opcode"></param>
        /// <param name="arg"></param>
        public override sealed void Emit(OpCode opcode, byte arg)
        {
            _generator.Emit(opcode, arg);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="opcode"></param>
        /// <param name="con"></param>
        public override sealed void Emit(OpCode opcode, ConstructorInfo con)
        {
            _generator.Emit(opcode, con);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="opcode"></param>
        /// <param name="arg"></param>
        public override sealed void Emit(OpCode opcode, double arg)
        {
            _generator.Emit(opcode, arg);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="opcode"></param>
        /// <param name="field"></param>
        public override sealed void Emit(OpCode opcode, FieldInfo field)
        {
            _generator.Emit(opcode, field);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="opcode"></param>
        /// <param name="arg"></param>
        public override sealed void Emit(OpCode opcode, float arg)
        {
            _generator.Emit(opcode, arg);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="opcode"></param>
        /// <param name="arg"></param>
        public override sealed void Emit(OpCode opcode, int arg)
        {
            if (opcode == OpCodes.Ldarg)
                _EmitLdarg(arg);
            else
                _generator.Emit(opcode, arg);
        }

        private void _EmitLdarg(int arg)
        {
            switch (arg)
            {
                case 0:
                    _generator.Emit(OpCodes.Ldarg_0);
                    break;
                case 1:
                    _generator.Emit(OpCodes.Ldarg_1);
                    break;
                case 2:
                    _generator.Emit(OpCodes.Ldarg_2);
                    break;
                case 3:
                    _generator.Emit(OpCodes.Ldarg_3);
                    break;
                default:
                    if (arg <= 255)
                        _generator.Emit(OpCodes.Ldarg_S, arg);
                    else
                        _generator.Emit(OpCodes.Ldarg, arg);
                    break;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="opcode"></param>
        /// <param name="label"></param>
        public override sealed void Emit(OpCode opcode, Label label)
        {
            _generator.Emit(opcode, label);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="opcode"></param>
        /// <param name="labels"></param>
        public override sealed void Emit(OpCode opcode, Label[] labels)
        {
            _generator.Emit(opcode, labels);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="opcode"></param>
        /// <param name="local"></param>
        public override sealed void Emit(OpCode opcode, LocalBuilder local)
        {
            _generator.Emit(opcode, local);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="opcode"></param>
        /// <param name="arg"></param>
        public override sealed void Emit(OpCode opcode, long arg)
        {
            _generator.Emit(opcode, arg);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="opcode"></param>
        /// <param name="meth"></param>
        public override sealed void Emit(OpCode opcode, MethodInfo meth)
        {
            _generator.Emit(opcode, meth);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="opcode"></param>
        /// <param name="arg"></param>
        public override sealed void Emit(OpCode opcode, short arg)
        {
            _generator.Emit(opcode, arg);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="opcode"></param>
        /// <param name="signature"></param>
        public override sealed void Emit(OpCode opcode, SignatureHelper signature)
        {
            _generator.Emit(opcode, signature);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="opcode"></param>
        /// <param name="str"></param>
        public override sealed void Emit(OpCode opcode, string str)
        {
            _generator.Emit(opcode, str);
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="opcode"></param>
        /// <param name="cls"></param>
        public override sealed void Emit(OpCode opcode, Type cls)
        {
            _generator.Emit(opcode, cls);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="opcode"></param>
        /// <param name="methodInfo"></param>
        /// <param name="optionalParameterTypes"></param>
        public override sealed void EmitCall(OpCode opcode, MethodInfo methodInfo, Type[] optionalParameterTypes)
        {
            if (optionalParameterTypes == Type.EmptyTypes)
                optionalParameterTypes = null;
            _generator.EmitCall(opcode, methodInfo, optionalParameterTypes);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="opcode"></param>
        /// <param name="unmanagedCallConv"></param>
        /// <param name="returnType"></param>
        /// <param name="parameterTypes"></param>
        public override sealed void EmitCalli(OpCode opcode, CallingConvention unmanagedCallConv, Type returnType, Type[] parameterTypes)
        {
            _generator.EmitCalli(opcode, unmanagedCallConv, returnType, parameterTypes);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="opcode"></param>
        /// <param name="callingConvention"></param>
        /// <param name="returnType"></param>
        /// <param name="parameterTypes"></param>
        /// <param name="optionalParameterTypes"></param>
        public override sealed void EmitCalli(OpCode opcode, CallingConventions callingConvention, Type returnType, Type[] parameterTypes, Type[] optionalParameterTypes)
        {
            _generator.EmitCalli(opcode, callingConvention, returnType, parameterTypes, optionalParameterTypes);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fld"></param>
        public override sealed void EmitWriteLine(FieldInfo fld)
        {
            _generator.EmitWriteLine(fld);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="localBuilder"></param>
        public override sealed void EmitWriteLine(LocalBuilder localBuilder)
        {
            _generator.EmitWriteLine(localBuilder);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        public override sealed void EmitWriteLine(string value)
        {
            _generator.EmitWriteLine(value);
        }

        /// <summary>
        /// 
        /// </summary>
        public override sealed void EndExceptionBlock()
        {
            _generator.EndExceptionBlock();
        }

        /// <summary>
        /// 
        /// </summary>
        public override sealed void EndScope()
        {
            _generator.EndScope();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="loc"></param>
        public override sealed void MarkLabel(Label loc)
        {
            _generator.MarkLabel(loc);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="document"></param>
        /// <param name="startLine"></param>
        /// <param name="startColumn"></param>
        /// <param name="endLine"></param>
        /// <param name="endColumn"></param>
        public override sealed void MarkSequencePoint(ISymbolDocumentWriter document, int startLine, int startColumn, int endLine, int endColumn)
        {
            _generator.MarkSequencePoint(document, startLine, startColumn, endLine, endColumn);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="excType"></param>
        public override sealed void ThrowException(Type excType)
        {
            _generator.ThrowException(excType);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="usingNamespace"></param>
        public override sealed void UsingNamespace(string usingNamespace)
        {
            _generator.UsingNamespace(usingNamespace);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="local"></param>
        public override sealed void ReleaseLocal(LocalBuilder local)
        {
        }
    }
}
