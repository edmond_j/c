﻿/*
Copyright Julien Edmond (05/06/2014) 

edmondju@gmail.com

This library is a .NET library whose purpose is to help developpers in
diffent ways.

This software is governed by the CeCILL-C  license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

using System;

using System.Collections.Generic;

using System.Diagnostics.SymbolStore;

using System.Linq;

using System.Reflection;

using System.Reflection.Emit;

using System.Runtime.InteropServices;

using System.Security;

using System.Text;

using System.Threading.Tasks;


namespace My.Reflection.Emit
{
    /// <summary>
    /// Interface that serve as base for any ILGenerator
    /// </summary>
    public interface IILGenerator
    {
        /// <summary>Gets the current offset, in bytes, in the Common intermediate language (CIL) stream that is being emitted by the <see cref="T:System.Reflection.Emit.ILGenerator" />.</summary>
        /// <returns>The offset in the CIL stream at which the next instruction will be emitted. </returns>
        int ILOffset { get; }

        /// <summary>Begins a catch block.</summary>
        /// <param name="exceptionType">The <see cref="T:System.Type" /> object that represents the exception. </param>
        void BeginCatchBlock(Type exceptionType);

        /// <summary>Begins an exception block for a filtered exception.</summary>
        void BeginExceptFilterBlock();

        /// <summary>Begins an exception block for a non-filtered exception.</summary>
        /// <returns>The label for the end of the block. This will leave you in the correct place to execute finally blocks or to finish the try.</returns>
        Label BeginExceptionBlock();

        /// <summary>Begins an exception fault block in the Common intermediate language (CIL) stream.</summary>
        void BeginFaultBlock();

        /// <summary>Begins a finally block in the Common intermediate language (CIL) instruction stream.</summary>
        void BeginFinallyBlock();

        /// <summary>Begins a lexical scope.</summary>
        void BeginScope();

        /// <summary>Declares a local variable of the specified type.</summary>
        /// <returns>The declared local variable.</returns>
        /// <param name="localType">A <see cref="T:System.Type" /> object that represents the type of the local variable. </param>
        LocalBuilder DeclareLocal(Type localType);

        /// <summary>Declares a local variable of the specified type, optionally pinning the object referred to by the variable.</summary>
        /// <returns>A <see cref="T:System.Reflection.Emit.LocalBuilder" /> object that represents the local variable.</returns>
        /// <param name="localType">A <see cref="T:System.Type" /> object that represents the type of the local variable.</param>
        /// <param name="pinned">true to pin the object in memory; otherwise, false.</param>
        LocalBuilder DeclareLocal(Type localType, bool pinned);

        /// <summary>Release a local variable</summary>
        /// <returns>The declared local variable.</returns>
        /// <param name="local">A <see cref="System.Reflection.Emit.LocalBuilder" /> object that represents the local variable to release.</param>
        void ReleaseLocal(LocalBuilder local);

        /// <summary>Declares a new label.</summary>
        /// <returns>Returns a new label that can be used as a token for branching.</returns>
        Label DefineLabel();

        /// <summary>Puts the specified instruction onto the stream of instructions.</summary>
        /// <param name="opcode">The Common Intermediate Language (CIL) instruction to be put onto the stream. </param>
        void Emit(OpCode opcode);

        /// <summary>Puts the specified instruction and character argument onto the Common intermediate language (CIL) stream of instructions.</summary>
        /// <param name="opcode">The CIL instruction to be put onto the stream. </param>
        /// <param name="arg">The character argument pushed onto the stream immediately after the instruction. </param>
        void Emit(OpCode opcode, byte arg);

        /// <summary>Puts the specified instruction and metadata token for the specified constructor onto the Common intermediate language (CIL) stream of instructions.</summary>
        /// <param name="opcode">The CIL instruction to be emitted onto the stream. </param>
        /// <param name="con">A ConstructorInfo representing a constructor. </param>
        void Emit(OpCode opcode, ConstructorInfo con);

        /// <summary>Puts the specified instruction and numerical argument onto the Common intermediate language (CIL) stream of instructions.</summary>
        /// <param name="opcode">The CIL instruction to be put onto the stream. Defined in the OpCodes enumeration. </param>
        /// <param name="arg">The numerical argument pushed onto the stream immediately after the instruction. </param>
        void Emit(OpCode opcode, double arg);

        /// <summary>Puts the specified instruction and metadata token for the specified field onto the Common intermediate language (CIL) stream of instructions.</summary>
        /// <param name="opcode">The CIL instruction to be emitted onto the stream. </param>
        /// <param name="field">A FieldInfo representing a field. </param>
        void Emit(OpCode opcode, FieldInfo field);

        /// <summary>Puts the specified instruction and numerical argument onto the Common intermediate language (CIL) stream of instructions.</summary>
        /// <param name="opcode">The CIL instruction to be put onto the stream. </param>
        /// <param name="arg">The Single argument pushed onto the stream immediately after the instruction. </param>
        void Emit(OpCode opcode, float arg);

        /// <summary>Puts the specified instruction and numerical argument onto the Common intermediate language (CIL) stream of instructions.</summary>
        /// <param name="opcode">The CIL instruction to be put onto the stream. </param>
        /// <param name="arg">The numerical argument pushed onto the stream immediately after the instruction. </param>
        void Emit(OpCode opcode, int arg);

        /// <summary>Puts the specified instruction onto the Common intermediate language (CIL) stream and leaves space to include a label when fixes are done.</summary>
        /// <param name="opcode">The CIL instruction to be emitted onto the stream. </param>
        /// <param name="label">The label to which to branch from this location. </param>
        void Emit(OpCode opcode, Label label);

        /// <summary>Puts the specified instruction onto the Common intermediate language (CIL) stream and leaves space to include a label when fixes are done.</summary>
        /// <param name="opcode">The CIL instruction to be emitted onto the stream. </param>
        /// <param name="labels">The array of label objects to which to branch from this location. All of the labels will be used. </param>
        void Emit(OpCode opcode, Label[] labels);

        /// <summary>Puts the specified instruction onto the Common intermediate language (CIL) stream followed by the index of the given local variable.</summary>
        /// <param name="opcode">The CIL instruction to be emitted onto the stream. </param>
        /// <param name="local">A local variable. </param>
        void Emit(OpCode opcode, LocalBuilder local);

        /// <summary>Puts the specified instruction and numerical argument onto the Common intermediate language (CIL) stream of instructions.</summary>
        /// <param name="opcode">The CIL instruction to be put onto the stream. </param>
        /// <param name="arg">The numerical argument pushed onto the stream immediately after the instruction. </param>
        void Emit(OpCode opcode, long arg);

        /// <summary>Puts the specified instruction onto the Common intermediate language (CIL) stream followed by the metadata token for the given method.</summary>
        /// <param name="opcode">The CIL instruction to be emitted onto the stream. </param>
        /// <param name="meth">A MethodInfo representing a method. </param>
        void Emit(OpCode opcode, MethodInfo meth);

        /// <summary>Puts the specified instruction and numerical argument onto the Common intermediate language (CIL) stream of instructions.</summary>
        /// <param name="opcode">The CIL instruction to be emitted onto the stream. </param>
        /// <param name="arg">The Int argument pushed onto the stream immediately after the instruction. </param>
        void Emit(OpCode opcode, short arg);

        /// <summary>Puts the specified instruction and a signature token onto the Common intermediate language (CIL) stream of instructions.</summary>
        /// <param name="opcode">The CIL instruction to be emitted onto the stream. </param>
        /// <param name="signature">A helper for constructing a signature token. </param>
        void Emit(OpCode opcode, SignatureHelper signature);

        /// <summary>Puts the specified instruction onto the Common intermediate language (CIL) stream followed by the metadata token for the given string.</summary>
        /// <param name="opcode">The CIL instruction to be emitted onto the stream. </param>
        /// <param name="str">The String to be emitted. </param>
        void Emit(OpCode opcode, string str);

        /// <summary>Puts the specified instruction onto the Common intermediate language (CIL) stream followed by the metadata token for the given type.</summary>
        /// <param name="opcode">The CIL instruction to be put onto the stream. </param>
        /// <param name="cls">A Type. </param>
        void Emit(OpCode opcode, Type cls);

        /// <summary>Puts a call or callvirt instruction onto the Common intermediate language (CIL) stream to call a varargs method.</summary>
        /// <param name="opcode">The CIL instruction to be emitted onto the stream. Must be <see cref="F:System.Reflection.Emit.OpCodes.Call" />, <see cref="F:System.Reflection.Emit.OpCodes.Callvirt" />, or <see cref="F:System.Reflection.Emit.OpCodes.Newobj" />.</param>
        /// <param name="methodInfo">The varargs method to be called. </param>
        /// <param name="optionalParameterTypes">The types of the optional arguments if the method is a varargs method; otherwise, null. </param>
        void EmitCall(OpCode opcode, MethodInfo methodInfo, Type[] optionalParameterTypes);

        /// <summary>Puts a <see cref="F:System.Reflection.Emit.OpCodes.Calli" /> instruction onto the Common intermediate language (CIL) stream, specifying an unmanaged calling convention for the indirect call.</summary>
        /// <param name="opcode">The CIL instruction to be emitted onto the stream. Must be <see cref="F:System.Reflection.Emit.OpCodes.Calli" />.</param>
        /// <param name="unmanagedCallConv">The unmanaged calling convention to be used. </param>
        /// <param name="returnType">The <see cref="T:System.Type" /> of the result. </param>
        /// <param name="parameterTypes">The types of the required arguments to the instruction. </param>
        void EmitCalli(OpCode opcode, CallingConvention unmanagedCallConv, Type returnType, Type[] parameterTypes);

        /// <summary>Puts a <see cref="F:System.Reflection.Emit.OpCodes.Calli" /> instruction onto the Common intermediate language (CIL) stream, specifying a managed calling convention for the indirect call.</summary>
        /// <param name="opcode">The CIL instruction to be emitted onto the stream. Must be <see cref="F:System.Reflection.Emit.OpCodes.Calli" />. </param>
        /// <param name="callingConvention">The managed calling convention to be used. </param>
        /// <param name="returnType">The <see cref="T:System.Type" /> of the result. </param>
        /// <param name="parameterTypes">The types of the required arguments to the instruction. </param>
        /// <param name="optionalParameterTypes">The types of the optional arguments for varargs calls. </param>
        void EmitCalli(OpCode opcode, CallingConventions callingConvention, Type returnType, Type[] parameterTypes, Type[] optionalParameterTypes);

        /// <summary>Emits the Common intermediate language (CIL) necessary to call <see cref="System.Console.WriteLine()" /> with the given field.</summary>
        /// <param name="fld">The field whose value is to be written to the console. </param>
        void EmitWriteLine(FieldInfo fld);

        /// <summary>Emits the Common intermediate language (CIL) necessary to call <see cref="System.Console.WriteLine()" /> with the given local variable.</summary>
        /// <param name="localBuilder">The local variable whose value is to be written to the console. </param>
        void EmitWriteLine(LocalBuilder localBuilder);

        /// <summary>Emits the Common intermediate language (CIL) to call <see cref="System.Console.WriteLine()" /> with a string.</summary>
        /// <param name="value">The string to be printed. </param>
        void EmitWriteLine(string value);

        /// <summary>Ends an exception block.</summary>
        void EndExceptionBlock();

        /// <summary>Ends a lexical scope.</summary>
        void EndScope();

        /// <summary>Marks the Common intermediate language (CIL) stream's current position with the given label.</summary>
        /// <param name="loc">The label for which to set an index. </param>
        void MarkLabel(Label loc);

        /// <summary>Marks a sequence point in the Common intermediate language (CIL) stream.</summary>
        /// <param name="document">The document for which the sequence point is being defined. </param>
        /// <param name="startLine">The line where the sequence point begins. </param>
        /// <param name="startColumn">The column in the line where the sequence point begins. </param>
        /// <param name="endLine">The line where the sequence point ends. </param>
        /// <param name="endColumn">The column in the line where the sequence point ends. </param>
        void MarkSequencePoint(ISymbolDocumentWriter document, int startLine, int startColumn, int endLine, int endColumn);

        /// <summary>Emits an instruction to throw an exception.</summary>
        /// <param name="excType">The class of the type of exception to throw. </param>
        void ThrowException(Type excType);

        /// <summary>Specifies the namespace to be used in evaluating locals and watches for the current active lexical scope.</summary>
        /// <param name="usingNamespace">The namespace to be used in evaluating locals and watches for the current active lexical scope </param>
        void UsingNamespace(string usingNamespace);
    }
}
