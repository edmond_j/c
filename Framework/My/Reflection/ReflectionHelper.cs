﻿/*
Copyright Julien Edmond (05/06/2014) 

edmondju@gmail.com

This library is a .NET library whose purpose is to help developpers in
diffent ways.

This software is governed by the CeCILL-C  license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace My.Reflection
{
    /// <summary>
    /// Helper for Reflection
    /// </summary>
    public static class ReflectionHelper
    {
        /// <summary>
        /// Change the type of an object (handles enum)
        /// </summary>
        /// <param name="value">The value of the object</param>
        /// <param name="conversionType">The destination type</param>
        /// <returns>The value of <paramref name="value"/> converted to <paramref name="conversionType"/></returns>
        /// <exception cref="System.InvalidCastException">This conversion is not supported.  -or-<paramref name="value" /> is null and <paramref name="conversionType" /> specifies a value type.-or-<paramref name="value" /> does not implement the <see cref="System.IConvertible" /> interface.</exception>
        /// <exception cref="System.FormatException">
        ///   <paramref name="value" /> is not in a format recognized by the <paramref name="conversionType" /> type.</exception>
        /// <exception cref="System.OverflowException">
        ///   <paramref name="value" /> represents a number that is out of the range of the <paramref name="conversionType" /> type.</exception>
        /// <exception cref="System.ArgumentException">
        ///   <paramref name="conversionType" /> is invalid. </exception>
        public static Object ChangeType(Object value, Type conversionType)
        {
            String strValue = value as String;

            if (conversionType.IsEnum && strValue != null)
               return Enum.Parse(conversionType, strValue, true);
            return Convert.ChangeType(value, conversionType);
        }

        /// <summary>
        /// Compute the minimum and maximum parameters that method or constructor can take
        /// </summary>
        /// <param name="method">The method or constructor to test</param>
        /// <returns>A Tuple composed of the minimum numbers of parameters as Item1 and the maximum as Item2</returns>
        public static Tuple<int, int> GetParametersRange(MethodBase method)
        {
            if (method == null)
                throw new ArgumentNullException("method");

            int min = 0;
            int max = 0;
            ParameterInfo[] paramInfos = method.GetParameters();

            foreach (ParameterInfo info in paramInfos)
            {
                if (max != Int32.MaxValue)
                    ++max;
                if (info.IsDefined(typeof(ParamArrayAttribute)))
                    max = Int32.MaxValue;
                else if (!info.HasDefaultValue)
                    ++min;
            }
            return new Tuple<int, int>(min, max);
        }

        /// <summary>
        /// Find the first method that can take the <paramref name="parameters"/> as parameters
        /// </summary>
        /// <param name="parameters">The parameters that would call the method</param>
        /// <param name="methods">An enumeration of methods</param>
        /// <param name="converted">The parameters converted to the types of the method parameters</param>
        /// <returns>The method of <paramref name="methods"/> that would accept the parameters <paramref name="parameters"/></returns>
        public static MethodBase FindSuitableMethod(Object[] parameters, IEnumerable<MethodBase> methods, out Object[] converted)
        {
            foreach (MethodBase method in methods)
            {
                try
                {
                    Tuple<int, int> range = GetParametersRange(method);

                    if (range.Item1 <= parameters.Length && parameters.Length <= range.Item2)
                    {
                        ParameterInfo[] paramInfos = method.GetParameters();
                        object[] newParams = new object[paramInfos.Length];

                        for (int i = 0; i < paramInfos.Length; i++)
                        {
                            if (!paramInfos[i].IsDefined(typeof(ParamArrayAttribute)))
                            {
                                if (i >= parameters.Length)
                                    newParams[i] = paramInfos[i].DefaultValue;
                                else
                                    newParams[i] = ChangeType(parameters[i], paramInfos[i].ParameterType);
                            }
                            else
                            {
                                newParams[i] = Array.CreateInstance(paramInfos[i].ParameterType.GetElementType(), Math.Max(parameters.Length - i, 0));
                                for (int j = 0; j < parameters.Length - i; j++)
                                    ((Array)newParams[i]).SetValue(ChangeType(parameters[i + j], paramInfos[i].ParameterType.GetElementType()), j);
                            }
                        }
                        converted = newParams;
                        return method;
                    }
                }
                catch
                {
                }
            }
            converted = null;
            return null;
        }

        /// <summary>
        /// Find the first method that can take the <paramref name="parameters"/> as parameters
        /// </summary>
        /// <param name="parameters">The parameters that would call the method</param>
        /// <param name="methods">An enumeration of methods</param>
        /// <returns>The method of <paramref name="methods"/> that would accept the parameters <paramref name="parameters"/></returns>
        public static MethodBase FindSuitableMethod(Object[] parameters, IEnumerable<MethodBase> methods)
        {
            object[] ignore;

            return FindSuitableMethod(parameters, methods, out ignore);
        }

        /// <summary>
        /// Find and invoke the first suitable method inside <paramref name="methods"/> that would take <paramref name="parameters"/> as parameters
        /// </summary>
        /// <param name="instance">The instance of the object to call the method on, or null if methods are statics</param>
        /// <param name="parameters">The parameters to send to the method</param>
        /// <param name="methods">The enumeration of methods</param>
        /// <returns>The return of the method call</returns>
        /// <exception cref="My.Reflection.NoSuitableMethodFoundException">No suitable method found</exception>
        public static Object InvokeSuitableMethod(Object instance, Object[] parameters, IEnumerable<MethodBase> methods)
        {
            object[] newParams;
            MethodBase method = FindSuitableMethod(parameters, methods, out newParams);

            if (method != null)
            {
                if (method as ConstructorInfo != null)
                    return ((ConstructorInfo)method).Invoke(newParams);
                return method.Invoke(instance, newParams);
            }
            throw new NoSuitableMethodFoundException();
        }

        /// <summary>
        /// Extracts the types out of a ParameterInfo collection.
        /// </summary>
        /// <param name="paramInfo">The parameters informations.</param>
        /// <returns>A collection containing each parameters types.</returns>
        public static Type[] ExtractTypes(this ParameterInfo[] paramInfo)
        {
            Type[] sent = new Type[paramInfo.Length];

            for (int i = 0; i < paramInfo.Length; ++i)
                sent[i] = paramInfo[i].ParameterType;
            return sent;
        }

        /// <summary>
        /// Extracts the types and modifiers out of a ParameterInfo collection.
        /// </summary>
        /// <param name="parameters">The parameters informations.</param>
        /// <param name="types">A collection containing each parameters types.</param>
        /// <param name="requiredCustomModifiers">A collection containing each parameters required custom modifiers.</param>
        /// <param name="optionalCustomModifiers">A collection containing each parameters custom modifiers.</param>
        public static void ExtractTypesAndModifiers(this ParameterInfo[] parameters, out Type[] types, out Type[][] requiredCustomModifiers, out Type[][] optionalCustomModifiers)
        {
            types = new Type[parameters.Length];
            requiredCustomModifiers = new Type[parameters.Length][];
            optionalCustomModifiers = new Type[parameters.Length][];

            for (int i = 0; i < parameters.Length; ++i)
            {
                types[i] = parameters[i].ParameterType;
                requiredCustomModifiers[i] = parameters[i].GetRequiredCustomModifiers();
                optionalCustomModifiers[i] = parameters[i].GetOptionalCustomModifiers();
            }
        }

        /// <summary>
        /// Gets the property from method.
        /// </summary>
        /// <param name="method">The method, must be the getter or the setter of a Propery.</param>
        /// <returns>The property bound to this method, null if no such property exists.</returns>
        public static PropertyInfo GetPropFromMethod(this MethodInfo method)
        {
            if (!method.IsSpecialName) return null;
            return method.DeclaringType.GetProperty(method.Name.Substring(4),
              BindingFlags.Instance | BindingFlags.Static | BindingFlags.NonPublic);
        }
    }
}
