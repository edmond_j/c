﻿/*
Copyright Julien Edmond (05/06/2014) 

edmondju@gmail.com

This library is a .NET library whose purpose is to help developpers in
diffent ways.

This software is governed by the CeCILL-C  license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace My.IO
{
    /// <summary>
    /// Helper class for Reader interface.
    /// </summary>
    public static class ReaderHelper
    {
        /// <summary>
        /// Create a new <see cref="StringReader"/>.
        /// </summary>
        /// <param name="s">The string to consume.</param>
        /// <returns>A new <see cref="StringReader"/> as a <see cref="IReader{T}"/></returns>
        public static TextReaderWrap NewStringReader(string s)
        {
            return new TextReaderWrap(new StringReader(s), true);
        }

        /// <summary>
        /// Create a new <see cref="StreamReader"/>.
        /// </summary>
        /// <param name="stream">The stream to read.</param>
        /// <param name="encoding">The character encoding to use.</param>
        /// <param name="detectEncodingFromByteOrderMarks">true to look for byte order marks at the beginning of the file; otherwise, false.</param>
        /// <param name="bufferSize">The minimum buffer size.</param>
        /// <param name="leaveOpen">true to leave the stream open after the StreamReader object is disposed; otherwise, false.</param>
        /// <returns>A new <see cref="StreamReader"/> as a <see cref="IReader{T}"/></returns>
        public static TextReaderWrap NewStreamReader(Stream stream, Encoding encoding = null,
            bool detectEncodingFromByteOrderMarks = true, int bufferSize = 1024, bool leaveOpen = false)
        {
            if (encoding == null)
                encoding = Encoding.UTF8;
            return new TextReaderWrap(new StreamReader(stream, encoding, detectEncodingFromByteOrderMarks, bufferSize, leaveOpen), true);
        }

        /// <summary>
        /// Create a new <see cref="StreamReader"/>.
        /// </summary>
        /// <param name="path">The complete file path to be read. </param>
        /// <param name="encoding">The character encoding to use.</param>
        /// <param name="detectEncodingFromByteOrderMarks">true to look for byte order marks at the beginning of the file; otherwise, false.</param>
        /// <param name="bufferSize">The minimum buffer size.</param>
        /// <returns>A new <see cref="StreamReader"/> as a <see cref="IReader{T}"/></returns>
        public static TextReaderWrap NewStreamReader(string path, Encoding encoding = null,
            bool detectEncodingFromByteOrderMarks = true, int bufferSize = 1024)
        {
            if (encoding == null)
                encoding = Encoding.UTF8;
            return new TextReaderWrap(new StreamReader(path, encoding, detectEncodingFromByteOrderMarks, bufferSize), true);
        }
    }
}
