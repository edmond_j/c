﻿/*
Copyright Julien Edmond (05/06/2014) 

edmondju@gmail.com

This library is a .NET library whose purpose is to help developpers in
diffent ways.

This software is governed by the CeCILL-C  license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using My.ComponentModel;

namespace My.StateMachine
{
    internal class State<T>
    {
        private ICollection<Pair<ITransition<T>, State<T>>> _transitions = new List<Pair<ITransition<T>, State<T>>>();
        private bool _isFinal = false;
        private MyEventHandlerList _handlerList = new MyEventHandlerList();

        internal MyEventHandlerList HandlerList
        {
            get { return _handlerList; }
        }

        enum EventType { OnStateEnter, OnStateExit }

        internal bool IsFinal
        {
            get { return _isFinal; }
            set { _isFinal = value; }
        }

        internal ICollection<Pair<ITransition<T>, State<T>>> Transitions
        {
            get { return _transitions; }
        }

        internal void StateEnter(object sender, object context)
        {
            EventHelper.BroadcastEvent<object>((EventHandler<object>)_handlerList[EventType.OnStateEnter], sender, context);
        }

        internal event EventHandler<object> OnStateEnter
        {
            add { _handlerList.AddHandler(EventType.OnStateEnter, value); }
            remove { _handlerList.RemoveHandler(EventType.OnStateEnter, value); }
        }

        internal void StateExit(object sender, object context)
        {
            EventHelper.BroadcastEvent<object>((EventHandler<object>)_handlerList[EventType.OnStateExit], sender, context);
        }

        internal event EventHandler<object> OnStateExit
        {
            add { _handlerList.AddHandler(EventType.OnStateExit, value); }
            remove { _handlerList.RemoveHandler(EventType.OnStateExit, value); }
        }
    }
}
