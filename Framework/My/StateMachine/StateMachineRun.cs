﻿/*
Copyright Julien Edmond (05/06/2014) 

edmondju@gmail.com

This library is a .NET library whose purpose is to help developpers in
diffent ways.

This software is governed by the CeCILL-C  license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace My.StateMachine
{
    internal class StateMachineRun<T> : IStateMachineRun<T>
    {
        private object _context;
        private bool _needStart;

        private List<State<T>> _currentStates;
        private Dictionary<StateMachineRun<T>, Placeholder<T>> _subrun;
        private Queue<StateMachineRun<T>> _subrunToRemove;
        private List<State<T>> _stateToAdd;

        public StateMachineRun(State<T> initialState, object context)
        {
            _context = context;
            _needStart = true;
            _currentStates = new List<State<T>>() { initialState };
            _subrun = new Dictionary<StateMachineRun<T>, Placeholder<T>>();
            _subrunToRemove = new Queue<StateMachineRun<T>>();
            _stateToAdd = new List<State<T>>();
        }

        public bool NeedStart
        {
            get { return _needStart; }
        }

        public void Start()
        {
            if (NeedStart)
            {
                this.Expand(_currentStates);
                _needStart = false;
            }
        }

        private void Expand(List<State<T>> states)
        {
            Queue<State<T>> toVisit = new Queue<State<T>>(states);
            List<State<T>> visited = new List<State<T>>();
            List<State<T>> newStates = new List<State<T>>();

            while (toVisit.Count > 0)
            {
                State<T> current = toVisit.Dequeue();
                visited.Add(current);

                foreach (Pair<ITransition<T>, State<T>> transition in current.Transitions)
                {
                    if (transition.First is EpsilonTransition<T>)
                    {
                        if (!toVisit.Contains(transition.Second) && !visited.Contains(transition.Second))
                            toVisit.Enqueue(transition.Second);
                    }
                }
                _AddNewState(current, newStates);
            }
            _currentStates = newStates;
            _CleanseSubrun();
            if (this.IsInError)
                EventHelper.BroadcastEvent<object>(this.Error, this, _context);
            else if (this.IsInFinal)
                EventHelper.BroadcastEvent<object>(this.Final, this, _context);
        }

        private void _CleanseSubrun()
        {
            while (_subrunToRemove.Count > 0)
                _subrun.Remove(_subrunToRemove.Dequeue());
        }

        private void _AddNewState(State<T> state, List<State<T>> target)
        {
            Placeholder<T> placeholder = state as Placeholder<T>;

            if (placeholder != null && !_stateToAdd.Contains(placeholder))
            {
                StateMachineRun<T> subrun = new StateMachineRun<T>(placeholder.Machine.InitialState, _context);

                subrun.Error += subrun_Error;
                subrun.Final += subrun_Final;
                _subrun.Add(subrun, placeholder);
                subrun.Start();
            }
            else
            {
                state.StateEnter(this, _context);
                target.Add(state);
            }
        }

        void subrun_Final(object sender, object e)
        {
            State<T> state = _subrun[(StateMachineRun<T>)sender];

            if (!_stateToAdd.Contains(state))
                _stateToAdd.Add(state);
        }

        void subrun_Error(object sender, object e)
        {
            _subrunToRemove.Enqueue((StateMachineRun<T>)sender);
        }

        public void Feed(T t)
        {
            if (NeedStart)
                Start();

            foreach (StateMachineRun<T> subrun in this._subrun.Keys)
            {
                subrun.Feed(t);
            }

            List<State<T>> newStates = new List<State<T>>(_stateToAdd);

            foreach (State<T> state in _currentStates)
            {
                foreach (State<T> newState in _Move(state, t))
                {
                    if (!newStates.Contains(newState))
                        newStates.Add(newState);
                }
            }

            this.Expand(newStates);

            _stateToAdd.Clear();
        }

        private static IEnumerable<State<T>> _Move(State<T> state, T t)
        {
            List<State<T>> sent = new List<State<T>>();

            foreach (Pair<ITransition<T>, State<T>> transition in state.Transitions)
            {
                if (!(transition.First is EpsilonTransition<T>) && transition.First.Validate(t))
                    sent.Add(transition.Second);
            }
            return sent;
        }

        public bool IsInError
        {
            get { return _currentStates.Count <= 0 && _subrun.Count <= 0; }
        }

        public bool IsInFinal
        {
            get { return _currentStates.Find(s => s.IsFinal) != null; }
        }

        public event EventHandler<object> Error;

        public event EventHandler<object> Final;
    }
}
