﻿/*
Copyright Julien Edmond (05/06/2014) 

edmondju@gmail.com

This library is a .NET library whose purpose is to help developpers in
diffent ways.

This software is governed by the CeCILL-C  license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace My.StateMachine
{
    internal class StateBuilder<T> : IStateBuilder<T>, IEquatable<IStateBuilder<T>>
    {
        private static int _NextUID = 0;
        private int _uid;

        protected StateMachineBuilder<T> _owner;

        private State<T> _state;

        public StateBuilder(StateMachineBuilder<T> stateMachineBuilder)
            : this(stateMachineBuilder, new State<T>())
        { }

        protected StateBuilder(StateMachineBuilder<T> stateMachineBuilder, State<T> state)
        {
            _uid = ++_NextUID;
            _owner = stateMachineBuilder;
            _state = state;
        }

        internal State<T> State
        {
            get { return _state; }
        }

        internal StateMachineBuilder<T> Owner
        {
            get { return _owner; }
        }

        public void AddTransition(T t, IStateBuilder<T> state)
        {
            this.AddTransition(new EqualityTransition<T>(t), state);
        }

        public void AddTransition(ITransition<T> transition, IStateBuilder<T> state)
        {
            AssertNotFinalized();
            StateBuilder<T> trueState = state as StateBuilder<T>;

            if (trueState == null || trueState.Owner != this.Owner)
                throw new InvalidOperationException("Can not create a transition to a state owned by a different machine.");

            _state.Transitions.Add(new Pair<ITransition<T>, State<T>>(transition, trueState.State));
        }

        private void AssertNotFinalized()
        {
            if (this._owner.IsFinalized)
                throw new InvalidOperationException("Machine already finalized");
        }

        public void AddEpsilonTransition(IStateBuilder<T> state)
        {
            this.AddTransition(EpsilonTransition<T>.Instance, state);
        }

        public bool IsFinal
        {
            get { return _state.IsFinal; }
            set 
            {
                AssertNotFinalized();
                _state.IsFinal = value;
            }
        }

        public event EventHandler<object> OnStateEnter
        {
            add
            {
                AssertNotFinalized();
                _state.OnStateEnter += value;
            }
            remove 
            {
                AssertNotFinalized();
                _state.OnStateEnter -= value;
            }
        }

        public event EventHandler<object> OnStateExit
        {
            add
            {
                AssertNotFinalized();
                _state.OnStateExit += value;
            }
            remove
            {
                AssertNotFinalized();
                _state.OnStateExit -= value;
            }
        }

        public bool Equals(IStateBuilder<T> o)
        {
            StateBuilder<T> other = o as StateBuilder<T>;

            return other != null && _uid == other._uid;
        }
    }
}
