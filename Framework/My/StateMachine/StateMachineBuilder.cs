﻿/*
Copyright Julien Edmond (05/06/2014) 

edmondju@gmail.com

This library is a .NET library whose purpose is to help developpers in
diffent ways.

This software is governed by the CeCILL-C  license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace My.StateMachine
{
    /// <summary>
    /// Implementation of a StateMachineBuilder.
    /// </summary>
    /// <typeparam name="T">The type of the alphabet used by the generated state machine.</typeparam>
    public sealed class StateMachineBuilder<T> : IStateMachineBuilder<T>
    {
        private bool _finalized = false;
        private IStateBuilder<T> _initialState = null;
        private StateMachine<T> _result = null;

        private List<StateBuilder<T>> _states;
        private List<PlaceholderBuilder<T>> _placeholders;
        private List<StateMachineBuilder<T>> _submachines;

        /// <summary>
        /// Initializes a new instance of the <see cref="StateMachineBuilder{T}"/> class.
        /// </summary>
        public StateMachineBuilder()
        {
            _result = new StateMachine<T>();
            _states = new List<StateBuilder<T>>();
            _placeholders = new List<PlaceholderBuilder<T>>();
            _submachines = new List<StateMachineBuilder<T>>();
        }

        internal IReadOnlyCollection<StateBuilder<T>> States
        {
            get { return _states; }
        }

        internal IReadOnlyCollection<PlaceholderBuilder<T>> Placeholders
        {
            get { return _placeholders; }
        }

        internal IReadOnlyCollection<StateMachineBuilder<T>> Submachines
        {
            get { return _submachines; }
        }

        internal bool IsFinalized
        {
            get { return _finalized; }
        }

        internal StateMachine<T> InternalResult
        {
            get { return _result; }
        }

        /// <summary>
        /// Gets the result.
        /// </summary>
        /// <value>
        /// The result.
        /// </value>
        /// <exception cref="System.InvalidOperationException">Machine has not been finalized yet</exception>
        public IStateMachine<T> Result
        {
            get 
            {
                if (!this.IsFinalized)
                    throw new InvalidOperationException("Machine has not been finalized yet");
                return _result; 
            }
        }

        /// <summary>
        /// Finalizes the building of this state machine.
        /// </summary>
        /// <returns>The StateMachine generated.</returns>
        public IStateMachine<T> Finalize()
        {
            AssertNotFinalized();
            if (this._initialState == null)
                throw new InvalidOperationException("An initial state must be defined");
            _finalized = true;
            return _result;
        }

        private void AssertNotFinalized()
        {
            if (this.IsFinalized)
                throw new InvalidOperationException("Machine already finalized");
        }

        /// <summary>
        /// Creates a state inside this state machine.
        /// </summary>
        /// <returns>The StateBuilder corresponding to that state.</returns>
        public IStateBuilder<T> CreateState()
        {
            AssertNotFinalized();
            StateBuilder<T> sent = new StateBuilder<T>(this);

            _states.Add(sent);
            if (_initialState == null)
                _initialState = sent;
            return sent;
        }

        /// <summary>
        /// Creates a reference to a state machine inside this state machine.
        /// </summary>
        /// <param name="submachine">The referenced submachine.</param>
        /// <returns>A StateBuilder that act as a placeholder for the referenced state machine.</returns>
        public IPlaceHolder<T> CreatePlaceHolder(IBaseStateMachineBuilder<T> submachine)
        {
            AssertNotFinalized();
            PlaceholderBuilder<T> sent = new PlaceholderBuilder<T>(this, (StateMachineBuilder<T>)submachine);

            _placeholders.Add(sent);
            if (_initialState == null)
                _initialState = sent;
            return sent;
        }

        /// <summary>
        /// Creates a submachine that can be referenced by this state machine or others.
        /// </summary>
        /// <returns>A StateMachineBuilder corresponding to the created machine.</returns>
        public IBaseStateMachineBuilder<T> CreateSubmachine()
        {
            AssertNotFinalized();
            StateMachineBuilder<T> sent = new StateMachineBuilder<T>();

            _submachines.Add(sent);
            return sent;
        }

        /// <summary>
        /// Gets or sets the initial state.
        /// </summary>
        /// <value>
        /// The initial state.
        /// </value>
        public IStateBuilder<T> InitialState
        {
            get { return _initialState; }
            set 
            {
                AssertNotFinalized();
                _initialState = value;
                _result.InitialState = ((StateBuilder<T>)_initialState).State;
            }
        }
    }
}
