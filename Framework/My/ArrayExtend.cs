﻿/*
Copyright Julien Edmond (05/06/2014) 

edmondju@gmail.com

This library is a .NET library whose purpose is to help developpers in
diffent ways.

This software is governed by the CeCILL-C  license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace My
{
    /// <summary>
    /// Extensions for arrays.
    /// </summary>
    public static class ArrayExtend
    {
        /// <summary>
        /// Create a sub array.
        /// </summary>
        /// <typeparam name="T">The Type of the items stored inside the array.</typeparam>
        /// <param name="array">The source array.</param>
        /// <param name="index">The index where to start the sub array.</param>
        /// <returns>An array containing the items of <paramref name="array"/>, starting from <paramref name="index"/></returns>
        public static T[] SubArray<T>(this T[] array, int index)
        {
            return SubArray<T>(array, index, array.Length - index);
        }

        /// <summary>
        /// Create a sub array.
        /// </summary>
        /// <typeparam name="T">The Type of the items stored inside the array.</typeparam>
        /// <param name="array">The source array.</param>
        /// <param name="index">The index where to start the sub array.</param>
        /// <param name="length">The length of the returned array.</param>
        /// <returns>An array containing the items of <paramref name="array"/>, starting from <paramref name="index"/></returns>
        public static T[] SubArray<T>(this T[] array, int index, int length)
        {
            T[] sent = new T[length];

            for (int i = 0; i < length && i < array.Length - index; ++i)
            {
                sent[i] = array[i + index];
            }
            return sent;
        }
    }
}
