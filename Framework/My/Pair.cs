﻿/*
Copyright Julien Edmond (05/06/2014) 

edmondju@gmail.com

This library is a .NET library whose purpose is to help developpers in
diffent ways.

This software is governed by the CeCILL-C  license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace My
{
    /// <summary>
    /// Class representing a pair of objects.
    /// </summary>
    /// <typeparam name="TFirst">The type of the first object.</typeparam>
    /// <typeparam name="TSecond">The type of the second object.</typeparam>
    public class Pair<TFirst, TSecond> : IEquatable<Pair<TFirst, TSecond>>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Pair{TFirst, TSecond}"/> class with default values.
        /// </summary>
        public Pair() : this(default(TFirst), default(TSecond))
        {}

        /// <summary>
        /// Initializes a new instance of the <see cref="Pair{TFirst, TSecond}"/> class.
        /// </summary>
        /// <param name="first">The value of the first element.</param>
        /// <param name="second">The value of the second element.</param>
        public Pair(TFirst first, TSecond second)
        {
            First = first;
            Second = second;
        }

        /// <summary>
        /// The first element
        /// </summary>
        public TFirst First;

        /// <summary>
        /// The second element
        /// </summary>
        public TSecond Second;

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>
        /// true if the current object is equal to the <paramref name="other" /> parameter; otherwise, false.
        /// </returns>
        public bool Equals(Pair<TFirst, TSecond> other)
        {
            return First.Equals(other.First) && Second.Equals(other.Second);
        }
    }

    /// <summary>
    /// Class representing a pair of objects.
    /// </summary>
    /// <typeparam name="T">The type of both elements</typeparam>
    public class Pair<T> : Pair<T, T>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Pair{T}"/> class with default values.
        /// </summary>
        public Pair()
        {}

        /// <summary>
        /// Initializes a new instance of the <see cref="Pair{T}"/> class.
        /// </summary>
        /// <param name="first">The value of the first element.</param>
        /// <param name="second">The value of the second element.</param>
        public Pair(T first, T second) : base(first, second)
        {}
    }
}
