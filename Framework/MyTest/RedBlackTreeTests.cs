﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using My.Collection.Generic;

namespace MyTest
{
    [TestClass]
    public class RedBlackTreeTests
    {
        [TestMethod]
        public void TestAdd()
        {
            //int[] toAdd = new int[] { 10, 12, 14, 15, 16, 17, 18, 21, 24, 25, 33, 55, 66, 95, 100 };
            int[] toAdd = new int[] { 10, 95, 15, 66, 21, 55, 33, 17, 25, 12, 100, 24, 16, 14, 18};
            //int[] toAdd = new int[] { 10, 85, 15, 70, 20, 60, 30, 50, 65, 80, 90, 40, 5, 55 };
            RedBlackTree<int> collection = new RedBlackTree<int>();

            for (int i = 0; i < toAdd.Length; ++i)
            {
                collection.Add(toAdd[i]);
#if DEBUG
                collection.ToDot(String.Format("add{0}.gv", i));
#endif
            }

            foreach (int item in collection)
            {
                Console.WriteLine(item);
            }
            collection.Dispose();
        }

        [TestMethod]
        public void TestDeletion()
        {
            RedBlackTree<int> collection = new RedBlackTree<int>() { 10, 12, 14, 15, 16, 17, 18, 21, 24, 25, 33, 55, 66, 95, 100 };
            int[] toRm = new int[] { 10, 95, 15, 66, 21, 55, 33, 17, 25, 12, 100, 24, 16, 14, 18 };

#if DEBUG
            collection.ToDot("del0.gv");
#endif
            for (int i = 0; i < toRm.Length; ++i)
            {
                collection.Remove(toRm[i]);
#if DEBUG
                collection.ToDot(String.Format("del{0}.gv", i+1));
#endif

                Console.Write("List:");

                foreach (int item in collection)
                {
                    Console.Write(" {0}", item);
                }
                Console.WriteLine();
            }
            collection.Dispose();
        }
    }
}
