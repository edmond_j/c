﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using My.StateMachine;

namespace MyTest
{
    /// <summary>
    /// Summary description for StateMachineTest
    /// </summary>
    [TestClass]
    public class StateMachineTest
    {
        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void Empty()
        {
            StateMachineBuilder<char> main = new StateMachineBuilder<char>();

            main.Finalize();
        }

        [TestMethod]
        public void Rendering1()
        {
            StateMachineBuilder<char> main = new StateMachineBuilder<char>();
            IBaseStateMachineBuilder<char> a = main.CreateSubmachine();
            IBaseStateMachineBuilder<char> b = main.CreateSubmachine();

            // main
            IStateBuilder<char> phaM = main.CreatePlaceHolder(a);

            main.InitialState = phaM;
            phaM.IsFinal = true;

            // a
            IStateBuilder<char> a0 = a.CreateState();
            IStateBuilder<char> phbA = a.CreatePlaceHolder(b);

            a.InitialState = a0;
            a0.AddTransition('a', phbA);
            phbA.IsFinal = true;

            // b
            IStateBuilder<char> b0 = b.CreateState();
            IStateBuilder<char> phaB = b.CreatePlaceHolder(a);

            b.InitialState = b0;
            b0.AddTransition('b', phaB);
            phaB.IsFinal = true;


            main.Finalize();
        }

        [TestMethod]
        public void Rendering2()
        {
            StateMachineBuilder<char> main = new StateMachineBuilder<char>();
            IBaseStateMachineBuilder<char> a = main.CreateSubmachine();
            IBaseStateMachineBuilder<char> b = main.CreateSubmachine();
            IBaseStateMachineBuilder<char> c = main.CreateSubmachine();

            // main
            IStateBuilder<char> m0 = main.CreateState();
            IPlaceHolder<char> phaM = main.CreatePlaceHolder(a);
            IPlaceHolder<char> phbM = main.CreatePlaceHolder(b);

            main.InitialState = m0;
            m0.AddEpsilonTransition(phaM);
            m0.AddEpsilonTransition(phbM);
            phaM.IsFinal = true;
            phbM.IsFinal = true;

            // a
            IStateBuilder<char> a0 = a.CreateState();
            IStateBuilder<char> a1 = a.CreateState();
            IPlaceHolder<char> phcA = a.CreatePlaceHolder(c);

            a.InitialState = a0;
            a0.AddTransition('l', phcA);
            phcA.AddTransition('l', a1);
            a1.IsFinal = true;
            
            // b
            IStateBuilder<char> b0 = b.CreateState();
            IStateBuilder<char> b1 = b.CreateState();
            IPlaceHolder<char> phcB = b.CreatePlaceHolder(c);

            b.InitialState = b0;
            b0.AddTransition('p', phcB);
            phcB.AddTransition('p', b1);
            b1.IsFinal = true;

            // c
            IStateBuilder<char> c0 = c.CreateState();
            IStateBuilder<char> c1 = c.CreateState();

            c.InitialState = c0;
            c0.AddTransition('i', c1);
            c0.AddTransition('o', c1);
            c1.IsFinal = true;


            main.Finalize();
        }

        [TestMethod]
        public void Rendering3()
        {
            StateMachineBuilder<char> main = new StateMachineBuilder<char>();
            IBaseStateMachineBuilder<char> a = main.CreateSubmachine();
            IBaseStateMachineBuilder<char> b = main.CreateSubmachine();

            // main
            IPlaceHolder<char> phaM = main.CreatePlaceHolder(a);

            main.InitialState = phaM;
            phaM.IsFinal = true;

            // a
            IStateBuilder<char> a0 = a.CreateState();
            IStateBuilder<char> a1 = a.CreatePlaceHolder(b);
            IStateBuilder<char> a2 = a.CreateState();

            a.InitialState = a0;
            a0.AddTransition('a', a1);
            a1.AddTransition('a', a2);
            a2.IsFinal = true;

            // b
            IStateBuilder<char> b0 = b.CreateState();
            IStateBuilder<char> b1 = b.CreatePlaceHolder(b);
            IStateBuilder<char> b2 = b.CreateState();

            b.InitialState = b0;
            b0.AddTransition('b', b1);
            b1.AddTransition('b', b2);
            b2.IsFinal = true;


            main.Finalize();
        }

#region BasicTest
        
        [TestMethod]
        public void BasicTest()
        {
            StateMachineBuilder<bool> main = new StateMachineBuilder<bool>();
            IStateBuilder<bool> s1 = main.CreateState();
            IStateBuilder<bool> s2 = main.CreateState();
            IStateBuilder<bool> s3 = main.CreateState();
            IStateBuilder<bool> s4 = main.CreateState();

            main.InitialState = s1;

            s1.AddTransition(false, s2);
            s1.AddEpsilonTransition(s3);
            s1.OnStateEnter += s1_OnStateEnter;

            s2.AddTransition(true, s2);
            s2.AddTransition(true, s4);
            s2.OnStateEnter += s2_OnStateEnter;

            s3.AddEpsilonTransition(s2);
            s3.AddTransition(false, s4);
            s3.IsFinal = true;
            s3.OnStateEnter += s3_OnStateEnter;

            s4.AddTransition(false, s3);
            s4.IsFinal = true;
            s4.OnStateEnter += s4_OnStateEnter;

            IStateMachine<bool> machine = main.Finalize();

            NewRun<bool>(machine, new bool[] { true, true, true, false, true, false, false, false, false, true, true, true, true, false });
        }

        void s4_OnStateEnter(object sender, object e)
        {
            Console.WriteLine("S4");
        }

        void s3_OnStateEnter(object sender, object e)
        {
            Console.WriteLine("S3");
        }

        void s2_OnStateEnter(object sender, object e)
        {
            Console.WriteLine("S2");
        }

        void s1_OnStateEnter(object sender, object e)
        {
            Console.WriteLine("S1");
        }

#endregion

#region Recursive

        [TestMethod]
        public void Recursive()
        {
            StateMachineBuilder<char> main = new StateMachineBuilder<char>();
            IBaseStateMachineBuilder<char> a = main.CreateSubmachine();

            // main
            IStateBuilder<char> m0 = main.CreatePlaceHolder(a);

            main.InitialState = m0;

            m0.OnStateEnter += m0_OnStateEnter;
            m0.IsFinal = true;

            // a
            IStateBuilder<char> sa0 = a.CreateState();
            IStateBuilder<char> sa1 = a.CreateState();
            IStateBuilder<char> sa2 = a.CreatePlaceHolder(main);

            a.InitialState = sa0;

            sa0.OnStateEnter += sa0_OnStateEnter;
            sa0.AddTransition('o', sa1);
            sa0.AddTransition('l', sa2);

            sa1.OnStateEnter += sa1_OnStateEnter;
            sa1.IsFinal = true;

            sa2.OnStateEnter += sa2_OnStateEnter;
            sa2.AddTransition('l', sa1);

            IStateMachine<char> machine = main.Finalize();

            NewRun<char>(machine, "lol".ToCharArray());
            NewRun<char>(machine, "loll".ToCharArray());
            NewRun<char>(machine, "lllolll".ToCharArray());
            NewRun<char>(machine, "lllollll".ToCharArray());
        }

        void sa2_OnStateEnter(object sender, object e)
        {
            Console.WriteLine("SA2");
        }

        void sa1_OnStateEnter(object sender, object e)
        {
            Console.WriteLine("SA1");
        }

        void sa0_OnStateEnter(object sender, object e)
        {
            Console.WriteLine("SA0");
        }

        void m0_OnStateEnter(object sender, object e)
        {
            Console.WriteLine("M0");
        }

#endregion

        private void NewRun<T>(IStateMachine<T> machine, T[] p)
        {
            IStateMachineRun<T> run = machine.CreateRun(null);

            run.Error += run_Error;
            run.Final += run_Final;

            Console.WriteLine("------ Start -----");
            run.Start();
            Console.WriteLine();

            for (int i = 0; i < p.Length; ++i)
            {
                Console.WriteLine("New input {0}", p[i]);
                run.Feed(p[i]);
                Console.WriteLine(i + 1 == p.Length ? "------------------" : "");
            }
        }

        void run_Final(object sender, object e)
        {
            Console.WriteLine("Final");
        }

        void run_Error(object sender, object e)
        {
            Console.WriteLine("Error");
        }
    }
}
